import numpy
from keras.callbacks import Callback
from nltk.tokenize import WordPunctTokenizer
from nltk.translate import bleu_score

import glovembedder


class GuessScore(Callback):
    def __init__(self, x_test, y_test, i2p, expected=1, printout=True):
        super(GuessScore, self).__init__()
        self.expected = expected
        self.i2p = i2p
        self.exp_name = self.i2p[expected]
        self.X = x_test
        self.Y = y_test
        self.po = printout

    def on_epoch_end(self, epoch, logs=None):
        if logs is None:
            logs = {}
        try:
            tp, fp, fn = 0, 0, 0
            for gold_sent, to_pred_sent in zip(self.Y, self.X):
                pred_sent = self.model.predict(glovembedder.encode_sentence(to_pred_sent))
                for true, pred in zip(gold_sent, pred_sent):
                    pred = numpy.argmax(pred)
                    true = numpy.argmax(true)
                    if true == self.expected:
                        if pred == true:
                            tp += 1
                        else:
                            fn += 1
                    elif pred == self.expected:
                        fp += 1

            precision, recall, f1 = -1., -1., -1.
            if tp + fp != 0 and tp + fn != 0:
                precision = float(tp) / (tp + fp)
                recall = float(tp) / (tp + fn)
                f1 = 2 * (precision * recall) / (precision + recall)

            logs['precision'] = precision
            logs['recall'] = recall
            logs['f1'] = f1

            if self.po:
                print
                print "Precision: ", precision
                print "Recall: ", recall
                print "F1:", f1
        except Exception, e:
            print "Error: ", str(e)


class BleuScore(Callback):
    def __init__(self, x_test, y_test, answer_embedder):
        """

        :param List[str | unicode] x_test:
        :param List[str | unicode] y_test:
        :param S2SDecEmbedder answer_embedder:
        """
        Callback.__init__(self)
        tokenizer = WordPunctTokenizer()
        self.x = x_test
        self.y = [tokenizer.tokenize(sent) for sent in y_test]
        self.answer_embedder = answer_embedder

    def on_epoch_end(self, batch, logs=None):
        predictions_corpus = []
        predictions = []
        for sentence in self.x:
            prediction = self.model.predict(glovembedder.encode_sentence(sentence))
            predictions_corpus.append([self.answer_embedder.decode_sentence(prediction).split()])
            predictions.append([self.answer_embedder.decode_sentence(prediction).split()])

        tot_score = bleu_score.corpus_bleu(predictions_corpus, self.y)

        avg_score = .0
        for prediction, answer in zip(predictions, self.y):
            avg_score += bleu_score.sentence_bleu(prediction, answer)
        avg_score /= len(self.x)

        logs['tot_bleu'] = tot_score
        logs['avg_bleu'] = avg_score
