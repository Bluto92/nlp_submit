import os
import pickle
import numpy
from keras.callbacks import ModelCheckpoint, CSVLogger, ReduceLROnPlateau
from keras.layers import LSTM, Dense, InputLayer, Masking
from keras.metrics import binary_accuracy
from keras.models import Sequential
from nltk.tokenize import WordPunctTokenizer
from sklearn.model_selection import train_test_split
import glovembedder
from src.utils import database
from Callbacks import GuessScore

MODEL_TEMP_DIR = "../models/"
LOG_DIR = "../logs/"
UNK = '$unk$'
PAD = '$pad$'
i2p = {0: 'Yes', 1: 'No'}


def glove_embedder_generator(X, Y):
    for x, y in zip(X, Y):
        yield glovembedder.encode_sentence(x), y


def create_yes_no_model(relations, name, overwrite=False):
    model_dir = MODEL_TEMP_DIR + "yesno/glove_emb/"
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)

    model_dir += name + "/"
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)

    elif not overwrite:
        print "Model yet calculated for ", relations
        return None

    model_name = "yes_no_" + name
    yes_qas, no_qas = [], []
    for relation in relations:
        yes_qas += [qa[0] for qa in database.get_by_answer(relation, 'yes', lower=True)]
        no_qas += [qa[0] for qa in database.get_by_answer(relation, 'no', lower=True)]

    if len(no_qas) == 0:
        print "Skipped: ", relations
        return None
    size_ratio = float(len(yes_qas)) / len(no_qas)

    ratio_unit = "y/n"
    if size_ratio < .65:
        size_ratio = 1 / size_ratio
        yes_qas *= int(size_ratio)
        ratio_unit = "n/y"
    elif size_ratio > 1.3:
        no_qas *= int(size_ratio)

    size_ratio = float(len(yes_qas)) / len(no_qas)
    print "Yesses: ", str(len(yes_qas))
    print "Nos: ", str(len(no_qas))
    print "Size ratio: ", str(size_ratio), " ", ratio_unit

    answer_w2i = {'yes': 0, 'no': 1}
    pickle.dump(answer_w2i, open(model_dir + model_name + "_yn_answer_w2i.pickle", mode='w+'))

    # create X, y
    X_yes_train, X_yes_test = train_test_split(yes_qas, test_size=0.15)
    X_no_train, X_no_test = train_test_split(no_qas, test_size=0.15)
    X_train = X_yes_train + X_no_train
    X_test = X_yes_test + X_no_test

    yes = numpy.zeros((1, 2), dtype=bool)
    yes[0, 0] = True
    no = numpy.zeros((1, 2), dtype=bool)
    no[0, 1] = True

    Y_train = [yes for _ in X_yes_train, no for _ in X_no_train]
    Y_test = [yes for _ in X_yes_test, no for _ in X_no_test]

    dropout = 0.4

    weights_improvement_file_path = model_dir + "/" + model_name + \
                                    "-{epoch:02d}-a{binary_accuracy:.4f}-l{loss:.4f}-" \
                                    "va{val_binary_accuracy:.4f}-vl{val_loss:.4f}-f1{f1:.4}.keras"

    checkpoint = ModelCheckpoint(weights_improvement_file_path, monitor='binary_accuracy', mode='max',
                                 save_best_only=True, )

    csv_log_path = LOG_DIR + model_name + ".log"
    csv_logger = CSVLogger(csv_log_path)
    reduce_lr = ReduceLROnPlateau(monitor='binary_accuracy', patience=2)

    # early_stopping_acc = EarlyStopping(monitor='val_binary_accuracy', patience=2, mode='max')
    # early_stopping_loss = EarlyStopping(monitor='val_loss', patience=2, mode='min')

    guess_score = GuessScore(X_test, Y_test, i2p, expected=0)
    callbacks = [guess_score, checkpoint, csv_logger, reduce_lr]

    # RNN model
    model = Sequential()
    model.add(InputLayer(input_shape=(None, 300)))
    model.add(Masking())
    # model.add(Bidirectional(LSTM(200, return_sequences=True, recurrent_dropout=dropout, dropout=dropout)))

    model.add(LSTM(1000, return_sequences=False, recurrent_dropout=dropout, dropout=dropout))

    model.add(Dense(len(answer_w2i.keys()), dtype=numpy.bool, activation='softmax'))

    model.compile(optimizer='rmsprop', loss='binary_crossentropy', metrics=[binary_accuracy])

    model.summary()

    model.fit_generator(generator=glove_embedder_generator(X_train, Y_train),
                        steps_per_epoch=int(abs(len(X_train) / 32)) + 1,
                        validation_data=glove_embedder_generator(X_test, Y_test),
                        validation_steps=int(abs(len(X_test) / 32)) + 1,
                        callbacks=callbacks)

    return True


def encode_question(question, w2i):
    tokenizer = WordPunctTokenizer()
    tokens = tokenizer.tokenize(question)
    encoded_question = numpy.full((1, len(tokens) + 2), 0, dtype=numpy.uint32)
    for index, token in enumerate(tokens):
        encoded_question[0, index] = w2i.get(token, w2i[UNK])
    return encoded_question


if __name__ == '__main__':
    from ..utils import domains_utils

    relations = domains_utils.get_all_relations_kbs()

    for relation in relations:
        print "*" * 15, relation, "*" * 15
        if create_yes_no_model([relation], relation.lower()):
            pass
            # break
