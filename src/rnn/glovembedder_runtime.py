from src.utils import embedding_utils
from nltk.tokenize import WordPunctTokenizer
from nltk.corpus import stopwords
import numpy

PAD = '$pad$'
UNK = '$unk$'
# Matrix, W2I = embedding_utils.get_full_glove_matrix_w2i(embedding_utils.GLOVE_FILE_PATH_LITTLE)
# Matrix, W2I = embedding_utils.get_full_glove_matrix_w2i(embedding_utils.GLOVE_FILE_PATH)
W2I = embedding_utils.get_full_glove_w2i(pad=PAD, unk=UNK)
Tokenizer = WordPunctTokenizer()
wordsstop = stopwords.words('english')


# size = len(W2I)


def decode_sentences(encoded_sentences, w2i=W2I, unk=UNK):
    decoded_sentences = []
    for encoded_sentence in encoded_sentences:
        decoded_sentences.append(decode_sentence(encoded_sentence, w2i=w2i, unk=unk))

    return decoded_sentences


def decode_sentence(encoded_sentence, w2i=W2I, unk=UNK):
    decoded_sentence = u''
    for encoded_word in encoded_sentence:
        decoded_sentence += w2i.get(numpy.argmax(encoded_word), unk) + u' '

    return decoded_sentence.rstrip()


def encode_sentence(sentence, w2i=W2I, unk=UNK, max_len=None, use_stopwords=False,
                    glove_file_path=embedding_utils.GLOVE_FILE_PATH, vector_size=300):
    tokens = Tokenizer.tokenize(sentence)
    if use_stopwords:
        n_tokens = []
        for token in tokens:
            if token not in wordsstop:
                n_tokens.append(token)
        tokens = n_tokens

    if max_len is None:
        max_len = len(tokens)
        if max_len == 0:
            return None
    sentence_array = numpy.zeros((1, max_len, vector_size), dtype=numpy.float32)
    for index, word in enumerate(tokens):
        if index >= max_len:
            break
        # print word
        w2i_word = w2i.get(word, w2i[unk])
        word_vect = numpy.zeros(vector_size, dtype=numpy.float32)
        if w2i_word != w2i[unk]:
            for glove_word, glove_vector in embedding_utils.generator_glove_word_vector(
                    glove_file_path=glove_file_path):
                if glove_word == word:
                    word_vect = glove_vector
                    break

        sentence_array[0, index] = word_vect

    return sentence_array


if __name__ == '__main__':
    sent_ar = encode_sentence('is there anybody here ?')
