from keras.models import Sequential, load_model
from keras.layers import LSTM, Dense, InputLayer, Bidirectional, Masking
from keras.callbacks import ModelCheckpoint, CSVLogger, Callback
from keras.metrics import binary_accuracy, binary_crossentropy
from src.utils import database, domains_utils
import numpy
import os
from sklearn.model_selection import train_test_split
from src.rnn.Callbacks import GuessScore

MODEL_TEMP_DIR = "../models/"
LOG_DIR = "../logs/"
i2p = {0: "Subj", 1: "Obj"}
MODEL_DIR = MODEL_TEMP_DIR + "is_subj_obj/"


def generator(X, Y):
    for x, y in zip(X, Y):
        yield src.rnn.glovembedder.encode_sentence(x), y


def get_data(relations):
    subj_count, obj_count = 0, 0
    X, Y = [], []
    for relation in relations:
        subjects = database.get_all_subjects(relation)
        subj_count += len(subjects)
        X += subjects

        for _ in subjects:
            y = numpy.full((1, len(i2p)), False, dtype=bool)
            y[0, 0] = True
            Y.append(y)

        objects = database.get_all_objects(relation)
        obj_count += len(objects)
        X += objects

        for _ in objects:
            y = numpy.full((1, len(i2p)), False, dtype=bool)
            y[0, 1] = True
            Y.append(y)

    print "Number of Subjects: ", subj_count
    print "Number of Objects: ", obj_count
    return X, Y


def build_model():
    # build model
    model = Sequential()
    model.add(InputLayer((None, 300)))
    model.add(Masking())
    # model.add(Bidirectional(LSTM(500, return_sequences=True, dropout=0.5)))
    model.add(Bidirectional(LSTM(500, return_sequences=False, dropout=0.3)))
    model.add(Dense(len(i2p), activation='softmax', dtype=numpy.bool))
    model.compile(optimizer='rmsprop', loss=binary_crossentropy, metrics=[binary_accuracy])
    model.summary()

    return model


def fit_model(model, relations, model_dir=MODEL_DIR):
    # callbacks
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)

    model_dir += "all/"
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)

    model_name = "is_subj_obj_all"
    # model_name = "subj_obj_finder_questions"
    weights_improvement_file_path = model_dir + "/" + model_name + \
                                    "-{epoch:02d}-a{binary_accuracy:.4f}-l{loss:.4f}-" \
                                    "va{val_binary_accuracy:.4f}-vl{val_loss:.4f}-f1{f1:.4}.keras"

    # Collecting Data
    X, Y = get_data(relations)
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.1)

    print
    print "Train size: ", len(X_train)
    print "Test size: ", len(X_test)
    print

    # build callbacks
    checkpoint = ModelCheckpoint(weights_improvement_file_path, monitor='binary_accuracy', mode='max')
    # save_best_only=True,

    csv_log_path = LOG_DIR + model_name + ".log"
    csv_logger = CSVLogger(csv_log_path)
    guess_score_obj = GuessScore(X_test, Y_test, expected=1)

    callbacks = [guess_score_obj, checkpoint, csv_logger]
    # reduce_lr, guess_score_subj, early_stopping_f1O, early_stopping_acc, early_stopping_loss,

    batch_size = 32
    model.fit_generator(generator=generator(X_train, Y_train), steps_per_epoch=len(X_train) / batch_size,
                        validation_data=generator(X_test, Y_test), validation_steps=len(X_test) / batch_size,
                        epochs=20, callbacks=callbacks)


if __name__ == '__main__':
    # relations = domains_utils.get_all_relations_kbs()
    relations = [u'GENERALIZATION']
    import src.rnn.glovembedder

    model = build_model()
    fit_model(model, relations)

    model_name = raw_input("Give me the model name: ")
    model_filepath = MODEL_DIR + model_name

    print "Loading Model"
    model = load_model(model_filepath)

    while True:
        query = raw_input("Write a question, i'll tell you Subj and Obj: ")
        if query == 'quit':
            break
        elif query.startswith('cmod '):
            model_name = query[5:]
            if os.path.exists(MODEL_DIR + model_name):
                print "changing model"
                model = load_model(MODEL_DIR + model_name)
            else:
                print "The specified model do not exists in dir ", MODEL_DIR

        x = src.rnn.glovembedder.encode_sentence(query)

        print query
        print
        y_pred = model.predict(x)

        for yy in y_pred[0]:
            print i2p[numpy.argmax(yy)]
