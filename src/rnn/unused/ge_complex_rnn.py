import os

import numpy
from keras.callbacks import ModelCheckpoint, CSVLogger
from keras.layers import LSTM, Dense, InputLayer, Bidirectional, Masking, RepeatVector, TimeDistributed
from keras.metrics import categorical_accuracy, categorical_crossentropy
from keras.models import Sequential, load_model
from sklearn.model_selection import train_test_split

from src.rnn.Callbacks import BleuScore
from src.rnn.unused.embedder import S2SDecEmbedder
from src.utils import database

MODEL_TEMP_DIR = "../models/"
LOG_DIR = "../logs/"
MODEL_DIR = MODEL_TEMP_DIR + "complex/"
MAX_LEN_ANS = 7


def generator(X, Y, answer_embedder):
    for x, y in zip(X, Y):
        yield src.rnn.glovembedder.encode_sentence(x), answer_embedder.encode_sentence(y, max_len=MAX_LEN_ANS)


def get_data(relations, test_size=0.1):
    x_train, y_train, x_test, y_test = [], [], [], []
    for relation in relations:
        qas = database.get_yes_no_qas(relation, schema=database.ENRICHING_SCHEMA, yes_no=False)
        x, y = [], []
        for qa in qas:
            question = qa[0]
            answer = qa[1]

            x.append(question)
            y.append(answer)

        # splits the data of each relation in the 4 final sets
        x_train_rel, x_test_rel, y_train_rel, y_test_rel = train_test_split(x, y, test_size=test_size)
        x_train += x_train_rel
        x_test += x_test_rel
        y_train += y_train_rel
        y_test += y_test_rel

    return x_train, y_train, x_test, y_test


def build_model(w2i):
    # build model
    model = Sequential()
    model.add(InputLayer((None, 300)))
    model.add(Masking())
    model.add(Bidirectional(LSTM(250, return_sequences=True, dropout=0.4)))
    model.add(Bidirectional(LSTM(250, return_sequences=False, dropout=0.4)))
    model.add(RepeatVector(MAX_LEN_ANS))
    model.add(Bidirectional(LSTM(250, return_sequences=True, dropout=0.4)))
    model.add(Bidirectional(LSTM(250, return_sequences=True, dropout=0.4)))
    model.add(TimeDistributed(Dense(len(w2i), activation='softmax', dtype=numpy.bool)))
    model.compile(optimizer='rmsprop', loss=categorical_crossentropy, metrics=[categorical_accuracy])
    model.summary()

    return model


def fit_model(model, answer_embedder, model_dir=MODEL_DIR):
    # callbacks
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)

    model_dir += "all/"
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)

    model_name = "complex_all"
    # model_name = "subj_obj_finder_questions"
    weights_improvement_file_path = model_dir + "/" + model_name + \
                                    "-{epoch:02d}-a{categorical_accuracy:.4f}-l{loss:.4f}-" \
                                    "va{val_categorical_accuracy:.4f}-vl{val_loss:.4f}-" \
                                    "bleu-avg{avg_bleu:.4}-bleu-tot{tot_bleu:.4}.keras"

    # build callbacks
    checkpoint = ModelCheckpoint(weights_improvement_file_path, monitor='categorical_accuracy', mode='max')
    # save_best_only=True,

    csv_log_path = LOG_DIR + model_name + ".log"
    csv_logger = CSVLogger(csv_log_path)
    bleu_callback = BleuScore(X_test, Y_test, answer_embedder)
    callbacks = [bleu_callback, checkpoint, csv_logger]
    # reduce_lr, guess_score_subj, early_stopping_f1O, early_stopping_acc, early_stopping_loss,

    batch_size = 32
    model.fit_generator(generator=generator(X_train, Y_train, answer_embedder),
                        steps_per_epoch=len(X_train) / batch_size,
                        validation_data=generator(X_test, Y_test, answer_embedder),
                        validation_steps=len(X_test) / batch_size,
                        epochs=2, callbacks=callbacks)


if __name__ == '__main__':
    relations = [u'GENERALIZATION']
    # relations = domains_utils.get_all_relations_kbs()
    import src.rnn.glovembedder

    # Collecting Data
    X_train, Y_train, X_test, Y_test = get_data(relations, test_size=0.1)

    print
    print "Train size: ", len(X_train)
    print "Test size: ", len(X_test)
    print

    decoder_embedder = S2SDecEmbedder(Y_train)
    model = build_model(decoder_embedder.word2index)
    fit_model(model, decoder_embedder)

    model_name = raw_input("Give me the model name: ")
    model_filepath = MODEL_DIR + model_name

    print "Loading Model"
    model = load_model(model_filepath)

    while True:
        query = raw_input("Write a question, i'll tell you Subj and Obj: ")
        if query == 'quit':
            break
        elif query.startswith('cmod '):
            model_name = query[5:]
            if os.path.exists(MODEL_DIR + model_name):
                print "changing model"
                model = load_model(MODEL_DIR + model_name)
            else:
                print "The specified model do not exists in dir ", MODEL_DIR

        print query
        print
        x = src.rnn.glovembedder.encode_sentence(query)
        y_pred = model.predict(x)
        print src.rnn.glovembedder.decode_sentence(y_pred[0])
