from tqdm import tqdm
from src.utils import database
from src import prove
import keras
from keras.models import Sequential
from keras.layers import LSTM, Embedding, Dense, InputLayer, Bidirectional, TimeDistributed, RepeatVector, Masking
from keras.callbacks import ModelCheckpoint, CSVLogger, ReduceLROnPlateau, EarlyStopping
import numpy
import os
import pickle
import src.rnn.glovembedder
from keras.metrics import categorical_accuracy
from sklearn.model_selection import train_test_split
from nltk.tokenize import WordPunctTokenizer

RELATION = u'GENERALIZATION'
MODEL_TEMP_DIR = "../models/"
LOG_DIR = "../logs/"
MAX_RSP = 5
# MAX_QST = 15


def generator_x_y(data_x, data_y, aw2i):
    assert len(data_x) == len(data_y)

    for x, y in zip(data_x, data_y):
        x_enc = src.rnn.glovembedder.encode_sentence(x)
        # x_enc = encode_sentence_x(x, w2i=w2iq, tokenizer=tokenizer)
        y_enc = encode_sentence_y(y, aw2i=aw2i)
        # y_enc = glovembedder.encode_sentence(y)
        yield x_enc, y_enc


def encode_sentence_x(sentence, w2i, tokenizer=WordPunctTokenizer()):
    tokens = tokenizer.tokenize(sentence)
    encoded_sentence = numpy.zeros((1, len(tokens),), dtype=numpy.uint32)

    # encoded_sentence[0, 0] = w2i['<START>']
    # for index, token in enumerate(tokens, start=1):
    #     encoded_sentence[0, index] = w2i.get(token, w2i['<UNK>'])
    # encoded_sentence[0, len(tokens) + 1] = w2i['<STOP>']

    for index, token in enumerate(tokens):
        encoded_sentence[0, index] = w2i.get(token, w2i['<UNK>'])

    return encoded_sentence


def encode_sentence_y(sentence, aw2i):
    encoded_sentence = numpy.full((1, MAX_RSP, len(aw2i)), False, dtype=numpy.bool)

    # encoded_sentence[0, 0, w2i['<START>']] = True
    # for index, token in enumerate(tokens, start=1):
    #     if index >= ans_len:
    #         break
    #     encoded_sentence[0, index, w2i.get(token, w2i['<UNK>'])] = True
    # encoded_sentence[0, ans_len-1, w2i['<STOP>']] = True
    tokenizer = WordPunctTokenizer()
    for index, word in enumerate(tokenizer.tokenize(sentence)):
        if index >= MAX_RSP:
            break
        encoded_sentence[0, index, aw2i[word]] = True

    return encoded_sentence


def create_others_model():
    model_dir = MODEL_TEMP_DIR + "/others/"
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)
    model_name = "others_" + RELATION.lower()

    qas = database.get_yes_no_qas(RELATION, False)

    # build vocabulary_questions / w2i / i2w
    tokenizer = WordPunctTokenizer()
    answers_token_set = set()
    with tqdm(total=len(qas), desc="Extracting vocabulary for answers") as td:
        for entry in qas:
            td.update(1)
            answer = entry[1]
            for token in tokenizer.tokenize(answer):
                answers_token_set.add(token)

    answers_word2index = {word: index for index, word in enumerate(answers_token_set)}
    pickle.dump(answers_word2index, open(model_dir + model_name + "_others_answer_w2i.pickle", mode='w+'))

    # create X, y
    X, Y = [], []
    with tqdm(total=len(qas), desc="Creating X, Y") as td:
        for entry in qas:
            td.update(1)
            question = entry[0]
            X.append(question)

            answer = entry[1]
            Y.append(answer)

    test_size = .15 if len(X) > 60000 else .1
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size)

    batch_size = 16
    num_layers = 2
    dropout = 0.3

    weights_improvement_file_path = model_dir + "/" + model_name + \
                                    "-{epoch:02d}-a{categorical_accuracy:.4f}-l{loss:.4f}-" \
                                    "va{val_categorical_accuracy:.4f}-vl{val_loss:.4f}.keras"

    checkpoint = ModelCheckpoint(weights_improvement_file_path, monitor='categorical_accuracy', mode='max')
    # save_best_only=True,

    csv_log_path = LOG_DIR + model_name + ".log"
    csv_logger = CSVLogger(csv_log_path)
    reduce_lr = ReduceLROnPlateau(monitor='categorical_accuracy', patience=1)

    early_stopping_acc = EarlyStopping(monitor='categorical_accuracy', patience=3, mode='max')
    early_stopping_loss = EarlyStopping(monitor='loss', patience=3, mode='min')

    callbacks = [checkpoint,
                 csv_logger,
                 early_stopping_acc,
                 early_stopping_loss,
                 reduce_lr]

    # RNN model
    model = Sequential()
    model.add(InputLayer(input_shape=(None, 300)))
    model.add(Masking())

    # encoder
    for _ in xrange(0, num_layers):
        model.add(Bidirectional(
            LSTM(200, return_sequences=True, recurrent_dropout=dropout, dropout=dropout)))

    model.add(Bidirectional(LSTM(512, return_sequences=False, recurrent_dropout=dropout, dropout=dropout)))

    model.add(RepeatVector(MAX_RSP))

    # decoder
    for _ in xrange(0, num_layers):
        model.add(Bidirectional(
            LSTM(200, return_sequences=True, recurrent_dropout=dropout, dropout=dropout)))

    model.add(Bidirectional(LSTM(512, return_sequences=True, recurrent_dropout=dropout, dropout=dropout)))

    model.add(TimeDistributed(Dense(len(answers_word2index), dtype=numpy.bool, activation='softmax')))

    model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=[categorical_accuracy])

    model.summary()

    model.fit_generator(
        generator=generator_x_y(X_train, Y_train, aw2i=answers_word2index),
        steps_per_epoch=(len(X_train) / batch_size),
        validation_data=generator_x_y(X_test, Y_test, aw2i=answers_word2index),
        validation_steps=(len(X_test) / batch_size),
        epochs=20, callbacks=callbacks)


def answer_full_query_recognition(relation, question):
    results = database.get_by_question(relation=relation, question=question)

    if len(results) > 0:
        return results[0][1]

    return None


def answer_through_patterns_recognition(relation, question):
    subj, obj = prove.get_question_subj_obj(relation=relation, question=question)
    print "Subj: ", subj, "\tObj: ", obj
    if subj is not None and obj is not None:
        results = database.get_obj_subj(relation=relation, subj=subj, obj=obj)
        if len(results) > 0:
            return results[0][1]

    return None


def encode_question(question, w2i):
    tokenizer = WordPunctTokenizer()
    tokens = tokenizer.tokenize(question)
    encoded_question = numpy.full((1, len(tokens) + 2), 0, dtype=numpy.uint32)
    for index, token in enumerate(tokens):
        encoded_question[0, index] = w2i.get(token, w2i['<UNK>'])
    return encoded_question


def ask_queries():
    model_dir = "/home/fra/Projects/Universita/NLP/FinalProject/models/yesno/"
    model_name = "yes_no_generalization-06-a0.9635-l0.1220-va0.9651-vl0.1095.keras"
    question_w2i_name = "yes_no_generalization_question_w2i.pickle"
    answer_w2i_name = "yes_no_generalization_answer_w2i.pickle"

    print "Loading W2I/I2W"
    QuestionW2I = pickle.load(open(model_dir + question_w2i_name, mode="r"))
    AnswerW2I = pickle.load(open(model_dir + answer_w2i_name, mode="r"))
    AnswerI2W = {index: word for word, index in AnswerW2I.items()}

    print "Loading Keras Model: ", model_name
    model = keras.models.load_model(model_dir + model_name)
    print "Model startup"
    model.predict(encode_question(question="startup", w2i=QuestionW2I))

    while True:
        # question
        question = raw_input('Ask a Question: ').lstrip(' ').rstrip(' ')
        if question == "quit":
            break

        answer = answer_full_query_recognition(relation=u'GENERALIZATION', question=question)
        if answer is not None:
            print answer, "[FQ]"

        answer = answer_through_patterns_recognition(relation=u'GENERALIZATION', question=question)
        if answer is not None:
            print answer, "[PR]"
            continue

        # RNN
        encoded_question = encode_question(question=question, w2i=QuestionW2I)
        encoded_answers = model.predict(encoded_question)
        answer = ""
        for encoded_answer in encoded_answers:
            for word_distribution in encoded_answer:
                word_index = numpy.argmax(word_distribution)
                word = AnswerI2W.get(word_index, ' UNK ')
                if word not in ['<START>', '<STOP>', '<PAD>', '<UNK>']:
                    answer += word
            print
            print answer, "\t[RNN]"


if __name__ == '__main__':
    create_others_model()
    # ask_queries()
