from nltk.tokenize import WordPunctTokenizer
import numpy

PAD = '$pad$'
UNK = '$unk$'


class S2SDecEmbedder(object):
    def __init__(self, y):
        self.tokenizer = WordPunctTokenizer()
        self.vocabulary = set()
        vocabulary_filter = {}
        for sentence in y:
            for word in self.tokenizer.tokenize(sentence.lower()):
                vocabulary_filter[word] = vocabulary_filter.get(word, 0) + 1

        for word, occurrence in vocabulary_filter.items():
            if occurrence > 0:
                self.vocabulary.add(word)

        print "All Vocabulary len: ", len(vocabulary_filter)
        print "Filtered vocabulary len: ", len(self.vocabulary)

        self.word2index = {word: index for index, word in enumerate(sorted(list(self.vocabulary)), start=2)}
        self.word2index[PAD] = 0
        self.word2index[UNK] = 1
        self.index2word = {index: word for word, index in self.word2index.items()}

    def encode_sentence(self, sentence, max_len=None, unk=UNK):
        sentence_tokens = self.tokenizer.tokenize(sentence.lower())
        if not max_len:
            max_len = len(sentence_tokens)

        encoded_sentence = numpy.full((1, max_len, len(self.word2index)), False, dtype=numpy.bool)
        for index, word in enumerate(sentence_tokens):
            if index >= max_len:
                break
            encoded_sentence[0, index, self.word2index.get(word, self.word2index[unk])] = True

        return encoded_sentence

    def decode_sentence(self, encoded_sentence, unk=UNK):
        decoded_sentence = u''
        for encoded_word in encoded_sentence[0]:
            decoded_sentence += self.index2word.get(numpy.argmax(encoded_word), unk) + u' '

        return decoded_sentence

