from keras.models import Sequential, load_model
from keras.layers import LSTM, Dense, InputLayer, Bidirectional, TimeDistributed, Masking
from keras.callbacks import ModelCheckpoint, CSVLogger, Callback
from keras.metrics import categorical_accuracy, categorical_crossentropy
from src.utils import database, domains_utils
from nltk.tokenize import WordPunctTokenizer
# import glovembedder
import numpy
import os
from tqdm import tqdm
from sklearn.model_selection import train_test_split

MODEL_TEMP_DIR = "../models/"
LOG_DIR = "../logs/"

SUBJ_TAG = 'Subj'
OBJ_TAG = 'Obj'
NONE_TAG = 'Ni'
INDEX2TAG = {0: NONE_TAG, 1: SUBJ_TAG, 2: OBJ_TAG}


def generator(X, Y):
    for x, y in zip(X, Y):
        yield glovembedder.encode_sentence(x), y


def get_data(relations, is_question=True):
    tokenizer = WordPunctTokenizer()
    subj_count, obj_count = 0, 0

    X, Y = [], []
    with tqdm(total=len(relations)) as td:
        for relation in relations:
            td.desc = relation + " "
            td.refresh()
            qas = database.get_all_qas(relation, schema=database.ENRICHING_SCHEMA)
            # qas = database.get_yes_no_qas(relation, yes_no=False,
            #  schema=database.ENRICHING_SCHEMA)
            # qas = database.get_yes_no_qas(relation, yes_no=True, schema=database.ENRICHING_SCHEMA)
            for qa in qas:
                # sentence
                sentence = qa[0].rstrip(' ').lower() if is_question else qa[1].rstrip(' ')  # type: str
                if not sentence:
                    continue

                # check if obj/subj is in sentence
                subj, obj = None, None
                if is_question:
                    subj_sent = qa[6]  # type: bool
                    if subj_sent:
                        subj = qa[2]  # type: str
                        subj_count += 1

                    obj_sent = qa[7]  # type: bool
                    if obj_sent:
                        obj = qa[3]  # type: str
                        obj_count += 1
                else:
                    subj_sent = qa[8]  # type: bool
                    if subj_sent:
                        subj = qa[2]  # type: str
                        subj_count += 1

                    obj_sent = qa[9]  # type: bool
                    if obj_sent:
                        obj = qa[3]  # type: str
                        obj_count += 1

                if (not obj and obj_sent) or (not subj and subj_sent) or (not subj_sent and not obj_sent):
                    continue
                #
                # high = obj or subj
                # if not high or (sentence.find(subj) == -1 and sentence.find(obj) == -1):
                #     continue

                # find obj/subj index in sentence
                qst_tokens = tokenizer.tokenize(sentence)
                start_index_subj, end_index_subj = -1, -1
                if subj:
                    subj_tokens = tokenizer.tokenize(subj)
                    for pos in numpy.arange(0, len(qst_tokens), 1):
                        if pos + len(subj_tokens) <= len(qst_tokens):
                            is_good = True
                            for ht, qt in zip(subj_tokens, qst_tokens[pos:pos + len(subj_tokens)]):
                                if ht != qt:
                                    is_good = False
                                    break
                            if is_good:
                                start_index_subj = pos
                                end_index_subj = pos + len(subj_tokens)

                start_index_obj, end_index_obj = -1, -1
                if obj:
                    obj_tokens = tokenizer.tokenize(obj)
                    for pos in numpy.arange(0, len(qst_tokens), 1):
                        if pos + len(obj_tokens) <= len(qst_tokens):
                            is_good = True
                            for ht, qt in zip(obj_tokens, qst_tokens[pos:pos + len(obj_tokens)]):
                                if ht != qt:
                                    is_good = False
                                    break
                            if is_good:
                                start_index_obj = pos
                                end_index_obj = pos + len(obj_tokens)

                # if (start_index_subj == -1 or end_index_subj == -1) and
                # (start_index_obj == -1 or end_index_obj == -1):
                #     continue

                # build answer  0 -> Stuff, 1 -> Subj, 2 -> Obj
                y = numpy.full((1, len(qst_tokens), len(INDEX2TAG)), False, dtype=numpy.bool)
                for pos, qt in enumerate(qst_tokens):
                    if start_index_subj <= pos < end_index_subj:
                        y[0, pos, 1] = True
                    elif start_index_obj <= pos < end_index_obj:
                        # y[0, pos, 1] = True
                        y[0, pos, 2] = True
                    else:
                        y[0, pos, 0] = True

                X.append(sentence)
                Y.append(y)
            td.update(1)

    print "Number of Subjects: ", subj_count
    print "Number of Objects: ", obj_count
    return X, Y


def build_model():
    # build model
    model = Sequential()
    model.add(InputLayer((None, 300)))
    model.add(Masking())
    # model.add(Bidirectional(LSTM(32, return_sequences=True, dropout=0.3)))
    model.add(Bidirectional(LSTM(500, return_sequences=True, dropout=0.3)))
    model.add(Bidirectional(LSTM(500, return_sequences=True, dropout=0.3)))
    model.add(TimeDistributed(Dense(len(INDEX2TAG), activation='softmax', dtype=numpy.bool)))
    model.compile(optimizer='rmsprop', loss=categorical_crossentropy, metrics=[categorical_accuracy])
    model.summary()

    return model


def fit_model(model, relations=domains_utils.get_all_relations_kbs(), question=True):
    # # callbacks
    if question:
        model_dir = MODEL_TEMP_DIR + "sofinder/questions/"
        model_name = "subj_obj_finder_questions_all"
    else:
        model_name = "subj_obj_finder_answers_all"
        model_dir = MODEL_TEMP_DIR + "sofinder/answers/"

    if not os.path.exists(model_dir):
        os.mkdir(model_dir)

    model_dir += "all/"
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)

    weights_improvement_file_path = model_dir + "/" + model_name + \
                                    "-{epoch:02d}-a{categorical_accuracy:.4f}-l{loss:.4f}-" \
                                    "va{val_categorical_accuracy:.4f}-vl{val_loss:.4f}-" \
                                    "f1-Obj{f1-Obj:.4}-f1-Subj{f1-Subj:.4}.keras"

    # Collecting Data
    X, Y = get_data(relations)
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.1)

    print
    print "Train size: ", len(X_train)
    print "Test size: ", len(X_test)
    print

    # build callbacks
    checkpoint = ModelCheckpoint(weights_improvement_file_path, monitor='f1-Obj', mode='max')
    # save_best_only=True,

    csv_log_path = LOG_DIR + model_name + ".log"
    csv_logger = CSVLogger(csv_log_path)
    # reduce_lr = ReduceLROnPlateau(monitor='categorical_accuracy', patience=3)

    # early_stopping_acc = EarlyStopping(monitor='categorical_accuracy', patience=3, mode='max')
    # early_stopping_loss = EarlyStopping(monitor='loss', patience=3, mode='min')
    # early_stopping_f1O = EarlyStopping(monitor='f1-Obj', patience=3, mode='max')

    guess_score_subj = GuessScore(X_test, Y_test, expected=1)
    guess_score_obj = GuessScore(X_test, Y_test, expected=2)
    # guess_score_obj = GuessScore(X_test, Y_test, expected=1)

    callbacks = [guess_score_obj, guess_score_subj, checkpoint, csv_logger]
    # reduce_lr,  early_stopping_f1O, early_stopping_acc, early_stopping_loss,

    model.fit_generator(generator=generator(X_train, Y_train), steps_per_epoch=len(X_train) / 32,
                        validation_data=generator(X_test, Y_test), validation_steps=len(X_test) / 32,
                        epochs=2, callbacks=callbacks)


class GuessScore(Callback):
    def __init__(self, x_test, y_test, expected=1, printout=True):
        super(GuessScore, self).__init__()
        self.expected = expected
        self.exp_name = INDEX2TAG[expected]
        self.X = x_test
        self.Y = y_test
        self.po = printout

    def on_epoch_end(self, epoch, logs=None):
        if logs is None:
            logs = {}
        try:
            tp, fp, fn = 0, 0, 0
            for gold_sent, to_pred_sent in zip(self.Y, self.X):
                pred_sent = self.model.predict(glovembedder.encode_sentence(to_pred_sent))
                for true, pred in zip(gold_sent[0], pred_sent[0]):
                    pred = numpy.argmax(pred)
                    true = numpy.argmax(true)
                    if true == self.expected:
                        if pred == true:
                            tp += 1
                        else:
                            fn += 1
                    elif pred == self.expected:
                        fp += 1

            precision, recall, f1 = .0, .0, .0
            if tp + fp != 0 and tp + fn != 0:
                precision = float(tp) / (tp + fp)
                recall = float(tp) / (tp + fn)
                f1 = 2 * (precision * recall) / (precision + recall)

            logs['precision-' + self.exp_name] = precision
            logs['recall-' + self.exp_name] = recall
            logs['f1-' + self.exp_name] = f1

            if self.po:
                print
                print "Precision ", self.exp_name, ": ", precision
                print "Recall ", self.exp_name, ": ", recall
                print "F1 ", self.exp_name, ":", f1
        except Exception, e:
            print "Error: ", str(e)


if __name__ == '__main__':
    model = build_model()

    fit_model(model, relations=domains_utils.get_all_relations_kbs(), question=True)

    # model_dir = MODEL_TEMP_DIR + "sofinder/answers/"
    model_dir = MODEL_TEMP_DIR + "sofinder/questions/"
    tokenizer = WordPunctTokenizer()

    model_name = raw_input("Give me the model name: ")
    model_filepath = model_dir + model_name

    print "Loading Model"
    model = load_model(model_filepath)

    while True:
        query = raw_input("Write a question, i'll tell you Subj and Obj: ")
        if query == 'quit':
            break
        elif query.startswith('cmod '):
            model_name = query[5:]
            if os.path.exists(model_dir + model_name):
                print "changing model"
                model = load_model(model_dir + model_name)
            else:
                print "The specified model do not exists in dir ", model_dir
        import glovembedder
        x = glovembedder.encode_sentence(query)

        print query
        print
        y_pred = model.predict(x)
        y_lst = []
        for yy in y_pred[0]:
            y_lst.append(INDEX2TAG[numpy.argmax(yy)])

        qst_tokens = tokenizer.tokenize(query.lower())
        for word, y in zip(qst_tokens, y_lst):
            print word, y
