# coding=utf-8
from nltk.tokenize import WordPunctTokenizer
from threading import Thread, Timer, Event

from src.ai import main_sense_interceptor
from src.utils import babelnet_client, database
from src.utils import sentence_manipulation, patterns_manager
from src.rnn import subj_obj_rnn, glovembedder_runtime

import numpy
import regex
import spacy
import logging

logging.basicConfig(filename='../logs/tgrambot.log', level=logging.DEBUG,
                    format='%(asctime)s - %(name)s -  %(message)s')
logger = logging.getLogger(__name__)

nlp = spacy.load('en_core_web_md')

MODEL_PATH = "../models/sofinder/questions/all/subj_obj_finder_questions_all-00-a0.9846-l0.0822" \
                 "-va0.9924-vl0.0746-f1-Obj0.9718-f1-Subj0.9899.keras"

logger.debug("Patterns Extraction: Loading Model")
SubjObjRNN = subj_obj_rnn.load_model(MODEL_PATH)

SENSE_PLACEHOLDER = "SNS"
MATCHED_THRESHOLD = 20


def get_question_representation(question, meaningless_words):
    # tokenizer = WordPunctTokenizer()
    # pre-processing
    # 0.1  - right and left whitespace and question mark strip, and lower the question
    question = question.lstrip().rstrip('?! ').lower()

    # 0.2 - strip from sentence all the elements in strippers
    strippers = u'.,;:'
    # strippers = list(punctuation)
    stripped_question = question.strip(strippers)

    # 1 - mask sensed substrings
    masked_stripped_question = u''
    # for word in tokenizer.tokenize(stripped_question):
    for word in stripped_question.split():
        masked_stripped_question += word + u' ' if word in meaningless_words else SENSE_PLACEHOLDER

    # 1.1 - remove multiple SENSE_PLACEHOLDER
    masked_stripped_question = regex.sub(r'(' + SENSE_PLACEHOLDER + ')+', SENSE_PLACEHOLDER + u' ',
                                         masked_stripped_question)

    return masked_stripped_question.rstrip()


def validate_structure(questions, pattern, reg_expr):
    """
  
    :param list[unicode | str] questions:
    :param unicode pattern:
    :param str | unicode reg_expr:
    :return: tagged pattern
    :rtype: unicode
    """

    if not 1 <= pattern.count(SENSE_PLACEHOLDER) <= 2:
        return None
    reg_expr = reg_expr.rstrip('$').lstrip('^')

    # print "Encoding Questions"
    encoded_questions = []
    for question in questions:
        encoded_qst = glovembedder_runtime.encode_sentence(question)
        encoded_questions.append(encoded_qst)

    # print "Predicting Questions"
    predicted_questions = []
    for enc_qst in encoded_questions:
        predicted_quest = SubjObjRNN.predict(enc_qst)
        predicted_questions.append(predicted_quest)

    # print "Matching"
    matches = {}
    for quest, predicted_quest in zip(questions, predicted_questions):
        quest_analysis = quest
        pattern_analysis = quest
        # find Subj/Obj
        subj_obj = find_subj_obj(quest, predicted_quest)
        if subj_obj:
            subj, obj = subj_obj[0], subj_obj[1]

            if subj:
                if len(sentence_manipulation.find_words(quest, subj)) == 1:
                    quest_analysis = quest_analysis.replace(subj, '(.*?)')
                    pattern_analysis = pattern_analysis.replace(subj, patterns_manager.SUBJ_PLACEHOLDER)

            if obj:
                if len(sentence_manipulation.find_words(quest, obj)) == 1:
                    pattern_analysis = pattern_analysis.replace(obj, patterns_manager.OBJ_PLACEHOLDER)
                    quest_analysis = quest_analysis.replace(obj, '(.*?)')

            if quest_analysis == reg_expr:
                matches[pattern_analysis] = matches.get(pattern_analysis, 0) + 1

        # count how many time the given pattern and the obtained one are equal
        if len(matches) > 0:
            count2pattern = {occ: p for p, occ in matches.items()}
            max_occ = max(matches.values())
            max_pattern = count2pattern[max_occ]

            import pprint
            pprint.pprint(count2pattern)

            if max_occ >= MATCHED_THRESHOLD:
                return max_pattern

    return None


def find_subj_obj(sentence, subj_obj_prediction):
    tokenize = WordPunctTokenizer().tokenize
    word_predictions = []
    for word_prediction in subj_obj_prediction[0]:
        word_predictions.append(subj_obj_rnn.INDEX2TAG[numpy.argmax(word_prediction)])

    objects, subjects = [], []
    obj, subj = u'', u''
    for word, prediction in zip(tokenize(sentence), word_predictions):
        if prediction == subj_obj_rnn.SUBJ_TAG:
            subj += word + u' '

        elif subj != u'':
            subjects.append(subj.rstrip())
            subj = u''

        if prediction == subj_obj_rnn.OBJ_TAG:
            obj += word + u' '
        elif obj != u'':
            objects.append(obj.rstrip())
            obj = u''

    if obj != u'':
        objects.append(obj.rstrip())
    if subj != u'':
        subjects.append(subj.rstrip())

    # check invalid results
    if (len(subjects) > 1 or len(objects) > 1) or (len(subjects) == 0 and len(objects) == 0):
        return None

    subj = subjects[0] if len(subjects) == 1 else None
    obj = objects[0] if len(objects) == 1 else None

    return subj, obj


def extract_patterns_from_non_understood_questions(threshold=MATCHED_THRESHOLD):
    print "Extracting data from db"
    rel2qas = database.get_unmatched_questions()

    rel2questions_yn, rel2questions_oe = {}, {}
    for relation, qas in rel2qas.items():
        for qa in qas:
            question, yes_no = qa[0], qa[1]
            if yes_no:
                rel2questions_yn[relation] = rel2questions_yn.get(relation, []) + [question.lstrip().rstrip()]
            else:
                rel2questions_oe[relation] = rel2questions_oe.get(relation, []) + [question.lstrip().rstrip()]

    # print "Yes No Question"
    rel2patterns_yn, rel2patterns_oe = {}, {}
    for relation, questions in rel2questions_yn.items():
        # print "\tRelation: ", relation
        valid_patterns_and_questions = extract_valid_patterns(questions=questions, relation=relation,
                                                              yes_no=True, threshold=threshold)

        valid_patterns_yn = []
        for pattern, validated_questions in valid_patterns_and_questions:
            patterns_manager.add_pattern(pattern=pattern, relation=relation, clean=False)
            valid_patterns_yn.append(pattern)
            for question in validated_questions:
                database.delete_unmatched_question(question=question, relation=relation)

        rel2patterns_yn[relation] = valid_patterns_yn

    # print "Open-Ended Question"
    for relation, questions in rel2questions_oe.items():
        # print "\tRelation: ", relation
        valid_patterns_and_questions = extract_valid_patterns(questions=questions, relation=relation,
                                                              yes_no=False, threshold=threshold)

        valid_patterns_oe = []
        for pattern, validated_questions in valid_patterns_and_questions:
            patterns_manager.add_pattern(pattern=pattern, relation=relation, clean=False)
            valid_patterns_oe.append(pattern)
            for question in validated_questions:
                database.delete_unmatched_question(question=question, relation=relation)

        rel2patterns_yn[relation] = valid_patterns_oe

    return rel2patterns_yn, rel2patterns_oe


def extract_valid_patterns(questions, relation, yes_no=None, threshold=MATCHED_THRESHOLD):
    meaningless_words = main_sense_interceptor.Relation2Stopwords[relation]
    case_stopwords = main_sense_interceptor.extract_stopwords_from_list(questions, complete=False)
    # import pprint
    # print "Case Stopwords"
    # pprint.pprint(case_stopwords)
    meaningless_words += case_stopwords

    occurrences = {}
    for question in questions:
        question = unicode(question).rstrip(u'? ').lstrip().lower()
        masked_sent = get_question_representation(question, meaningless_words=meaningless_words)
        le = occurrences.get(masked_sent, (0, list()))  # type: tuple[int, list]
        occurrences[unicode(masked_sent)] = (le[0] + 1, le[1] + [question])

    # pprint.pprint(occurrences)
    valid_patterns = []
    for pattern, info in occurrences.items():  # type: str, tuple
        questions_sensed = info[1]  # type: list

        if len(questions_sensed) > threshold:
            zeros = pattern.count(SENSE_PLACEHOLDER)
            masked_sent_regex = u'^' + pattern.replace(SENSE_PLACEHOLDER, '(.*?)') + u'$'
            sense_count_condition = zeros <= 2 if yes_no is None else zeros == 2 if yes_no else zeros == 1
            if sense_count_condition and masked_sent_regex not in valid_patterns:
                matched = 0
                structure = validate_structure(questions=questions_sensed, pattern=pattern,
                                               reg_expr=masked_sent_regex)

                if not structure:
                    continue

                # print masked_sent_regex
                for question in questions_sensed:
                    question = question.rstrip('? ')
                    # build regex
                    if regex.match(masked_sent_regex, question.lower()):
                        # base condition
                        if matched >= threshold:
                            # the pattern is valid
                            valid_patterns.append((structure + u' ?', questions))
                            break

                        # processing
                        found = regex.findall(masked_sent_regex, question.lower())
                        if found and len(found) > 0:
                            senses = {f: False for f in found if f != u''}
                        else:
                            continue

                        try:
                            doc = nlp(question)
                            for entity in doc.ents:
                                for sense in senses.keys():
                                    if entity.lower_ == sense:
                                        senses[sense] = True
                                        break

                            check_validation = False if False in senses.values() else True
                            if check_validation:
                                matched += 1
                                continue

                            dis_question = babelnet_client.disambiguate_sentence(question)
                            for token in dis_question:
                                for sense in senses:
                                    token_start_index = token['charFragment']['start']
                                    token_stop_index = token['charFragment']['end'] + 1
                                    dis_sense = question[token_start_index:token_stop_index]

                                    if sense == dis_sense:
                                        senses[sense] = True

                            check_validation = False if False in senses.values() else True
                            if check_validation:
                                matched += 1
                                continue

                        except UnicodeEncodeError:
                            pass

    return valid_patterns


class PatternMinerController(Thread):
    def __init__(self, matches_threshold=20, timer_interval=3600):
        super(PatternMinerController, self).__init__()
        self.matches_threshold = matches_threshold
        self.timer = Timer(timer_interval, self.__start_mining)
        self.event = Event()
        self.miner = PatternMiner(matches_threshold=self.matches_threshold)

    def __start_mining(self):
        if self.miner.isAlive():
            if self.miner.is_running():
                logger.debug("Miner is yet in action")
                pass
            else:
                logger.debug("Launching Miner")
                self.miner.start()
        else:
            self.miner = PatternMiner(matches_threshold=self.matches_threshold)
            self.miner.start()
            logger.debug("ReNewing Miner")

    def run(self):
        logger.debug("Running Pattern Miner Controller!")
        self.__start_mining()
        self.timer.start()
        while self.event.wait():
            pass


class PatternMiner(Thread):
    def __init__(self, matches_threshold=20):
        super(PatternMiner, self).__init__()
        self.threshold = matches_threshold
        self.__running = False

    def is_running(self):
        return self.__running

    def run(self):
        print "Running Miner"
        self.__running = True
        logger.debug("Miner Launched")

        rel2patterns_yn, rel2patterns_oe = extract_patterns_from_non_understood_questions(threshold=self.threshold)
        logger.debug("YesNo extracted patterns: " + str(rel2patterns_yn))
        logger.debug("Open-Ended extracted patterns: " + str(rel2patterns_oe))

        for rel, yn_pattern in rel2patterns_yn.items():
            logger.debug("Extracted YesNo patterns for " + rel + ": " + unicode(yn_pattern))
        for rel, oe_pattern in rel2patterns_oe.items():
            logger.debug("Extracted Open-Ended patterns for " + rel + ": " + unicode(oe_pattern))
        self.__running = False


if __name__ == '__main__':
    pm = PatternMinerController(matches_threshold=20, timer_interval=3600)
    pm.start()

    # import pprint
    # rel2patterns_yn_s, rel2patterns_oe_s = extract_patterns_from_non_understood_questions()
    # print
    # print
    # print "Yes No Patterns"
    # pprint.pprint(rel2patterns_yn_s)
    # print "OE Patterns"
    # pprint.pprint(rel2patterns_oe_s)

    # questions = [qa[0] for qa in database.get_all_qas(u'COLOR')]
    # expatterns = extract_valid_patterns(questions=questions, relation=u'COLOR')
    # pprint.pprint(expatterns)
