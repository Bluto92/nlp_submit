# coding=utf-8
import logging
from src.utils import database, domains_utils, babelnet_client
from tqdm import tqdm
import string
from nltk.corpus import stopwords
from nltk.tokenize import WordPunctTokenizer
from nltk.stem.snowball import EnglishStemmer
import spacy

RELATION = u'PART'
SW = stopwords.words('english')
stemmer = EnglishStemmer()

logging.basicConfig(filename='../logs/tgrambot.log', level=logging.DEBUG,
                    format='%(asctime)s - %(name)s -  %(message)s')
logger = logging.getLogger("main sense interceptor")

nlp = spacy.load('en_core_web_md')


def stemming_tokenizer(text):
    stemmed_text = []
    for word in WordPunctTokenizer().tokenize(text):
        stemmed_word = stemmer.stem(word)
        stemmed_text.append(stemmed_word)
    return stemmed_text


def merge_(doc, start, end):
    """
    merges two nodes in the tree

    :param doc: spacy document
    :param int start:
    :param int end:
    :return: merged token
    """
    if start < end:
        span = doc[start: end]
    else:
        span = doc[end: start]

    token = span.merge()
    return token


def best_candidate(user_question, relation, yes_no=False):
    """
    Finds the best candidate(s) for the given user question.
    It extracts the sense from the question by stripping all the stopwords - and the per-relation stopwords -
    and looks for the question in the database that shares that sense. Then, It computes the Jaccard distance
    (Intersection / Union) between the user-question-sense and each knowledge-question-sense.
    It returns an answer build on the list of best candidates (the Question-Answer pairs whose Question-Jaccard score
    was the higher), or None if it was not possible to extract any QA pair from the database that shares the
    User-Question Sense.

    :param str | unicode user_question:
    :param str | unicode relation:
    :param bool yes_no:
    :return:
    :rtype: unicode
    """
    uq_tokens = filter_sent(tokens=spacy_sense(user_question), relation=relation)
    uq_bab_tokens = filter_sent(tokens=babelnet_sense(user_question), relation=relation)
    if not uq_bab_tokens:
        return None, 1.

    qas = database.get_containing_words_in_question_qas(relation=relation, words=uq_bab_tokens, yes_no=yes_no)
    if not qas:
        qas = database.get_containing_words_in_question_qas(relation=relation, words=uq_bab_tokens, any=True,
                                                            yes_no=yes_no)

    """    
    Database calls return a collection of QA pairs in which was possible to find all (fist call) - or at least one if 
    previous call returns empty collection- the tokens of the main sense of the user question.
    the big problem is that it searches on plain text (i.e. str.find()), this means that if I look for 'car' I will
    have a match with 'card' or 'carillon' or 'carrion'.
    So as first thing we can try to filter those results without using spacy - that is heavy.  
    """
    tokenizer = WordPunctTokenizer()
    uq_tokens_wpt = set(filter_sent(tokens=tokenizer.tokenize(user_question), relation=relation))
    filtered_qas = []
    for qa in qas:
        question = qa[0].lower()
        qa_tokens = set(tokenizer.tokenize(question))
        if len(qa_tokens.intersection(set(uq_tokens_wpt))) > 0:
            filtered_qas.append(qa)

    """
    use spacy analysis on the filtered qas
    """
    candidate_qas = {}
    for qa in filtered_qas:
        question = qa[0].lower()
        try:
            tokenized_sent = spacy_sense(question)
            filtered_tokens = filter_sent(tokens=tokenized_sent, relation=relation)
            if filtered_tokens and len(filtered_tokens) > 0:
                similarity = jaccard_similarity(filtered_tokens, uq_tokens)
                candidate_qas.setdefault(similarity, []).append(qa)
        except Exception, e:
            print "Main Sense 1", e

    """
    use babelnet analysis on the remaining qas
    """
    candidates = {}
    if len(candidate_qas) > 0:
        remaining_qas = candidate_qas.get(max(candidate_qas.keys()), [])
        for qa in remaining_qas:
            question = qa[0].lower()
            answer = qa[1]
            try:
                tokenized_sent = babelnet_sense(question)
                filtered_tokens = filter_sent(tokens=tokenized_sent, relation=relation)
                similarity = jaccard_similarity(filtered_tokens, uq_tokens)
                candidates[similarity] = candidates.get(similarity, []) + [(question, answer)]
            except Exception, e:
                print "Main Sense 2", e

    if len(candidates) > 0:
        max_similarity = max(candidates.keys())
        logger.debug("BestCandidate: Max Similarity: " + str(max_similarity))
        logger.debug("BestCandidate: BestCandidates: " + str(
            [(candidate[0], candidate[1]) for candidate in candidates[max_similarity]]))

        answers = set([candidate[1] for candidate in candidates[max_similarity]])

        # check to avoid answers like 'yes, no'
        lower_answers = [answer.lower() for answer in answers]
        if u'yes' in lower_answers and u'no' in lower_answers:
            return None, 1.

        answer = u''
        for candidate_answer in answers:
            answer += candidate_answer + u', '
        answer = answer.rstrip(', ')
        logger.debug("BestCandidate: Answer: " + answer)
        logger.debug("BestCandidate: Score: " + str(max_similarity))
        return answer, max_similarity

    else:
        return None, 1.


def extract_stopwords_from_list(sentences, complete=True):
    tokenizer = WordPunctTokenizer()

    occurrence_list = {}
    for sentence in sentences:
        sentence = sentence.lower().rstrip(' ?')
        for qToken in tokenizer.tokenize(sentence):
            if qToken not in string.punctuation:
                occurrence_list[qToken] = occurrence_list.get(qToken, 0) + 1

    import numpy
    occurrences = numpy.asarray(occurrence_list.values(), dtype=numpy.uint32)
    from sklearn.cluster import KMeans
    k_means = KMeans(n_clusters=2).fit(X=occurrences.reshape(-1, 1))

    words = [word for word, label in zip(occurrence_list.keys(), k_means.labels_) if label == 1]
    logging.debug("extracted stopwords: " + str(words))

    if complete:
        sw = set(SW).union(set(words)).union(set(list(string.punctuation)))
        words = list(sw)

    return sorted(words)


def extract_stopwords_per_relations(relations, complete=True, progbar=True):
    relation2stopwords = {}
    if progbar:
        td = tqdm(total=len(relations))

    for relation in relations:
        if progbar:
            td.desc = relation.lower() + u' '
            td.refresh()

        counter = 0
        # sometimes it extracts a lot of words due to some random phase in clustering algorithm,
        # but most of the time it performs well
        while (relation not in relation2stopwords.keys() or len(relation2stopwords[relation]) > 100 + len(
                stopwords.words('english'))) and counter <= 5:
            logger.debug("Counter: " + str(counter))
            logger.debug("Relation2Stopwords[" + relation.lower() + "]: " + str(
                len(relation2stopwords.setdefault(relation, []))))

            counter += 1
            qas = database.get_all_qas(relation=relation, schema=database.ENRICHING_SCHEMA)
            questions = [qa[0] for qa in qas]
            relation2stopwords[relation] = extract_stopwords_from_list(questions, complete=complete)

        if progbar:
            td.update(1)
    return relation2stopwords


def extract_stopwords_per_relation(relation, complete=True):
    logging.debug('Extracting stopwords for the relation: ' + relation.lower())
    qas = database.get_all_qas(relation=relation, schema=database.ENRICHING_SCHEMA)
    questions = [qa[0] for qa in qas]
    return extract_stopwords_from_list(questions, complete=complete)


logger.debug("Extracting Stopwords per relation")
# Relation2Stopwords = extract_stopwords_per_relations([RELATION], progbar=True)
# Relation2Stopwords = extract_stopwords_per_relations(domains_utils.get_relations_for_domain_kbs('Education'),
#                                                      progbar=True)
Relation2Stopwords = extract_stopwords_per_relations(domains_utils.get_all_relations_kbs(), progbar=True)


# Relation2Stopwords = {}


def filter_sent(tokens, relation):
    ftokens = set()

    # filter tokens
    for token in tokens:
        # if stemming_tokenizer(token.lower())[0] not in extract_stopwords_per_relation(relation) \
        stemmed_tokens = stemming_tokenizer(token.lower())
        if len(stemmed_tokens) == 1 and stemmed_tokens[0] not in Relation2Stopwords[relation] \
                and token.lower() not in string.punctuation:
            ftokens.add(token)

    return ftokens


def jaccard_similarity(tokens1, tokens2):
    """

    :param tokens1:
    :param tokens2:
    :return:
    """
    tokens1 = set(tokens1)
    tokens2 = set(tokens2)

    intersection = len(tokens1.intersection(tokens2))
    union = len(tokens1.union(tokens2))

    return float(intersection) / union


def spacy_sense(question):
    question.rstrip('? ')
    # merge compounds and entities
    doc = nlp(question)
    for ent in doc.ents:
        merge_(doc, ent.start, ent.end)

    modified = True
    while modified:
        modified = False
        for token in doc:
            if token.dep_ == 'compound':
                head = token.head
                if head.i < token.i:
                    merge_(doc, head.i, token.i + 1)
                else:
                    merge_(doc, token.i, head.i + 1)
                modified = True
                break

    tokens = [token.lower_ for token in doc]
    return tokens


def babelnet_sense(question):
    tokens = babelnet_client.disambiguate_sentence(question)
    # pprint.pprint(tokens)
    # print

    tokens_indexes = {}
    for token in tokens:
        if token['tokenFragment']['start'] not in tokens_indexes.keys() or tokens_indexes[
            token['tokenFragment']['start']] < token['tokenFragment']['end']:
            tokens_indexes[token['tokenFragment']['start']] = token['tokenFragment']['end']

    # greedy: keep largest interval
    clean_tokens = {}
    while len(tokens_indexes) > 0:
        largest_key = 0
        largest_distance = -1
        for start, end in tokens_indexes.items():
            if largest_distance < end - start:
                largest_distance = end - start
                largest_key = start

        clean_tokens[largest_key] = tokens_indexes.pop(largest_key)

        # clean tokens_indexes
        to_clean_start, to_clean_end = largest_key, clean_tokens[largest_key]
        clean_tokens_indexes = {}
        for start, end in tokens_indexes.items():
            if not (to_clean_start <= start <= to_clean_end or to_clean_start <= start <= to_clean_end):
                clean_tokens_indexes[start] = end

        tokens_indexes = clean_tokens_indexes

    splits = question.split()
    final_tokens = []
    for start, end in clean_tokens.items():
        entity = u''
        for word in splits[start: end + 1]:
            entity += word + u' '
        final_tokens.append(entity.rstrip())

    return final_tokens


if __name__ == '__main__':
    # stopwords = Relation2Stopwords[u'COLOR']

    Question = u'when died napoleone ?'

    Candidate = best_candidate(Question, u'TIME', yes_no=False)

    print Candidate
