import pickle

import keras
import numpy

from src.classifiers.unused import yes_no_rnn
from ..ai import main_sense_interceptor
from ..classifiers.relation_classifier import ClassifierLoader
# from sklearn.pipeline import Pipeline
from ..utils import database

RELATION = u'GENERALIZATION'


def yes_no_answer(relation, question, model, question_w2i, answer_i2w):
    # Pattern Analysis
    answer = yes_no_rnn.answer_through_patterns_recognition(relation=relation, question=question)
    if answer is not None:
        print answer, "[PR]"
        return
    else:
        # un-matched pattern: save the query in the Database
        database.add_unmatched_query(predicted_relation=relation, question=question, yes_no=True)

    # RNN
    if False:
        encoded_question = yes_no_rnn.encode_question(question=question, w2i=question_w2i)
        encoded_answers = model.predict(encoded_question)
        answer = ""
        for encoded_answer in encoded_answers:
            for word_distribution in encoded_answer:
                word_index = numpy.argmax(word_distribution)
                word = answer_i2w.get(word_index, ' UNK ')
                if word not in ['<START>', '<STOP>', '<PAD>', '<UNK>']:
                    answer += word
            print
            print "I would guess ", answer, "\t[RNN]"


def ask_queries():
    model_dir = "/home/fra/Projects/Universita/NLP/FinalProject/models/yesno/"
    model_name = "yes_no_generalization-17-a0.9843-l0.0659-va0.9843-vl0.0738.keras"
    question_w2i_name = "yes_no_generalization_question_w2i.pickle"
    answer_w2i_name = "yes_no_generalization_answer_w2i.pickle"

    classifiers_dir = "/home/fra/Projects/Universita/NLP/FinalProject/classifiers/"
    classifier_name = "question_type_classifier.pickle"

    print "Loading Classifier"
    question_type_classifier = ClassifierLoader('question_type_classifier_all.pickle').load_classifier()

    # print "Loading W2I/I2W"
    QuestionW2I = pickle.load(open(model_dir + question_w2i_name, mode="r"))
    AnswerW2I = pickle.load(open(model_dir + answer_w2i_name, mode="r"))
    AnswerI2W = {index: word for word, index in AnswerW2I.items()}

    # print "Loading Keras Model: ", model_name
    model = keras.models.load_model(model_dir + model_name)
    #
    # print "Model startup"
    model.predict(yes_no_rnn.encode_question(question="startup", w2i=QuestionW2I))

    relation_classifier = ClassifierLoader('relation_classifier_education_0.99.pickle').load_classifier()
    domain = 'Education'
    from ..utils import domains_utils
    relations = domains_utils.get_relations_for_domain_kbs(domain=domain)
    print "Possible relation for domain '", domain, "' are: "
    for relation in relations:
        print relation,
    print
    while True:
        # question
        question = raw_input('Ask a Question: ').lstrip(' ').rstrip(' ').lower()
        if question == "quit":
            break

        relation = relations[relation_classifier.predict([question])[0]]

        print "Predicted Relation ", relation

        # check if I have the raw question in the KB
        answer = yes_no_rnn.answer_full_query_recognition(relation=relation, question=question)
        if answer is not None:
            print answer, "[FQ]"
            continue

        yes_no_flag = question_type_classifier.predict([question])[0]
        if yes_no_flag:
            yes_no_answer(relation=u'GENERALIZATION', question=question, model=None,
                          question_w2i=QuestionW2I, answer_i2w=AnswerI2W)
        else:
            answer = main_sense_interceptor.best_candidate(question, relation=relation, yes_no=False)
            print answer


if __name__ == '__main__':
    # create_yes_no_model(RELATION)
    ask_queries()
