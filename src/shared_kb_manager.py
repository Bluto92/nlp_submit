import requests
import ujson
from utils.babelnet_client import KEY
from utils import database, babelnet_client, sentence_manipulation
from threading import Thread, Event, Timer
import logging

logging.basicConfig(filename='../logs/tgrambot.log', level=logging.DEBUG,
                    format='%(asctime)s - %(name)s -  %(message)s')
logger = logging.getLogger(__name__)
SERVER_ADDRESS = "http://151.100.179.26:8080/KnowledgeBaseServer/rest-api/"
STATUS_FILE = "../data/KB_STATUS"
DEFAULT_WAIT_TIME = 3600  # 1 hour


def build_kb_data(question, answer, relation, domains=None, c1=None, c2=None, context=None):
    return {'question': question, 'answer': answer, 'relation': relation, 'domains': domains if domains else '',
            'c1': c1 if c1 else '', 'c2': c2 if c2 else '', 'context': context if context else answer}


def add_test(kb_data):
    action_url = SERVER_ADDRESS + "add_item_test"
    params = 'key=' + KEY
    data = ujson.encode(kb_data, ensure_ascii=False)
    get_res = requests.post(action_url, data=data, params=params)
    response_text = get_res.text
    return response_text


def add_item(kb_data):
    action_url = SERVER_ADDRESS + "add_item"
    params = 'key=' + KEY
    data = ujson.encode(kb_data, ensure_ascii=False)
    get_res = requests.post(action_url, data=data, params=params)
    response_text = get_res.text
    return response_text


def extract_field_synset(c):
    """

    :param str c:
    :return:
    """
    if '::' in c:
        splits = c.split('::')
        field = splits[0]
        synset = splits[1]
        return field, synset
    elif babelnet_client.is_valid_synset(c):
        return None, c
    else:
        return c, None


class KBUpdater(Thread):
    def __init__(self, wait_time=DEFAULT_WAIT_TIME):
        """

        :param int wait_time:
        """
        super(KBUpdater, self).__init__()
        self.__wait_time = wait_time
        self.__stop = Event()
        self.__pause = False
        self.__timer = Timer(self.__wait_time, self.update_db)
        print "KBUpdater Created"

    def pause(self, value=True):
        self.__pause = value

    def run(self):
        logger.info("Running Updating Knowledge Base")
        print "Running UKB"
        self.update_db()
        
        while self.__stop.wait():
            pass

    def stop(self):
        logger.info("Stopping Knowledge Base Updater")
        self.__stop.clear()

    def join(self, timeout=None):
        self.join(timeout=timeout)

    def __refresh_timer(self):
        if self.__timer.isAlive():
            self.__timer.cancel()
        self.__timer = Timer(self.__wait_time, self.update_db)
        self.__timer.start()

    def update_db(self):
        print "KBUpdater updating now man!"
        if self.__timer.isAlive():
            self.__timer.cancel()

        if self.__pause:
            self.__refresh_timer()
            return

        status_file = open(STATUS_FILE, mode='r')
        last_id = status_file.read(200)
        status_file.close()
        logger.info("Knowledge Base Updater: actual status " + last_id)

        action_url = SERVER_ADDRESS + "items_from/"
        params = 'key=' + KEY + "&id=" + last_id
        get_res = requests.get(url=action_url, params=params)
        response_text = get_res.text
        new_items = ujson.decode(response_text)
        last_id = int(last_id)

        old_id = last_id
        for item in new_items:
            subj, subj_synset = extract_field_synset(item['c1'])
            obj, obj_synset = extract_field_synset(item['c2'])
            subj_quest, subj_ans, obj_quest, obj_ans = None, None, None, None
            if subj:
                subj_quest = len(
                    sentence_manipulation.find_words(sentence=item['question'], sub_sentence=subj)) == 1
                subj_ans = len(
                    sentence_manipulation.find_words(sentence=item['answer'], sub_sentence=subj)) == 1
            if obj:
                obj_quest = len(
                    sentence_manipulation.find_words(sentence=item['question'], sub_sentence=obj)) == 1
                obj_ans = len(sentence_manipulation.find_words(sentence=item['answer'], sub_sentence=obj)) == 1

            for schema in [database.QUERING_SCHEMA, database.ENRICHING_SCHEMA]:
                database.add_qa_data(schema=schema,
                                     relation=item['relation'], question=item['question'], answer=item['answer'],
                                     subject=subj, subject_synset=subj_synset, subj_quest=subj_quest, subj_ans=subj_ans,
                                     object=obj, object_synset=obj_synset, obj_quest=obj_quest, obj_ans=obj_ans,
                                     yes_no=subj_quest and obj_quest)

            last_id += 1
            status_file = open(STATUS_FILE, mode='w')
            status_file.write(str(last_id))
            status_file.close()

        self.__refresh_timer()
        logger.info("Knowledge Base Updater: updated " + str(last_id - old_id) + ". New status id: " + str(last_id))
        return response_text


if __name__ == '__main__':
    KBUpdater().update_db()
