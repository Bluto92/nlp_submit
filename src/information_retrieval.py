from tqdm import tqdm
from whoosh.index import *
from whoosh.fields import *
from whoosh.qparser import QueryParser
from nltk.corpus import stopwords
from nltk.tokenize import WordPunctTokenizer
import numpy
from utils import database, domains_utils

IR_DIR = "../IR/"
INDEX_NAME = "index.ir"


def open_index(ir_dir=IR_DIR, index_name=INDEX_NAME):
    ix = open_dir(ir_dir, indexname=index_name)
    return ix


def create_index(ir_dir=IR_DIR, index_name=INDEX_NAME):
    # schema = Schema(
    #     title=TEXT(stored=True, analyzer=analysis.StandardAnalyzer(stoplist=string.punctuation), ),
    #     path=ID(stored=True, analyzer=analysis.StandardAnalyzer(stoplist=string.punctuation)),
    #     context=TEXT(stored=True, analyzer=analysis.StandardAnalyzer(stoplist=string.punctuation)),
    #     domains=TEXT(stored=True, analyzer=analysis.StandardAnalyzer(stoplist=string.punctuation)),
    #     relation=TEXT(stored=True, analyzer=analysis.StandardAnalyzer(stoplist=string.punctuation)))

    analyzer = analysis.StandardAnalyzer(stoplist=None) | analysis.LowercaseFilter()

    schema = Schema(
        title=TEXT(stored=True, multitoken_query='phrase', lang='en', analyzer=analyzer),
        # path=ID(stored=True, analyzer=analyzer),
        context=TEXT(stored=True, multitoken_query='phrase', lang='en', analyzer=analyzer),
        # domains=TEXT(stored=True, analyzer=analyzer),
        relation=TEXT(stored=True, analyzer=analyzer))

    ix = create_in(ir_dir, schema=schema, indexname=index_name)
    return ix


def populate_index(index):
    """

    :param whoosh.index.index index:
    :return:
    """
    relations = domains_utils.get_all_relations_kbs()
    with tqdm(total=len(relations)) as tm:
        with index.writer(procs=6) as writer:
            for relation in relations:
                tm.desc = relation.lower() + u' '
                tm.refresh()
                for qa in database.get_all_qas(relation, schema=database.ENRICHING_SCHEMA):
                    question = unicode(qa[0])
                    answer = unicode(qa[1])
                    relation = unicode(relation)

                    # Title
                    writer.add_document(title=question,
                                        context=answer,
                                        relation=relation, )
                tm.update(1)


def is_good_result_n_words(query_str, result, factor=2):
    word_in_query = len(query_str)
    # word_in_query = tokenized_query_len(query_str)
    return result.matched_terms() > numpy.floor(float(word_in_query) / factor)


def tokenized_query_len(query_str):
    tokenizer = WordPunctTokenizer()
    word_in_query = len(tokenizer.tokenize(query_str))
    return word_in_query


def tokenized_query(query_str, filter_stopwords=False, lower=True):
    tokenizer = WordPunctTokenizer()
    query_tokens = tokenizer.tokenize(query_str)

    if lower:
        query_lst_lower = [word.lower() for word in query_tokens]
        query_tokens = query_lst_lower

    if filter_stopwords:
        filtered = [unicode(word) for word in query_tokens if word not in stopwords.words('english')]
        return filtered
    else:
        return query_tokens


def perfect_phrase_matching(index, phrase, slop=1):
    phrase_tokens = tokenized_query(phrase.rstrip(' ?'))
    with index.searcher() as index_searcher:
        from whoosh import query
        terms = []
        for term in phrase_tokens:
            t1 = query.Term("title", term)
            terms.append(t1)

        query = query.SpanNear2(terms, slop=slop)

        print "Query List", phrase_tokens
        print "Query: ", query
        results = index_searcher.search(query, limit=5, terms=True)

        query_tokens = set(phrase_tokens)
        for top in results:
            print "\t", top
            print "\tMatched Terms (", str(len(top.matched_terms())), "/", str(len(query_tokens)), '): ', \
                top.matched_terms()
            print "\tScore: ", str(top.score)

        return results


def classic_terms_query(index, phrase):
    with index.searcher() as searcher:
        phrase_tokens = tokenized_query(phrase, filter_stopwords=False)

        qp = QueryParser("title", schema=Index.schema,)
        query = qp.parse(query_str.strip(string.punctuation))

        print "Performing query", query
        results = searcher.search(query, limit=5, terms=True)

        for top in results:
            print "\t", top
            print "\tMatched Terms (", str(len(top.matched_terms())), "/", str(len(phrase_tokens)), '): ',\
                top.matched_terms()
            print "\tScore: ", str(top.score)

    return results


if __name__ == '__main__':
    if not os.path.exists(IR_DIR):
        os.mkdir(IR_DIR)

    query_str = u'is metallic color a color?'
    import string

    print "Query str: ", query_str
    # Index = create_index()
    # populate_index(Index)

    Index = open_index()
    print "Index Opened"

    classic_terms_query(Index, query_str)
    print
    perfect_phrase_matching(Index, query_str)
