from utils import domains_utils, database, patterns_manager
from nltk.tokenize import WordPunctTokenizer

# RNN
from rnn import subj_obj_rnn, glovembedder_runtime
import keras
import numpy

# disambiguation
import ujson
import requests
from utils import babelnet_client
import spacy
from utils import sentence_manipulation
# from nltk.corpus import stopwords
from ai.main_sense_interceptor import Relation2Stopwords
from utils.sentence_manipulation import find_words
import shared_kb_manager

import random
import logging

# noinspection PyUnresolvedReferences
from classifiers.ynm_classifier import stemming_tokenizer, CLASSIFIER_PATH
import pickle

logging.basicConfig(filename='../logs/tgrambot.log', level=logging.DEBUG,
                    format='%(asctime)s - %(name)s -  %(message)s')
logger = logging.getLogger(__name__)

SORNN_DIR = "../models/sofinder/answers/all/"
SORNN_NAME = "subj_obj_finder_answers_all-00-a0.9835-l0.0984-va0.9965-vl0.0436-f1-Obj0.9939-f1-Subj0.9892.keras"


class Enricher(object):
    def __init__(self):
        logger.debug("Loading Subj Obj RNN")
        self.SubjObjRNN = keras.models.load_model(SORNN_DIR + SORNN_NAME)
        logger.debug("Loading Spacy")
        self.nlp = spacy.load('en_core_web_md')

        self.tokenize = WordPunctTokenizer().tokenize
        self.YesNoMaybeClassifier = pickle.load(open(CLASSIFIER_PATH, mode='r'))
        pass

    def subj_question(self, domain, chosen_relation):
        relations = domains_utils.get_relations_for_domain_kbs(domain=domain)

        # retrieve the domain concepts
        domain_concepts = set()
        positive_relations = [rel for rel in relations if rel != chosen_relation]
        for relation in positive_relations:
            subjs_objs = database.get_all_subj_obj(relation=relation, disambiguated=True)
            subjs = [subj_obj for subj_obj in subjs_objs]
            domain_concepts = domain_concepts.union(set(subjs))

        # remove the chosen relation concepts
        subjs_objs = database.get_all_subj_obj(relation=chosen_relation, disambiguated=True)
        subjs = [subj_obj for subj_obj in subjs_objs]

        domain_concepts = domain_concepts.difference(set(subjs))

        # choose concept
        subject = None
        while not subject:
            random_concept = list(domain_concepts)[random.randint(0, len(domain_concepts) - 1)]
            subject = self.check_and_strip(relation=chosen_relation, sentence=random_concept, babelnet=True)
            if not subject:
                domain_concepts.remove(random_concept)
            logger.debug("Subject -- " + random_concept + " -- is not a good subject :(")

        # choose a random pattern
        patterns = patterns_manager.get_patterns(relation=chosen_relation, subj=True, obj=False)
        pattern = list(patterns)[random.randint(0, len(patterns) - 1)]  # type: unicode

        # compose question
        question = pattern.replace(patterns_manager.SUBJ_PLACEHOLDER, subject)

        # create Enriching Data
        enriching_data = EnrichingData(domain=domain, relation=chosen_relation,
                                       question=question, subject=subject)

        # get synset Id
        enriching_data.subject_synset = self.disambiguate_concept(enriching_data.question, enriching_data.subject)

        return enriching_data

    def ask_question(self, domain):
        domain_relations = domains_utils.get_relations_for_domain_kbs(domain)
        # check for user unanswered queries
        unanswered_queries_subj = database.get_unanswered_questions(relations=domain_relations, yes_no=False)
        unanswered_queries_yn = database.get_unanswered_questions(relations=domain_relations, yes_no=True)
        unanswered_queries = unanswered_queries_yn + unanswered_queries_subj
        if unanswered_queries and len(unanswered_queries) > 0:
            query_index = random.randint(0, len(unanswered_queries) - 1)
            query = unanswered_queries[query_index]
            question = query[1]
            subject = query[2]
            chosen_relation = query[0]
            yes_no = query[6]

            database.delete_unanswered_queries(question, chosen_relation)

            # create Enriching Data
            enriching_data = EnrichingData(domain=domain, relation=chosen_relation, question=question, yes_no=yes_no,
                                           subject=subject, subject_synset=query[4], question_pattern=query[7])

            if yes_no:
                validated_object = query[3]
                if validated_object:
                    enriching_data.validated_object = validated_object

                validated_object_synset = query[5]
                if validated_object_synset:
                    enriching_data.validated_object_synset = validated_object_synset

            return enriching_data

        relations = domains_utils.get_relations_for_domain_kbs(domain=domain)
        # choose a relation. Heuristic: randomly choose one in the ones you know less
        relation2size = {relation: database.get_qas_size(relation=relation, yes_no=None) for relation in relations}
        min_relations = [(relation, size) for relation, size in relation2size.items()]
        min_relations = sorted(min_relations, key=lambda x: x[1], reverse=False)
        min_relations = min_relations[:len(min_relations) / 2 if len(min_relations) > 1 else 1]
        chosen_relation = min_relations[random.randint(0, len(min_relations) - 1)][0]

        logger.debug("Domain:" + domain)
        logger.debug("Chosen Relation:" + chosen_relation)

        # return question
        return self.subj_question(domain, chosen_relation)

    def disambiguate_concept(self, sentence, concept):
        disambiguated_sentence = self.disambiguate_sentence(sentence)
        for token in disambiguated_sentence:
            token_start_index = token['charFragment']['start']
            token_stop_index = token['charFragment']['end']
            token_in_question = sentence[token_start_index:token_stop_index]
            if token_in_question == concept:
                return token['babelSynsetID']
        return None

    def consume_answer(self, enriching_data):
        # clean answer
        enriching_data.user_answer = enriching_data.user_answer.rstrip().lstrip()

        enriching_data = self.process_answer(enriching_data=enriching_data)

        if (enriching_data.yes_no and 0 <= enriching_data.ynm_prediction <= 1) or (
                    not enriching_data.yes_no and enriching_data.validated_object):
            database.add_qa_data(enriching_data.relation, enriching_data.question, enriching_data.user_answer,
                                 schema=database.QUERING_SCHEMA, subject=enriching_data.subject,
                                 object=enriching_data.validated_object, subject_synset=enriching_data.subject_synset,
                                 object_synset=enriching_data.validated_object_synset,
                                 obj_quest=enriching_data.yes_no, subj_ans=enriching_data.subj_ans,
                                 obj_ans=len(sentence_manipulation.find_words(enriching_data.validated_object,
                                                                              enriching_data.user_answer)) > 0,
                                 yes_no=enriching_data.yes_no,
                                 question_pattern=enriching_data.question_pattern)

            database.add_qa_data(enriching_data.relation, enriching_data.question, enriching_data.user_answer,
                                 schema=database.ENRICHING_SCHEMA, subject=enriching_data.subject,
                                 object=enriching_data.validated_object, subject_synset=enriching_data.subject_synset,
                                 object_synset=enriching_data.validated_object_synset,
                                 obj_quest=enriching_data.yes_no, subj_ans=enriching_data.subj_ans,
                                 obj_ans=len(sentence_manipulation.find_words(enriching_data.validated_object,
                                                                              enriching_data.user_answer)) > 0,
                                 yes_no=enriching_data.yes_no,
                                 question_pattern=enriching_data.question_pattern)

            c1 = enriching_data.subject
            c1 += "::" + enriching_data.subject_synset if enriching_data.subject_synset else ''

            validated_object_synset = enriching_data.validated_object_synset
            c2 = enriching_data.validated_object
            c2 += "::" + validated_object_synset if validated_object_synset else ''

            kb_data = shared_kb_manager.build_kb_data(question=enriching_data.question,
                                                      answer=enriching_data.user_answer,
                                                      relation=enriching_data.relation,
                                                      c1=c1, c2=c2, )
            shared_kb_manager.add_item(kb_data)
            # shared_kb_manager.add_test(kb_data)

        return enriching_data

    def process_answer(self, enriching_data):
        """

        :param EnrichingData enriching_data:
        :return:
        """
        if enriching_data.yes_no:
            prediction = self.YesNoMaybeClassifier.predict([enriching_data.user_answer])
            enriching_data.ynm_prediction = prediction[0]
            logger.debug("Answer predicted as " + str(enriching_data.ynm_prediction))
            return enriching_data
        else:
            relation, answer, subject = enriching_data.relation, enriching_data.user_answer, enriching_data.subject
            # 1- it may be a factoid
            validated_answer = self.check_and_strip(relation, answer.lower())
            if validated_answer:
                enriching_data.validated_object = validated_answer
                return enriching_data

            # 2 - Try with CONCEPT-VERB-CONCEPT parsing
            doc = self.nlp(unicode(enriching_data.user_answer))

            root = None
            verbs = []
            for token in doc:
                if token.dep_ == u'ROOT' and token.pos_ == u'VERB':
                    root = token
                elif token.pos_ == u'VERB':
                    verbs.append(token)

            if root:
                for root in [root] + verbs:
                    for child in root.children:
                        if child.pos_ != u'VERB' and child.pos_ != u'NOUN' and child.pos_ != u"PROPN" \
                                and child.pos_ != u'PRON' and child.dep_ != u'acomp':
                            # it must not be a nominal verb (i.e. 'is green' must not be merged)
                            self.merge_(doc, child.i, root.i)

                    left_side = doc[0:root.i]
                    verb = root
                    right_side = doc[root.i + 1:]

                    print "Left Side: ", left_side
                    print "Verb: ", verb
                    print "Right Side: ", right_side

                    subject = None
                    for child in root.lefts:
                        if child.pos_ != u'NOUN' or child.pos_ != u"PROPN" or child.pos_ != u'PRON':
                            subject = child
                            break

                    if subject:
                        for child in subject.children:
                            if child.dep_ == u'compound' or child.dep_ == u'nummod':
                                self.merge_(doc, subject.i, child.i)

                    obj = None
                    for child in root.rights:
                        if child.pos_ == u'NOUN' or child.pos_ == u"PROPN" or child.pos_ == u'VERB' \
                                or child.dep_ == u'acomp':
                            obj = child
                            break
                    if obj:
                        for child in obj.children:
                            if child.dep_ == u'compound' or child.dep_ == u'nummod':
                                self.merge_(doc, obj.i, child.i)

                        validated_object = self.check_and_strip(relation, obj.orth_)
                        if validated_object:
                            enriching_data.validated_object = validated_object
                            return enriching_data

            # disambiguate object
            if enriching_data.validated_object:
                enriching_data.validated_object_synset = self.disambiguate_concept(enriching_data.user_answer,
                                                                                   enriching_data.validated_object)

            # check if subject is in answer
            enriching_data.subj_ans = len(find_words(enriching_data.user_answer, enriching_data.subject)) > 0

        return enriching_data

    def subj_obj_prediction(self, answer):
        subj_obj_prediction = self.SubjObjRNN.predict(glovembedder_runtime.encode_sentence(answer))
        word_predictions = []
        for word_prediction in subj_obj_prediction[0]:
            word_predictions.append(subj_obj_rnn.INDEX2TAG[numpy.argmax(word_prediction)])

        objects, subjects = [], []
        obj, subj = u'', u''
        for word, prediction in zip(self.tokenize(answer), word_predictions):
            logger.debug("[SOP] " + unicode(word) + " " + unicode(prediction))
            if prediction == subj_obj_rnn.SUBJ_TAG:
                subj += word + u' '
                logger.debug("[SOP] Detected Subject word: " + unicode(word) + " " + unicode(prediction))
            elif subj != u'':
                subjects.append(subj.rstrip())
                logger.debug("[SOP] Detected end of Subject: " + unicode(subj.rstrip()))
                subj = u''

            if prediction == subj_obj_rnn.OBJ_TAG:
                obj += word + u' '
                logger.debug("[SOP] Detected Object word: " + unicode(word) + " " + unicode(prediction))
            elif obj != u'':
                objects.append(obj.rstrip())
                logger.debug("[SOP] Detected end of Object: " + unicode(obj.rstrip()))
                obj = u''

        logger.debug("[SOP] Subjects: " + unicode(subjects))
        logger.debug("[SOP] Objects: " + unicode(objects))

        # check invalid results
        if (len(subjects) > 1 or len(objects) > 1) or (len(subjects) == 0 and len(objects) == 0):
            logger.debug("[SOP] More than one Object/Subject")
            return None

        return subj, obj

    @staticmethod
    def merge_(doc, start, end):
        """
        merges two nodes in the tree

        :param doc: spacy document
        :param int start:
        :param int end:
        :return: merged token
        """
        if start < end:
            span = doc[start: end + 1]
        else:
            span = doc[end: start + 1]

        token = span.merge()
        return token

    @staticmethod
    def disambiguate_sentence(text):
        babelfy_url = u'https://babelfy.io/v1/disambiguate'
        # params = 'text=' + text + '&lang=EN&key=KEY'  # + KEY
        params = u'text=' + text + u'&lang=EN&key=' + babelnet_client.KEY
        get_res = requests.get(unicode(babelfy_url), params=unicode(params))
        response_text = get_res.text
        if get_res.status_code == 200:
            decoded = ujson.decode(response_text)
            if u'message' in decoded:
                raise StopIteration(decoded['message'])
        else:
            raise Exception(get_res.text)

        return ujson.decode(response_text)

    def validate_sentence(self, sub_sentence, babelnet=True):
        """

        :param str | unicode sub_sentence:
        :param bool babelnet:
        :return: Returns True if Spacy Entities or Babelnet recognize the sub-sentence
        :rtype: bool
        """
        validation = False
        logger.debug("Validating sentence: " + sub_sentence)
        doc = self.nlp(sub_sentence)
        for entity in doc.ents:
            if entity.lower_ == sub_sentence.lower():
                validation = True

        if babelnet:
            disambiguated_sentence = Enricher.disambiguate_sentence(sub_sentence)
            logging.debug('[BABELNET CLIENT] sentence: ' + sub_sentence + ' len: ' + str(len(sub_sentence)))
            for token in disambiguated_sentence:
                token_start_index = token['charFragment']['start']
                token_stop_index = token['charFragment']['end']
                logging.debug(
                    '[BABELNET CLIENT] start: ' + str(token_start_index) + ' end: ' + str(token_stop_index))

                if token_start_index == 0 and token_stop_index == len(sub_sentence) - 1:
                    # return token['babelSynsetID']
                    return True

            return validation

        return validation

    def check_and_strip(self, relation, sentence, babelnet=True):
        """

        :param unicode sentence:
        :param str | unicode relation:
        :param bool babelnet:

        :return: None or the validated sentence/sub-sentence
        :rtype: None | unicode
        """
        sentence_validated = False

        if sentence:
            logger.debug("CheckAndStrip: Checking sentence: " + sentence)
            # check if the given sentence may be a plausible sentence
            sentence_validated = self.validate_sentence(sentence, babelnet=babelnet)

            # if not try to strip from start and end the stopwords
            if not sentence_validated:
                # sentence = sentence_manipulation.strip(sentence, StopWords)
                sentence = sentence_manipulation.strip(sentence, Relation2Stopwords[relation])

                logger.debug("CheckAndStrip: Stripped sentence: " + sentence)

                # check again
                if sentence:
                    sentence_validated = self.validate_sentence(sentence, babelnet=babelnet)

        if not sentence_validated:
            sentence = None
            logger.debug("CheckAndStrip: Could not extract a valid sentence")
        else:
            logger.debug("CheckAndStrip: Valid object: " + sentence)

        return sentence


class EnrichingData(object):
    def __init__(self, domain, relation, question, subject, subject_synset=None, question_pattern=None, yes_no=False):
        self.domain = domain
        self.relation = relation
        self.question = question
        self.subject = subject
        self.subject_synset = subject_synset
        self.question_pattern = question_pattern
        self.yes_no = yes_no
        # process data
        self.ynm_prediction = -1
        self.validated_object = None
        self.user_answer = None
        self.disambiguated_answer = None
        self.validated_object_synset = None
        self.subj_ans = None


if __name__ == '__main__':
    enricy = Enricher()
    while True:
        try:
            Data = enricy.ask_question(domain='Education')
            Data.user_answer = unicode(raw_input(Data.question))
            Data = enricy.consume_answer(enriching_data=Data)
            print u"Found object: ", unicode(Data.validated_object)

        except Exception, e:
            print e
