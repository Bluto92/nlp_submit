import spacy
from nltk.tokenize import WordPunctTokenizer
from utils import ioutils
import os
from tqdm import tqdm
import codecs
import xml.etree.cElementTree as ET
import gzip
import tarfile

GLOVE_DIR = "../glove/"
GLOVE_TRAIN_DIR = GLOVE_DIR + "train_data/"
GLOVE_VOCAB = GLOVE_TRAIN_DIR + "vocab.txt"
GLOVE_TRAIN_FILE = GLOVE_TRAIN_DIR + "train"
GLOVE_MODEL_DIR = GLOVE_DIR + "model/"
GLOVE_MODEL_DATA_DIR = GLOVE_MODEL_DIR + "data/"
GLOVE_MODEL_VOCAB_FILE = GLOVE_MODEL_DATA_DIR + "vocab.txt"
GLOVE_MODEL_VECTORS_FILE = GLOVE_MODEL_DATA_DIR + "vectors.txt"
GLOVE_MODEL_MERGED_FILE = GLOVE_MODEL_DIR + "glove_model.txt"

nlp = spacy.load('en')


def generate_sentence_datafile(filepath):
    datafile = gzip.open(filename=filepath)
    for single_file_path in datafile:

        single_file = gzip.open(filename=single_file_path)
        yield generate_sentences_from_filepath(single_file)


def parse_data_file(filepath):
    """

    :param filepath: the path to the file xml.gz
    :return: the text as a string and the root Element of the annotations' subtree
    :rtype: str, Element
    """
    document = ET.parse(gzip.open(filepath))
    root = document.getroot()  # type: ET.Element
    text_extracted = ""

    for el in root:
        if el.tag == 'text':
            text_extracted = unicode(el.text)

    return text_extracted


def generate_sentences_from_filepath(filepath):
    try:
        document = ET.parse(filepath)

        root = document.getroot()  # type: ET.Element
        if root.tag == 'data':
            for el in root:
                if el.tag == 'block':
                    for el_s in el:
                        if el_s.tag == 'sentence':
                            # clean sentence
                            yield unicode(el_s.text)

    except Exception:
        return


def count_big_data_file(big_data_dir):
    counter = 0
    with tqdm() as td:
        for dir in os.listdir(big_data_dir):
            counter += len(os.listdir(big_data_dir + "/" + dir))
            td.update(1)

    return counter


def extract_from_big_data(big_data_dir):
    vocabulary = set()
    space = u" "
    newline = u'\n'
    train_file = codecs.open(GLOVE_TRAIN_FILE, mode="w+", encoding='utf-8')
    with tqdm(total=count_big_data_file(big_data_dir=big_data_dir)) as td:
        for dir in os.listdir(big_data_dir):

            for datafile in os.listdir(big_data_dir + "/" + dir):
                filepath = big_data_dir + "/" + dir + "/" + datafile
                for sentence in generate_sentences_from_filepath(filepath=filepath):
                    for word in tokenized_query(sentence):
                        vocabulary.add(word)
                        train_file.writelines([sentence, space])
                td.update(1)

    train_file.close()

    print "Writing Vocabulary file: ", GLOVE_VOCAB
    vocab_file = codecs.open(GLOVE_VOCAB, mode='w+', encoding='utf-8')
    for lexeme in vocabulary:
        vocab_file.write(lexeme)
        vocab_file.write(newline)
    vocab_file.close()


def tokenized_query(query_str):
    tokenizer = WordPunctTokenizer

    return tokenizer.tokenize(query_str)

    # tokens = []
    # for tok in nlp(query_str):
    #     tokens.append(tok.lower_)
    # return tokens


def extract_train_data():
    if not os.path.exists(GLOVE_DIR):
        os.mkdir(GLOVE_DIR)
    space = u" "
    newline = u'\n'
    vocab = set()
    train_file = codecs.open(GLOVE_TRAIN_FILE, mode="w+", encoding='utf-8')
    from utils import embedding_utils
    Vocabulary = embedding_utils.get_training_set_vocabulary()

    for word in Vocabulary:
        train_file.write(word + space)

    with tqdm(total=ioutils.get_data_size()) as td:
        for data_entry in ioutils.generator_data_from_chunks():
            question = data_entry['question']
            answer = data_entry['answer']
            context = data_entry['context']

            question_tokend = tokenized_query(question)
            context_tokend = tokenized_query(context)
            answer_tokend = tokenized_query(answer)

            question_str = u""
            for word in question_tokend:
                if '\\' not in word:
                    vocab.add(word.lower())
                    question_str += word.lower() + space

            answer_str = u""
            for word in answer_tokend:
                if '\\' not in word:
                    vocab.add(word.lower())
                    answer_str += word.lower() + space

            context_str = u""
            for word in context_tokend:
                if '\\' not in word:
                    vocab.add(word.lower())
                    context_str += word.lower() + space

            train_file.writelines([question_str, answer_str, context_str])
            td.update(1)

    train_file.close()

    print "Writing Vocabulary file: ", GLOVE_VOCAB
    vocab_file = codecs.open(GLOVE_VOCAB, mode='w+', encoding='utf-8')
    for lexeme in vocab:
        vocab_file.write(lexeme.lower())
        vocab_file.write(newline)
    vocab_file.close()


if __name__ == '__main__':
    # BFW = "/run/media/fra/Dati 2T/Universita/Magistrale/NLP/babelfied-wikipediaXML/"

    # files = count_big_data_file(BFW)
    # files = 1972693
    # with tqdm(total=files) as td:
    #     train_file = codecs.open(GLOVE_TRAIN_FILE, mode="w+", encoding='utf-8')
    #     for dirbfw in os.listdir(BFW):
    #         for fibfw in os.listdir(BFW + dirbfw):
    #             try:
    #                 filepath = BFW + dirbfw + "/" + fibfw
    #                 # print filepath
    #                 text = parse_data_file(filepath=filepath)
    #                 train_file.write(text)
    #                 train_file.write(" ")
    #
    #             except Exception:
    #                 pass
    #             td.update(1)
    #     train_file.close()

    extract_train_data()
