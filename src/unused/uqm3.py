from keras.callbacks import ModelCheckpoint, CSVLogger, ReduceLROnPlateau, EarlyStopping
from keras.layers import LSTM, Embedding, Dense, TimeDistributed, Bidirectional, RepeatVector
from keras.models import Sequential

from nltk import WordPunctTokenizer
from sklearn.model_selection import train_test_split
from utils import features_utils
import pickle
import numpy
import os

BATCH_SIZE = 32
MODEL_TEMP_DIR = "../models/"
LOG_DIR = "../logs/"


def compile_lstm(embeddings, answer_space, max_length_q=features_utils.MAX_QST, max_length_a=features_utils.MAX_RSP,
                 num_layers=1, dropout=0.15):
    model = Sequential()

    if type(embeddings) == tuple:
        model.add(Embedding(input_dim=embeddings[0],
                            output_dim=embeddings[1],
                            input_length=max_length_q,
                            trainable=True,
                            mask_zero=True))
    else:
        model.add(Embedding(input_dim=embeddings.shape[0],
                            output_dim=embeddings.shape[1],
                            input_length=max_length_q,
                            trainable=False,
                            weights=[embeddings],
                            mask_zero=True))

    # encoder
    for _ in xrange(0, num_layers):
        model.add(Bidirectional(
            LSTM(500, return_sequences=True, recurrent_dropout=dropout, dropout=dropout)))

    model.add(LSTM(500, return_sequences=False, recurrent_dropout=dropout, dropout=dropout))

    # model.add(Dense(300))
    # repeater
    model.add(RepeatVector(max_length_a, ))

    # decoder
    for _ in xrange(0, num_layers):
        model.add(Bidirectional(
            LSTM(500, return_sequences=True, recurrent_dropout=dropout, dropout=dropout)))

    model.add(LSTM(500, return_sequences=True, recurrent_dropout=dropout, dropout=dropout))

    model.add(TimeDistributed(Dense(answer_space, dtype=numpy.bool, activation='softmax')))

    model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])

    model.summary()

    return model


def get_train_test_set(data, nlp, test_size=0.25):
    x_train, x_test, y_train, y_test = [], [], [], []

    for datas in data.values():
        questions, answers = [], []
        for data_value in datas:
            questions.append(nlp(data_value['question']))
            answers.append(nlp(data_value['answer']))

        x_train_p, x_test_p, y_train_p, y_test_p = train_test_split(questions, answers,
                                                                    test_size=test_size)

        x_train += x_train_p
        y_train += y_train_p

        x_test += x_test_p
        y_test += y_test_p

    return x_train, x_test, y_train, y_test


def parse_phrase_matrix(matrix, i2w):
    phrases = []
    for sentence in matrix:
        sent = ""
        for word_distribution in sentence:
            word_index = numpy.argmax(word_distribution)
            word = i2w[word_index]
            sent += word + " "
        phrases.append(sent)

    return phrases


def fit_model_relations(embeddings, w2iq, w2ia, relations, model_dir, model_name=u"model", epochs=5, test_size=0.25,
                        question_max_length=features_utils.MAX_QST, answer_max_length=features_utils.MAX_RSP,
                        num_layers=2, answer_min_length=1, question_min_length=1,
                        batch_size=BATCH_SIZE, generator=True, dropout=0.3):
    model_dir = MODEL_TEMP_DIR + model_dir + "/"
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)

    pickle.dump(w2ia, open(model_dir + "w2i.pickle", mode='w+'))
    weights_improvement_file_path = model_dir + "/" + model_name + \
                                    "-{epoch:02d}-a{acc:.4f}-l{loss:.4f}" + \
                                    "-va{val_acc:.4f}-vl{val_loss:.4f}.keras"

    checkpoint = ModelCheckpoint(weights_improvement_file_path,
                                 monitor='acc',
                                 save_best_only=True,
                                 mode='max')

    csv_log_path = LOG_DIR + model_name + ".log"
    csv_logger = CSVLogger(csv_log_path)
    reduce_lr = ReduceLROnPlateau(monitor='acc', patience=1)

    early_stopping_acc = EarlyStopping(monitor='acc', patience=3, mode='max')
    early_stopping_loss = EarlyStopping(monitor='loss', patience=3, mode='min')

    callbacks = [checkpoint,
                 csv_logger,
                 early_stopping_acc,
                 early_stopping_loss,
                 reduce_lr]

    model = compile_lstm(embeddings, answer_space=len(w2ia), max_length_q=question_max_length,
                         max_length_a=answer_max_length, num_layers=num_layers, dropout=dropout)

    tokenizer = WordPunctTokenizer()

    if generator:
        validation_steps = numpy.floor(
            features_utils.get_features_size(train=False, relations=relations, test_size=test_size,
                                             answer_min_length=answer_min_length,
                                             question_min_length=question_min_length) / batch_size)
        if validation_steps <= 0:
            validation_steps = 1

        model = model.fit_generator(
            generator=features_utils.get_features_generator_tokenizer(tokenizer=tokenizer, w2iq=w2iq, w2ia=w2ia,
                                                                      train=True, relations=relations,
                                                                      test_size=test_size,
                                                                      question_min_length=question_min_length,
                                                                      answer_min_length=answer_min_length,
                                                                      max_length_q=question_max_length,
                                                                      max_length_a=answer_max_length, remove_punct=None,
                                                                      eos=True, rotate=False, ssn=True),
            steps_per_epoch=numpy.ceil(
                features_utils.get_features_size(train=True, relations=relations, test_size=test_size,
                                                 answer_min_length=answer_min_length,
                                                 question_min_length=question_min_length) / batch_size),

            validation_data=features_utils.get_features_generator_tokenizer(tokenizer=tokenizer, w2iq=w2iq, w2ia=w2ia,
                                                                            train=True, relations=relations,
                                                                            test_size=test_size, remove_punct=None,
                                                                            question_min_length=question_min_length,
                                                                            answer_min_length=answer_min_length,
                                                                            max_length_q=question_max_length,
                                                                            max_length_a=answer_max_length,
                                                                            eos=True, rotate=False, ssn=True),

            validation_steps=validation_steps, epochs=epochs, callbacks=callbacks)
    else:

        features_x, features_y = features_utils.get_features_list_tokenizer(tokenizer=tokenizer, w2i=w2iq, train=True,
                                                                            relations=relations, test_size=0.,
                                                                            question_min_length=question_min_length,
                                                                            answer_min_length=answer_min_length,
                                                                            max_length_q=question_max_length,
                                                                            max_length_a=answer_max_length,
                                                                            remove_punct=None,
                                                                            eos=True, rotate=True, ssn=True)

        model = model.fit(x=features_x, y=features_y, epochs=epochs, callbacks=callbacks, validation_split=test_size)

    return model


def fit_model_domains(embeddings, w2i, domains, model_prefix="model", epochs=5, num_layers=2,
                      max_length_q=features_utils.MAX_QST, max_length_a=features_utils.MAX_RSP):
    """

    :param max_length_a:
    :param max_length_q:
    :param num_layers:
    :param embeddings:
    :param w2i:
    :param dict[str, str] domains:
    :param model_prefix:
    :param epochs:
    :return:
    """

    model_temp_dir = MODEL_TEMP_DIR
    models = []
    from utils import domains_utils

    for domain, dom_prefix in domains.items():
        relations = domains_utils.get_relations_for_domain_kbs(domain=domain)

        if dom_prefix is None:
            model_prefix += "-" + domain[:5].lower()
            model_temp_dir += domain.strip("\b").lower()[:5] + "/"
        else:
            model_prefix += "-" + dom_prefix.strip("\b").lower()
            model_temp_dir += dom_prefix.strip("\b").lower() + "/"

        if not os.path.exists(model_temp_dir):
            os.mkdir(model_temp_dir)

        model = fit_model_relations(embeddings=embeddings, w2iq=w2i, relations=relations,
                                    model_dir=model_temp_dir, model_name=model_prefix,
                                    question_max_length=max_length_q, answer_max_length=max_length_a,
                                    epochs=epochs, num_layers=num_layers)

        models.append(model)

    return models


def fit_model_marco():
    from utils import marcods
    VocabularyQuestion, VocabularyAnswer = marcods.get_marco_vocabularies()
    Additional_words = [u'$PAD$'.lower(), u'$UNK$'.lower(), u'$SSN$'.lower(), u'$EOS$'.lower()]

    from utils import embedding_utils
    Matrix, word2indexQuestion = embedding_utils.get_glove_matrix_w2i(embedding_utils.GLOVE_FILE_PATH,
                                                                      additional_words=Additional_words,
                                                                      restricted=set(VocabularyQuestion))

    word2indexAnswer = embedding_utils.get_glove_word2index(embedding_utils.GLOVE_FILE_PATH,
                                                            additional_words=Additional_words,
                                                            restricted=set(VocabularyAnswer))

    Question_min_length, Question_max_length, Answer_min_length, Answer_max_length = 4, 8, 7, 10
    model = compile_lstm(Matrix, answer_space=len(word2indexAnswer), max_length_q=Question_max_length,
                         max_length_a=Answer_max_length, num_layers=2, dropout=0.15)

    model.fit_generator(generator=marcods.get_features_generator(w2iq=word2indexQuestion, w2ia=word2indexAnswer,
                                                                 max_length_a=Answer_max_length,
                                                                 max_length_q=Question_max_length),
                        steps_per_epoch=marcods.get_marco_size() / 32)

    return model


if __name__ == "__main__":
    # from utils import embedding_utils
    from utils import features_utils
    from utils import domains_utils
    # from utils import embedding_utils
    from utils import data_utils

    # mdoel = fit_model_marco()

    Additional_words = [u'$PAD$'.lower(), u'$UNK$'.lower(), u'$SSN$'.lower(), u'$EOS$'.lower()]
    Test_size = 0.25

    Relations = [u'GENERALIZATION']
    WPtokenizer = WordPunctTokenizer()

    for relation in Relations:
        # import stats
        # question_min_length, question_max_length, answer_min_length, answer_max_length = stats.best_interval(
        #     relation=relation)

        per_rel_dir = "/home/fra/Projects/Universita/NLP/FinalProject/data/per_relation_refactored/"

        Question_min_length, Question_max_length, Answer_min_length, Answer_max_length = 0, 10, 0, 10
        rel_dir_name = relation.upper() + '_GBIG_' + str(Question_min_length) + '_' + str(Question_max_length) + '_' \
                       + str(Answer_min_length) + '_' + str(Answer_max_length) + '/'

        if os.path.exists("../models/" + rel_dir_name):
            continue

        VocabularyQuestion = data_utils.extract_vocabulary_from_data(fields=['question'], relations=[relation],
                                                                     per_rel_dir=per_rel_dir)
        VocabularyAnswer = data_utils.extract_vocabulary_from_data(fields=['answer'], relations=[relation],
                                                                   per_rel_dir=per_rel_dir)

        from utils import embedding_utils

        Matrix, word2indexQuestion = embedding_utils.get_glove_matrix_w2i(embedding_utils.GLOVE_FILE_PATH,
                                                                          additional_words=Additional_words,
                                                                          restricted=set(VocabularyQuestion))

        word2indexAnswer = embedding_utils.get_glove_word2index(embedding_utils.GLOVE_FILE_PATH,
                                                                additional_words=Additional_words,
                                                                restricted=set(VocabularyAnswer))

        # print "Vocabulary Coverage: ", str(float(Matrix.shape[0]) / len(Vocabulary))
        # word2indexQuestion = features_utils.get_word2index_unicode(vocab=VocabularyQuestion)
        # word2indexAnswer = features_utils.get_word2index_unicode(vocab=VocabularyAnswer)
        # Matrix = (len(word2indexQuestion.keys()), 200)

        print "Vocabulary Question Length: ", str(len(VocabularyQuestion))
        print "Vocabulary Answer Length: ", str(len(VocabularyAnswer))
        print "Vocabulary Question Coverage: ", str(float(Matrix.shape[0]) / len(VocabularyQuestion))
        print "Vocabulary Answer Coverage: ", str(float(Matrix.shape[0]) / len(VocabularyAnswer))

        print "Fitting Model for Relation: ", relation
        fit_model_relations(embeddings=Matrix, w2iq=word2indexQuestion, w2ia=word2indexAnswer, relations=[relation],
                            model_dir=rel_dir_name, model_name=relation.lower(), epochs=20, num_layers=2,
                            test_size=0.10, batch_size=64, answer_min_length=Answer_min_length,
                            question_min_length=Question_min_length, question_max_length=Question_max_length,
                            answer_max_length=Answer_max_length, generator=True, dropout=0.4)

    print "Bye"
