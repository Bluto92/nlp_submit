import keras
import numpy
from utils import data_utils
from nltk.tokenize import WordPunctTokenizer


class Answerer:
    def __init__(self, model_path, w2i, i2w, question_max_length):
        self.model_path = model_path
        self.model = keras.models.load_model(model_path, compile=False)
        self.w2i = w2i
        self.i2w = i2w
        self.question_max_length = question_max_length

    def answer(self, question, decoded=True, allow_unknown=False, human=False):
        from utils import features_utils
        tokenizer = WordPunctTokenizer()
        x_tokens = tokenizer.tokenize(question)
        x = features_utils.get_features_x(x_tokens, self.w2i, max_length_q=self.question_max_length)
        y_prediction = self.model.predict(x)

        if human:
            decoded_predictions = self.decode_prediction(y_prediction=y_prediction,
                                                         allow_unknown=allow_unknown)
            for decoded_prediction in decoded_predictions:
                phrase = ""
                for word in decoded_prediction:
                    if word not in [features_utils.WORD_UNK.lower_, features_utils.WORD_EOS.lower_,
                                    features_utils.WORD_SSN.lower_, features_utils.WORD_PAD.lower_]:
                        phrase += word + " "
                    if word == features_utils.WORD_EOS.lower_:
                        break
                return phrase

        else:
            if decoded:
                return self.decode_prediction(y_prediction=y_prediction,
                                              allow_unknown=allow_unknown)
            else:
                return [numpy.argmax(y) for y in y_prediction]

    def decode_prediction(self, y_prediction, allow_unknown=False):
        from utils import features_utils
        unknown_index = self.w2i[features_utils.WORD_UNK.lower_]
        predicted_sentences = []

        for sentence_distribution in y_prediction:
            predicted_sentence = []
            for word_index, word_distribution in enumerate(sentence_distribution):
                if word_index <= 1:
                    word_distribution[self.w2i[features_utils.WORD_EOS.lower_]] = .0

                if not allow_unknown:
                    word_distribution[unknown_index] = .0

                index = numpy.argmax(word_distribution)
                predicted_sentence.append(self.i2w[index])

            predicted_sentences.append(predicted_sentence)
        return predicted_sentences


if __name__ == "__main__":
    from utils import features_utils, data_utils, embedding_utils
    from copy import deepcopy

    model_path = "/home/fra/Projects/Universita/NLP/FinalProject/models/GENERALIZATION_GBIG_4_8_7_10/" \
                 "generalization-05-a0.6815-l2.9458-va0.6911-vl2.9537.keras"
    # model_path = "../models/GENERALIZATION_GMINE/emb-generalization-02-a0.2948-l0.1522-va0.3083-vl0.1431.keras"

    additional_words = [u'$PAD$'.lower(), u'$UNK$'.lower(), u'$SSN$'.lower(), u'$EOS$'.lower()]

    train_vocabulary = data_utils.extract_vocabulary_from_data(relations=[u'GENERALIZATION'])

    # w2i = embedding_utils.get_glove_word2index(glove_file_path=embedding_utils.GLOVE_FILE_PATH_MINE,
    #                                            additional_words=additional_words,
    #                                            restricted=deepcopy(train_vocabulary))

    w2i_path = "/home/fra/Projects/Universita/NLP/FinalProject/models/GENERALIZATION_GBIG_4_8_7_10/w2i.pickle"
    question_min_length = 4
    question_max_length = 8
    answer_min_length = 7

    import pickle

    # w2i = features_utils.get_word2index_unicode(vocab=train_vocabulary)
    w2i = pickle.load(open(w2i_path, mode='r'))
    i2w = features_utils.get_index2word_from_w2i(w2i=w2i)

    answerer = Answerer(model_path=model_path, w2i=w2i, i2w=i2w, question_max_length=8)
    per_rel_dir = "/home/fra/Projects/Universita/NLP/FinalProject/data/per_relation_refactored/"
    for counter, data_entry in enumerate(data_utils.parse_data_per_relation(relation=u'GENERALIZATION',
                                                                            question_min_length=4,
                                                                            answer_min_length=7,
                                                                            per_rel_dir=per_rel_dir)):
        # relations=domains_utils.get_relations_for_domain_kbs('Education'))):

        if counter >= 10:
            break

        question = data_entry['question']
        answer = data_entry['answer']

        print "Question: ", question
        print "Gold Answer: ", answer
        print "Predicted Answer: ", answerer.answer(question=question, allow_unknown=False, decoded=True, human=True)
        print '*' * 50
        print
