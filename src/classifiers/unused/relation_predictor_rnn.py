from ..utils import domains_utils
from ..utils import database
from nltk.tokenize import WordPunctTokenizer
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import InputLayer, Embedding, LSTM, Bidirectional, TimeDistributed, Dense
from keras.callbacks import EarlyStopping, ReduceLROnPlateau, CSVLogger, ModelCheckpoint
from keras.metrics import categorical_accuracy
import pickle
import numpy
import os
from ..utils import embedding_utils

# Relations = domains_utils.get_all_relations_kbs()
test_size = .0
LOG_DIR = '../logs/'

UNK = '$UNK$'
PAD = '$PAD$'
MAX_QST = 8
MAX_RSP = 3


def create_model(relations, name=""):
    tokenizer = WordPunctTokenizer()
    relation_questions = {}
    max_size = 0
    sizes = []
    relation2index = {relation: index for index, relation in enumerate(relations)}

    for relation in relations:
        entries = database.get_all_qas(relation)
        questions = [entry[0] for entry in entries if
                     len(tokenizer.tokenize(entry[0])) < MAX_QST and len(tokenizer.tokenize(entry[1])) < MAX_RSP]
        if len(questions) > max_size:
            max_size = len(questions)
        sizes.append(len(questions))

    # mean = int(abs(numpy.asarray(sizes, dtype=numpy.int).mean(axis=0)))
    for relation in relations:
        entries = database.get_all_qas(relation)
        questions = [entry[0].lower() for entry in entries if
                     len(tokenizer.tokenize(entry[0])) < MAX_QST and len(tokenizer.tokenize(entry[1])) < MAX_RSP]
        norm_questions = questions

        # if len(questions) <= mean:
        #     # norm_factor = float(mean) / len(questions)
        #     # norm_questions *= int(abs(norm_factor) + 1)
        #     pass
        # else:
        #     norm_questions, _ = train_test_split(norm_questions, train_size=mean)

        relation_questions[relation] = norm_questions

    tot_qst = sum(len(qs) for qs in relation_questions.values())
    print "Total Number of Questions: ", tot_qst

    # build vocabulary
    vocabulary = set()
    for question_list in relation_questions.values():
        for question in question_list:
            for word in tokenizer.tokenize(question):
                vocabulary.add(word)

    # build w2i/i2w
    # word2index = {word: index for index, word in enumerate(vocabulary, start=2)}
    # word2index[PAD] = 0
    # word2index[UNK] = 1
    # index2word = {index: word for index, word in word2index.items()}

    # glove
    matrix, word2index = embedding_utils.get_glove_matrix_w2i(glove_file_path=embedding_utils.GLOVE_FILE_PATH,
                                                              restricted=vocabulary, pad=PAD, unk=UNK)

    # build X, Y
    X_train, Y_train = [], []
    for relation, question_list in relation_questions.items():
        for question in question_list:
            qTokens = tokenizer.tokenize(question)
            x = numpy.zeros(MAX_QST, dtype=numpy.uint32)
            for index, token in enumerate(qTokens):
                if index >= MAX_QST:
                    break
                x[index] = word2index.get(token, word2index[UNK])

            y = numpy.full((len(relations)), False, dtype=bool)
            y[relation2index[relation]] = True

            X_train.append(x)
            Y_train.append(y)

    X_end = numpy.asarray(X_train)
    Y_end = numpy.asarray(Y_train)

    print "X shape", X_end.shape
    print "Y shape", Y_end.shape
    shape = matrix.shape

    # create model
    model = Sequential()
    model.add(InputLayer((MAX_QST,)))
    # shape = (len(word2index.keys()), 64)
    model.add(Embedding(input_dim=shape[0], output_dim=shape[1], mask_zero=True, trainable=False, weights=[matrix]))
    # model.add(Bidirectional(LSTM(512, return_sequences=True, dropout=0.5, recurrent_dropout=0.5)))
    model.add(Bidirectional(LSTM(512, return_sequences=False, dropout=0.5, recurrent_dropout=0.5)))
    model.add(Dense(len(relation2index.keys()), activation='softmax'))
    model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=[categorical_accuracy])
    model.summary()

    # callbacks
    model_dir = '../models/relation_predictor/' + name
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)

    pickle.dump(file=open(model_dir + "/w2i.pickle", mode='w+'), obj=word2index)
    pickle.dump(file=open(model_dir + "/r2i.pickle", mode='w+'), obj=relation2index)

    model_name = 'relation_predictor_' + name
    weights_improvement_file_path = model_dir + "/" + model_name + \
                                    "-{epoch:02d}-a{categorical_accuracy:.4f}-l{loss:.4f}" \
                                    "-va{val_categorical_accuracy:.4f}-vl{val_loss:.4f}.keras"

    checkpoint = ModelCheckpoint(weights_improvement_file_path, monitor='categorical_accuracy', mode='max')
    # save_best_only=True,
    csv_log_path = LOG_DIR + model_name + ".log"
    csv_logger = CSVLogger(csv_log_path)
    reduce_lr = ReduceLROnPlateau(monitor='categorical_accuracy', patience=1)

    early_stopping_acc = EarlyStopping(monitor='val_categorical_accuracy', patience=3, mode='max')
    # early_stopping_loss = EarlyStopping(monitor='val_loss', patience=3, mode='min')

    callbacks = [checkpoint, csv_logger, early_stopping_acc,
                 # early_stopping_loss,
                 reduce_lr]

    # fit model
    batch_size = 128
    model.fit(x=X_end, y=Y_end, batch_size=batch_size, validation_split=0.3, epochs=1, callbacks=callbacks)


class AnswererClassifier(object):
    def __init__(self, data_dir, model_name, input_w2i_filename, output_c2i_filename):
        """

        :param str data_dir:
        :param str model_name:
        :param str input_w2i_filename:
        :param str output_c2i_filename:
        """

        import keras
        import pickle
        self.data_dir = data_dir
        if not self.data_dir.endswith('/'):
            self.data_dir += '/'

        self.model_name = model_name
        self.w2i_filename = input_w2i_filename
        self.c2i_filename = output_c2i_filename

        self.model = keras.models.load_model(self.data_dir + self.model_name)
        self.w2i = pickle.load(open(self.data_dir + self.w2i_filename, mode='r'))
        self.c2i = pickle.load(open(self.data_dir + self.c2i_filename, mode='r'))
        self.i2c = {index: word for word, index in self.c2i.items()}

        print self.c2i.keys()

    def predict(self, sentence):
        """

        :param unicode | str sentence:
        :return:
        """
        from nltk.tokenize import WordPunctTokenizer
        tokenizer = WordPunctTokenizer()
        qTokens = tokenizer.tokenize(sentence.lower())
        x = numpy.zeros((1, MAX_QST), dtype=numpy.uint32)
        # x = numpy.zeros((1, len(qTokens)), dtype=numpy.uint32)
        for index, token in enumerate(qTokens):
            if index >= MAX_QST:
                break
            x[0, index] = self.w2i.get(token, self.w2i[UNK])

        prediction = self.model.predict(x)
        predicted_class = numpy.argmax(prediction[0])
        return self.i2c[predicted_class]


if __name__ == '__main__':
    domain = u'Education'
    domains = domains_utils.get_all_domains()
    for domain in domains:
        if os.path.exists('../models/relation_predictor/' + domain):
            continue

        create_model(name=domain, relations=domains_utils.get_relations_for_domain_kbs(domain=domain))

    # create_model(relations=domains_utils.get_all_relations_kbs())
    pass
