from ..utils import database, domains_utils
import numpy
import os
import pickle
from tqdm import tqdm
from nltk.stem.snowball import EnglishStemmer
from nltk import word_tokenize
from keras.models import Sequential
from keras.layers import InputLayer, Embedding, LSTM, Dense, TimeDistributed, Bidirectional
from keras.metrics import categorical_crossentropy
from keras.callbacks import EarlyStopping, ModelCheckpoint, CSVLogger, ReduceLROnPlateau

stemmer = EnglishStemmer()
SUBJ = 1
OBJ = 2
LOG_DIR = '../logs/'


def get_xy_qso(relations, max_seq=10, limit=None):
    X_temp, Y_temp = [], []
    with tqdm(total=len(relations)) as td:
        for relation in relations:
            td.desc = relation + " "
            td.refresh()

            entries = database.get_all_qas(relation, limit=limit)
            questions_data = []
            for entry in entries:
                if len(word_tokenize(entry[0])) > max_seq:
                    continue
                question_data = {'question': entry[0].lower()}
                if entry[2] and entry[6]:
                    question_data['subject'] = entry[2].lower()
                if entry[3] and entry[7]:
                    question_data['object'] = entry[3].lower()
                questions_data.append(question_data)

            soq, sq, oq = [], [], []
            for question_data in questions_data:
                question = question_data['question']
                subj = question_data.get('subject', None)
                obj = question_data.get('object', None)

                if subj and obj and subj in question and obj in question:
                    soq.append((question, subj, obj))
                elif subj and subj in question:
                    sq.append((question, subj))
                elif obj and obj in question:
                    oq.append((question, obj))

            for question, subj in sq:
                x = word_tokenize(question)
                y = numpy.zeros((max_seq,), dtype=numpy.short)
                qtokens = word_tokenize(question)
                for qpos, qword in enumerate(qtokens):
                    if qpos < max_seq and y[qpos] != 0:
                        continue

                    found = True
                    for sspos, qsubj in enumerate(word_tokenize(subj)):
                        if qtokens[qpos + sspos] != qsubj:
                            found = False
                            break

                    if found:
                        for subj_poss in numpy.arange(qpos, qpos + len(word_tokenize(subj)), 1):
                            if subj_poss < max_seq:
                                y[subj_poss] = SUBJ

                X_temp.append(x)
                Y_temp.append(y)

            for question, obj in oq:
                x = word_tokenize(question)
                y = numpy.zeros((max_seq,), dtype=numpy.short)
                for qpos, qword in enumerate(qtokens):
                    if qpos < max_seq and y[qpos] != 0:
                        continue

                    found = True
                    for sspos, qobj in enumerate(word_tokenize(obj)):
                        if qtokens[qpos + sspos] != qobj:
                            found = False
                            break

                    if found:
                        for subj_poss in numpy.arange(qpos, qpos + len(word_tokenize(obj)), 1):
                            if subj_poss < max_seq:
                                y[subj_poss] = OBJ

                X_temp.append(question)
                Y_temp.append(y)

            for question, subj, obj in soq:
                x = word_tokenize(question)
                y = numpy.zeros((max_seq,), dtype=numpy.short)
                qtokens = word_tokenize(question)
                for qpos, qword in enumerate(qtokens):
                    if qpos < max_seq and y[qpos] != 0:
                        continue

                    found = True
                    for sspos, qsubj in enumerate(word_tokenize(subj)):
                        if qtokens[qpos + sspos] != qsubj:
                            found = False
                            break

                    if found:
                        for subj_poss in numpy.arange(qpos, qpos + len(word_tokenize(subj)), 1):
                            if subj_poss < max_seq:
                                y[subj_poss] = SUBJ
                    else:
                        found = True
                        for sspos, qobj in enumerate(word_tokenize(obj)):
                            if qtokens[qpos + sspos] != qobj:
                                found = False
                                break

                        if found:
                            for subj_poss in numpy.arange(qpos, qpos + len(word_tokenize(obj)), 1):
                                if subj_poss < max_seq:
                                    y[subj_poss] = OBJ

                X_temp.append(x)
                Y_temp.append(y)

            td.update(1)

    # build vocabulary
    vocabulary_set = set()
    for question in X_temp:
        for qtoken in question:
            vocabulary_set.add(qtoken)

    vocabulary = sorted(list(vocabulary_set))

    # word2index
    word2index = {word: index for index, word in enumerate(vocabulary, start=2)}
    word2index[0] = '$PAD$'
    word2index[1] = 'UNK$'

    # encode X
    X = []
    for question in X_temp:
        x = numpy.zeros((max_seq, ), dtype=numpy.uint32)
        for index, word in enumerate(question):
            if index < max_seq:
                x[index] = word2index[word]
        X.append(x)

    # encode Y
    Y = []
    for y_temp in Y_temp:
        y = numpy.full((y_temp.shape[0], 3), False, dtype=numpy.bool)
        for index, y_v in enumerate(y_temp):
            y[index, y_v] = True
        Y.append(y)

    return numpy.asarray(X, dtype=numpy.uint32), numpy.asarray(Y, dtype=numpy.bool), word2index


def generator(X, Y):
    for x, y in zip(X, Y):
        yield x, y


def mlpc_estimate(relations, test_size=0.3):
    X, Y, word2index = get_xy_qso(relations=relations, max_seq=10)

    model_dir = '../models/so/'
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)

    pickle.dump(obj=word2index, file=open(model_dir + "w2i.pickle", mode='w'))

    model = Sequential()
    model.add(InputLayer((10, )))
    model.add(Embedding(input_dim=len(word2index.keys()), output_dim=64, mask_zero=True))
    model.add(Bidirectional(LSTM(512, return_sequences=True, dropout=0.5)))
    model.add(TimeDistributed(Dense(3, activation='softmax')))
    model.summary(line_length=200)
    model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=[categorical_crossentropy])

    model_name = "so_classifier_"
    weights_improvement_file_path = model_dir + "/" + model_name + \
                                    "-{epoch:02d}-a{categorical_crossentropy:.4f}-l{loss:.4f}-" \
                                    "va{val_categorical_crossentropy:.4f}-vl{val_loss:.4f}.keras"

    checkpoint = ModelCheckpoint(weights_improvement_file_path, monitor='acc', mode='max')
    # save_best_only=True,

    csv_log_path = LOG_DIR + model_name + ".log"
    csv_logger = CSVLogger(csv_log_path)
    reduce_lr = ReduceLROnPlateau(monitor='val_categorical_crossentropy', patience=1)

    early_stopping_acc = EarlyStopping(monitor='categorical_crossentropy', patience=3, mode='max')
    early_stopping_loss = EarlyStopping(monitor='loss', patience=3, mode='min')

    callbacks = [checkpoint,
                 csv_logger,
                 early_stopping_acc,
                 early_stopping_loss,
                 reduce_lr]

    model.fit(X, Y, validation_split=0.3, batch_size=32, epochs=5, callbacks=callbacks)
if __name__ == '__main__':
    # for domain in domains_utils.get_all_domains():
    #     print domain
    #     relations = domains_utils.get_relations_for_domain_kbs(domain=domain)
    #     classifier, best_score = get_best_estimator(relations=relations, classifier=SGDClassifier(), verbose=1)
    #
    #     classifiers_dir = '../classifiers/'
    #     score = "%.2f" % best_score
    #     classifier_path = classifiers_dir + 'relation_classifier_' + \
    #                       domain.replace(' ', '_').lower() + '_' + score + '.pickle'
    #     pickle.dump(classifier, open(classifier_path, mode='w+'), protocol=pickle.HIGHEST_PROTOCOL)
    #
    #     # clf = fit_classifier(relations=domains_utils.get_all_relations_kbs(), classifier=classifier,
    #     #                      verbose=1, test_size=.3)
    #     print

    # relations = domains_utils.get_all_relations_kbs()
    # mlpc_estimate(relations)

    import keras
    keras.models.load_model("../models/so/")
