import os
import pickle

import numpy
from keras.callbacks import ModelCheckpoint, CSVLogger, ReduceLROnPlateau, EarlyStopping
from keras.layers import LSTM, Embedding, Dense, InputLayer, Bidirectional
from keras.metrics import categorical_accuracy
from keras.models import Sequential
from nltk.tokenize import WordPunctTokenizer
from sklearn.model_selection import train_test_split
from tqdm import tqdm

from src.utils import data_refactor
from src.utils import database
from src.utils import embedding_utils

MODEL_TEMP_DIR = "../models/"
LOG_DIR = "../logs/"

UNK = '$unk$'
# UNK = '<UNK>'
PAD = '$pad$'


# PAD = '<PAD>'


def create_yes_no_model(relations, name, matrix=None, question_w2i=None, glove=True, overwrite=False,
                        glove_filepath=embedding_utils.GLOVE_FILE_PATH_LITTLE):
    model_dir = MODEL_TEMP_DIR + "yesno/glove_direct/" + name + "/"
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)
    elif not overwrite:
        print "Model yet calculated for ", relations
        return None

    model_name = "yes_no_" + name
    yes_qas, no_qas = [], []
    for relation in relations:
        yes_qas += database.get_by_answer(relation, 'yes', lower=True)
        no_qas += database.get_by_answer(relation, 'no', lower=True)

    if len(no_qas) == 0:
        print "Skipped: ", relations
        return None
    size_ratio = float(len(yes_qas)) / len(no_qas)

    ratio_unit = "y/n"
    if size_ratio < .65:
        size_ratio = 1 / size_ratio
        yes_qas *= int(size_ratio)
        ratio_unit = "n/y"
    elif size_ratio > 1.3:
        no_qas *= int(size_ratio)

    size_ratio = float(len(yes_qas)) / len(no_qas)
    print "Yesses: ", str(len(yes_qas))
    print "Nos: ", str(len(no_qas))
    print "Size ratio: ", str(size_ratio), " ", ratio_unit

    # build Vocabulary / w2i / i2w
    tokenizer = WordPunctTokenizer()
    if not glove:
        VocabularySet = set()
        with tqdm(total=len(yes_qas) + len(no_qas), desc="Extracting Vocabulary ") as td:
            for entry in yes_qas + no_qas:
                td.update(1)
                question = entry[0]
                for word in tokenizer.tokenize(question):
                    VocabularySet.add(word)
                    VocabularySet.add(word.lower())

        Vocabulary = sorted(list(VocabularySet))  # type: list
        Vocabulary.insert(0, PAD)
        Vocabulary.insert(1, UNK)

        question_w2i = {word: index for index, word in enumerate(Vocabulary)}
        # QuestionI2W = {index: word for word, index in QuestionW2I.items()}
    elif matrix is None or question_w2i is None:
        matrix, question_w2i = embedding_utils.get_full_glove_matrix_w2i(glove_file_path=glove_filepath)

    pickle.dump(question_w2i, open(model_dir + model_name + "_yn_question_w2i.pickle", mode='w+'))

    answer_w2i = {'yes': 0, 'no': 1}
    pickle.dump(answer_w2i, open(model_dir + model_name + "_yn_answer_w2i.pickle", mode='w+'))

    # create X, y
    X_list, Y_list = [], []
    max_x = 0

    print "Vocabulary Size: ", len(question_w2i)

    with tqdm(total=len(yes_qas) + len(no_qas), desc="Looking for max length of questions") as td:
        for entry in yes_qas + no_qas:
            tokens = tokenizer.tokenize(entry[0])
            if len(tokens) > max_x:
                max_x = len(tokens)
            td.update(1)
    print "Question Max length: ", str(max_x)

    with tqdm(total=len(yes_qas) + len(no_qas), desc="Creating X, Y") as td:
        for entry in yes_qas:
            td.update(1)
            question = entry[0]
            tokens = tokenizer.tokenize(question)
            x = numpy.zeros((max_x,), dtype=numpy.uint32)
            if len(tokens) > max_x:
                max_x = len(tokens)

            for index, word in enumerate(tokens):
                x[index] = question_w2i.get(word, question_w2i[UNK])
            X_list.append(x)

            # y = numpy.full((1, len(answer_w2i.keys()),), False, dtype=bool)
            # y[0, answer_w2i['yes']] = True
            y = numpy.full((len(answer_w2i.keys()),), False, dtype=bool)
            y[answer_w2i['yes']] = True
            Y_list.append(y)

        for entry in no_qas:
            td.update(1)
            question = entry[0]
            tokens = tokenizer.tokenize(question)
            x = numpy.zeros((max_x,), dtype=numpy.int32)
            if len(tokens) > max_x:
                max_x = len(tokens)

            for index, word in enumerate(tokens):
                x[index] = question_w2i.get(word, question_w2i[UNK])

            X_list.append(x)

            y = numpy.full((len(answer_w2i.keys()),), False, dtype=bool)
            y[answer_w2i['no']] = True
            # y = numpy.full((1, len(answer_w2i.keys()),), False, dtype=bool)
            # y[0, answer_w2i['no']] = True

            Y_list.append(y)

    X = numpy.asarray(X_list, dtype=numpy.int)
    print "X shape: ", X.shape
    Y = numpy.asarray(Y_list, dtype=numpy.bool)
    print "Y shape: ", Y.shape

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.15)

    num_layers = 0
    dropout = 0.4

    weights_improvement_file_path = model_dir + "/" + model_name + \
                                    "-{epoch:02d}-a{categorical_accuracy:.4f}-l{loss:.4f}-" \
                                    "va{val_categorical_accuracy:.4f}-vl{val_loss:.4f}.keras"

    checkpoint = ModelCheckpoint(weights_improvement_file_path, monitor='categorical_accuracy', mode='max',
                                 save_best_only=True, )

    csv_log_path = LOG_DIR + model_name + ".log"
    csv_logger = CSVLogger(csv_log_path)
    reduce_lr = ReduceLROnPlateau(monitor='categorical_accuracy', patience=1)

    early_stopping_acc = EarlyStopping(monitor='val_categorical_accuracy', patience=3, mode='max')
    early_stopping_loss = EarlyStopping(monitor='val_loss', patience=3, mode='min')

    callbacks = [checkpoint, csv_logger, early_stopping_acc, early_stopping_loss, reduce_lr]

    # RNN model
    model = Sequential()
    model.add(InputLayer(input_shape=(None,)))
    if glove:
        model.add(Embedding(input_dim=matrix.shape[0], output_dim=matrix.shape[1], mask_zero=True,
                            trainable=False, weights=[matrix]))
    else:
        model.add(Embedding(input_dim=len(question_w2i.keys()), output_dim=100, mask_zero=True, ))

    # encoder
    for _ in xrange(0, num_layers):
        model.add(Bidirectional(
            LSTM(200, return_sequences=True, recurrent_dropout=dropout, dropout=dropout)))

    model.add(LSTM(300, return_sequences=False, recurrent_dropout=dropout, dropout=dropout))

    # repeater
    # model.add(RepeatVector(1, ))

    # decoder
    # for _ in xrange(0, num_layers):
    #     model.add(Bidirectional(
    #         LSTM(200, return_sequences=True, recurrent_dropout=dropout, dropout=dropout)))
    #
    # model.add(LSTM(300, return_sequences=True, recurrent_dropout=dropout, dropout=dropout))

    # model.add(TimeDistributed(Dense(len(answer_w2i.keys()), dtype=numpy.bool, activation='softmax')))
    model.add(Dense(len(answer_w2i.keys()), dtype=numpy.bool, activation='softmax'))

    model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=[categorical_accuracy])

    model.summary()

    model.fit(X_train, Y_train, batch_size=32, epochs=5, validation_split=0.3, callbacks=callbacks)

    return True


def answer_full_query_recognition(relation, question):
    results = database.get_by_question(relation=relation, question=question)

    if len(results) > 0:
        return results[0][1]

    return None


def answer_through_patterns_recognition(relation, question):
    subj, obj = data_refactor.get_question_subj_obj(relation=relation, question=question)
    print "Subj: ", subj, "\tObj: ", obj
    if subj is not None and obj is not None:
        results = database.get_obj_subj(relation=relation, subj=subj, obj=obj)
        if len(results) > 0:
            return results[0][1]

    return None


def encode_question(question, w2i):
    tokenizer = WordPunctTokenizer()
    tokens = tokenizer.tokenize(question)
    encoded_question = numpy.full((1, len(tokens) + 2), 0, dtype=numpy.uint32)
    for index, token in enumerate(tokens):
        encoded_question[0, index] = w2i.get(token, w2i[UNK])
    return encoded_question


if __name__ == '__main__':
    from src.utils import domains_utils

    matrix, question_w2i = embedding_utils.get_full_glove_matrix_w2i(
        glove_file_path=embedding_utils.GLOVE_FILE_PATH_LITTLE)
    relations = domains_utils.get_all_relations_kbs()

    for relation in relations:
        print "*" * 15, relation, "*" * 15
        if create_yes_no_model([relation], relation.lower(), glove=True, matrix=matrix, question_w2i=question_w2i):
            break

