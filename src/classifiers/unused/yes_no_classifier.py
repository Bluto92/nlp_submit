from src.utils import database
import numpy

from sklearn import metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.svm import LinearSVC, NuSVC
from sklearn.linear_model import SGDClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.neural_network import MLPClassifier
import sklearn.metrics as skmetric

from nltk.corpus import stopwords
from nltk.stem.snowball import EnglishStemmer
from nltk import word_tokenize

import pprint as pp

RELATION = u'GENERALIZATION'
stemmer = EnglishStemmer()


def stemming_tokenizer(text):
    stemmed_text = [stemmer.stem(word) for word in word_tokenize(text, language='english')]
    return stemmed_text


if __name__ == '__main__':
    yes_qas, no_qas = database.get_by_answer(RELATION, 'yes'), database.get_by_answer(RELATION, 'no')

    size_ratio = float(len(yes_qas)) / len(no_qas)

    if size_ratio < .65:
        size_ratio = 1 / size_ratio
        yes_qas *= int(size_ratio)
    elif size_ratio > 1.3:
        no_qas *= int(size_ratio)

    size_ratio = float(len(yes_qas)) / len(no_qas)
    print "Yesses: ", str(len(yes_qas))
    print "Nos: ", str(len(no_qas))
    print "Size ratio: ", str(size_ratio)

    yes_questions = [entry[0] for entry in yes_qas]
    yes_answers = [True for entry in yes_questions]
    no_questions = [entry[0] for entry in no_qas]
    no_answers = [False for entry in no_questions]

    X_yes_train, X_yes_test, Y_yes_train, Y_yes_test = train_test_split(yes_questions, yes_answers, test_size=1000,
                                                                        train_size=1000)
    X_no_train, X_no_test, Y_no_train, Y_no_test = train_test_split(no_questions, no_answers, test_size=1000,
                                                                    train_size=1000)

    X_train = X_yes_train + X_no_train
    Y_train = Y_yes_train + Y_no_train
    X_test = X_yes_test + X_no_test
    Y_test = Y_yes_test + Y_no_test

    vectorizer = TfidfVectorizer(strip_accents=None, preprocessor=None)
    # svm = SGDClassifier()

    n_grams = [(x, y) for x in xrange(1, 3, 1) for y in xrange(1, 6, 1) if x < y]

    pipeline = Pipeline([
        ('vect', vectorizer),
        ('svm', SGDClassifier()),
        # ('knn', KNeighborsClassifier())
        # ('mnb', MultinomialNB())
        # ('mlp', MLPClassifier())
    ])

    parameters = {
        'vect__tokenizer': [None, stemming_tokenizer, word_tokenize],
        'vect__ngram_range': n_grams,
        'vect__analyzer': ['word', 'char'],
        # 'vect__max_df': numpy.arange(.8, 1., .1),
        # 'vect__min_df': numpy.arange(0., .2, .1),
        # 'vect__binary': [True, False],
        'vect__lowercase': [True, False],
        # 'vect__sublinear_tf': [True, False],
        'vect__stop_words': [None, stopwords.words("english")],

        # 'svm__C': [1, 5, 10, 25, 50]
        # 'knn__n_neighbors': numpy.arange(1, 12, 1),
    }

    grid_search = GridSearchCV(pipeline, parameters, pre_dispatch=10, refit=True,
                               scoring=metrics.make_scorer(skmetric.matthews_corrcoef),
                               error_score=0, n_jobs=8, verbose=1, )

    grid_search.fit(X_train, Y_train)
    # Print results for each combination of parameters.print
    number_of_candidates = len(grid_search.cv_results_['params'])
    print "Number of candidates: ", number_of_candidates

    print
    print("Best Estimator:")
    pp.pprint(grid_search.best_estimator_)
    print
    print("Best Parameters:")
    pp.pprint(grid_search.best_params_)
    print
    print("Used Scorer Function:")
    pp.pprint(grid_search.scorer_)
    print("Number of Folds:")
    pp.pprint(grid_search.n_splits_)
    print
    print ("Best Score")
    pp.pprint(grid_search.best_score_)
    print
    y_predicted = grid_search.predict(X_test)

    # Evaluate the performance of the classifier on the original Test-Set
    output_classification_report = metrics.classification_report(Y_test, y_predicted, target_names=['yes', 'no'],
                                                                 digits=2)

    print
    print "Report"
    print output_classification_report
    print

    confusion_matrix = metrics.confusion_matrix(Y_test, y_predicted)
    print
    print("Confusion Matrix: True-Classes X Predicted-Classes")
    print(confusion_matrix)
    print

    # Compute the accuracy classification score normalized (best performance 1)
    accuracy_score_normalized = metrics.accuracy_score(Y_test, y_predicted, normalize=True)
    print
    print("Accurancy Score Normalized: float")
    print(accuracy_score_normalized)
    print
