from src.utils import database

from sklearn import metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import LinearSVC
import sklearn.metrics as skmetric
from sklearn.datasets import load_files
from sklearn.decomposition import TruncatedSVD

from nltk.corpus import stopwords
from nltk.stem.snowball import EnglishStemmer
from nltk import word_tokenize

import pprint as pp
import numpy as np
from nltk.corpus import words

RELATION = u'GENERALIZATION'
stemmer = EnglishStemmer()


def stemming_tokenizer(text):
    stemmed_text = [stemmer.stem(word) for word in word_tokenize(text, language='english')]
    return stemmed_text


if __name__ == '__main__':
    # entries = database.get_all_qas(RELATION)
    entries_yn = database.get_yes_no_qas(RELATION, yes_no=True)
    entries_cl = database.get_yes_no_qas(RELATION, yes_no=False)

    questions_yn = [entry[0] for entry in entries_yn]
    yn_values_yn = [entry[10] for entry in entries_yn]

    questions_cl = [entry[0] for entry in entries_cl]
    yn_values_cl = [entry[10] for entry in entries_cl]

    target_names = ['questions', 'yn']

    # Load Training-Set
    # X_train_yn, X_test_DUMMY_yn, Y_train_yn, Y_test_DUMMY_yn = train_test_split(questions_yn, yn_values_yn,
    #                                                                             test_size=0.2)
    # X_train_cl, X_test_DUMMY_cl, Y_train_cl, Y_test_DUMMY_cl = train_test_split(questions_cl, yn_values_cl,
    #                                                                             test_size=0.2)

    X_train_dyn, X_test_dyn, Y_train_dyn, Y_test_dyn = train_test_split(questions_yn, yn_values_yn, train_size=1000,
                                                                        test_size=1000)
    X_train_dcl, X_test_dcl, Y_train_dcl, Y_test_dcl = train_test_split(questions_cl, yn_values_cl, train_size=1000,
                                                                        test_size=1000)

    X_train, Y_train = X_train_dcl + X_train_dyn, Y_train_dcl + Y_train_dyn
    X_test, Y_test = X_test_dcl + X_test_dyn, Y_test_dcl + Y_test_dyn

    n_grams = [(x, y) for x in xrange(1, 3, 1) for y in xrange(1, 10, 1) if x < y]
    print n_grams
    # Vectorization object
    # lowercase = True, because stopwords are lowercase
    vectorizer = TfidfVectorizer(strip_accents=None, preprocessor=None)
    # decompositor = TruncatedSVD()
    svm = LinearSVC()
    classifiers = [
        ['SVC', LinearSVC()],
        ['KNN', KNeighborsClassifier(n_jobs=1, )],
    ]

    parameters_list = {
        'KNN': {
            # 'dec': TruncatedSVD(),
            'vect__tokenizer': [None, stemming_tokenizer],
            'vect__ngram_range': n_grams,
            # 'vect__analyzer': ['word', 'char'],
            # 'vect__max_df': np.arange(.8, 1., .1),
            # 'vect__min_df': np.arange(0., .2, .1),
            # 'vect__binary': [True, False],
            'vect__lowercase': [True, False],
            # 'vect__sublinear_tf': [True, False],
            'vect__stop_words': [None, stopwords.words("english")],

            # 'dec__n_components': xrange(10, 15, 2),
            'nbc__n_neighbors': xrange(1, 12, 1),
            # 'nbc__weights': ['distance', 'uniform'],
        },
        'SVC': {
            'vect__tokenizer': [None, stemming_tokenizer],
            'vect__ngram_range': n_grams,
            # 'vect__analyzer': ['word', 'char'],
            # 'vect__max_df': np.arange(.8, 1., .1),
            # 'vect__min_df': np.arange(0., .2, .1),
            # 'vect__binary': [True, False],
            'vect__lowercase': [True, False],
            # 'vect__sublinear_tf': [True, False],
            'vect__stop_words': [None, stopwords.words("english")],
        }}

    for classifier in classifiers:
        classifier_name, classifier_estimator = classifier[0], classifier[1]
        print '*' * 5, classifier_name, '*' * 5

        pipeline = Pipeline([
            ('vect', vectorizer),
            ('nbc', classifier_estimator),
        ])

        parameters = parameters_list[classifier_name]

        grid_search = GridSearchCV(pipeline, parameters, pre_dispatch=10, refit=True,
                                   scoring=metrics.make_scorer(skmetric.matthews_corrcoef),
                                   error_score=0, n_jobs=8, verbose=1, )

        grid_search.fit(X_train, Y_train)

        # Print results for each combination of parameters.print
        number_of_candidates = len(grid_search.cv_results_['params'])
        """ print("Results:")
        for i in range(number_of_candidates):
            print(i, 'params - %s; mean - %0.3f; std - %0.3f' %
                  (grid_search.cv_results_['params'][i],
                   grid_search.cv_results_['mean_test_score'][i],
                   grid_search.cv_results_['std_test_score'][i]))
        """
        print
        print("Best Estimator:")
        pp.pprint(grid_search.best_estimator_)
        print
        print("Best Parameters:")
        pp.pprint(grid_search.best_params_)
        print
        print("Used Scorer Function:")
        pp.pprint(grid_search.scorer_)
        print("Number of Folds:")
        pp.pprint(grid_search.n_splits_)
        print
        print ("Best Score")
        pp.pprint(grid_search.best_score_)
        print
        Y_predicted = grid_search.predict(X_test)

        # Evaluate the performance of the classifier on the original Test-Set
        output_classification_report = metrics.classification_report(Y_test, Y_predicted, target_names=target_names,
                                                                     digits=2)
        confusion_matrix = metrics.confusion_matrix(Y_test, Y_predicted)
        print
        print("Confusion Matrix: True-Classes X Predicted-Classes")
        print(confusion_matrix)
        print

        # Compute the accuracy classification score normalized (best performance 1)
        accuracy_score_normalized = metrics.accuracy_score(Y_test, Y_predicted, normalize=True)
        print
        print("Accurancy Score Normalized: float")
        print(accuracy_score_normalized)
        print

        print '*' * 100
