import spacy
from ..utils import domains_utils


def merge_(doc, start, end):
    """
    merges two nodes in the tree

    :param doc: spacy document
    :param int start:
    :param int end:
    :return: merged token
    """
    if start < end:
        span = doc[start: end]
    else:
        span = doc[end: start]

    token = span.merge()
    return token

patterns = domains_utils.get_patterns_for_relations(relations=[u'GENERALIZATION'])

nlp = spacy.load('en')

q1 = u'Are title track examples of loosely-coupled?'

doc = nlp(q1)
for ent in doc.ents:
    merge_(doc, ent.start, ent.end)

for token in doc:
    print token.orth_, ": ", token.dep_, " ", token.pos_