from sklearn import metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import SGDClassifier

from nltk.corpus import stopwords
from nltk.stem.snowball import EnglishStemmer
from nltk.tokenize import word_tokenize

import pprint as pp
import unicodecsv
import pickle
import gzip


CLASSIFIER_PATH = '../classifiers/chosen/yes_no_maybe_classifier.pickle'
TAGS = ['YES', 'NO', 'MAYBE']


def parse(path):
    g = gzip.open(path, 'r')
    for l in g:
        yield eval(l)


def stemming_tokenizer(text):
    stemmer = EnglishStemmer()
    stemmed_text = [stemmer.stem(word) for word in word_tokenize(text, language='english')]
    return stemmed_text


def get_data():
    tsvfile = open('../data/ynm_answers.tsv', mode='r')
    reader = unicodecsv.reader(tsvfile, encoding='utf8', dialect="excel-tab")
    answers, tags = [], []
    for line in reader:
        answers.append(line[0])
        tags.append(line[1])

    return answers, tags


def get_best_estimator(verbose=0):
    target_names = ['yes', 'no', 'maybe']

    answers, tags = get_data()

    X_train, X_test, Y_train, Y_test = train_test_split(answers, tags, test_size=0.3)
    vectorizer = TfidfVectorizer(strip_accents=None, preprocessor=None)
    svm = SGDClassifier()

    n_grams = [(x, y) for x in xrange(1, 3, 1) for y in xrange(1, 10, 1) if x < y]

    pipeline = Pipeline([
        ('vect', vectorizer),
        ('svm', svm),
    ])

    parameters = {
        'vect__tokenizer': [None, stemming_tokenizer],
        'vect__ngram_range': n_grams,
        # 'vect__analyzer': ['word', 'char'],
        # 'vect__max_df': np.arange(.8, 1., .1),
        # 'vect__min_df': np.arange(0., .2, .1),
        # 'vect__binary': [True, False],
        'vect__lowercase': [True, False],
        # 'vect__sublinear_tf': [True, False],
        'vect__stop_words': [None, stopwords.words("english")],
    }

    grid_search = GridSearchCV(pipeline, parameters, pre_dispatch=10, refit=True, cv=5,
                               # scoring=metrics.make_scorer(skmetric.),
                               error_score=0, n_jobs=8, verbose=verbose, )

    grid_search.fit(X_train, Y_train)
    if verbose >= 1:
        # Print results for each combination of parameters.print
        number_of_candidates = len(grid_search.cv_results_['params'])
        print "Number of candidates: ", number_of_candidates

        print
        print("Best Estimator:")
        pp.pprint(grid_search.best_estimator_)
        print
        print("Best Parameters:")
        pp.pprint(grid_search.best_params_)
        print
        print("Used Scorer Function:")
        pp.pprint(grid_search.scorer_)
        print("Number of Folds:")
        pp.pprint(grid_search.n_splits_)
        print
        print ("Best Score")
        pp.pprint(grid_search.best_score_)
        print
        y_predicted = grid_search.predict(X_test)

        # Evaluate the performance of the classifier on the original Test-Set
        output_classification_report = metrics.classification_report(Y_test, y_predicted, target_names=target_names,
                                                                     digits=2)

        print
        print "Report"
        print output_classification_report
        print

        confusion_matrix = metrics.confusion_matrix(Y_test, y_predicted)
        print
        print("Confusion Matrix: True-Classes X Predicted-Classes")
        print(confusion_matrix)
        print

        # Compute the accuracy classification score normalized (best performance 1)
        accuracy_score_normalized = metrics.accuracy_score(Y_test, y_predicted, normalize=True)
        print
        print("Accurancy Score Normalized: float")
        print(accuracy_score_normalized)
        print

    return grid_search.best_estimator_


def fit_svc(svc=None, test_size=0.0, verbose=0):
    """

    :param Pipeline svc:
    :param test_size:
    :param verbose:
    :return:
    """
    if svc is None:
        svc = get_best_estimator(verbose=verbose)  # type:Pipeline

    answers, tags = get_data()
    X_train, X_test, Y_train, Y_test = train_test_split(answers, tags, test_size=test_size)
    svc = svc.fit(X_train, Y_train)

    if test_size != 0.:
        Y_predicted = svc.predict(X_test)

        if verbose >= 1:
            confusion_matrix = metrics.confusion_matrix(Y_test, Y_predicted)
            print
            print("Confusion Matrix: True-Classes X Predicted-Classes")
            print(confusion_matrix)
            print

    return svc


if __name__ == '__main__':
    best_estimator = get_best_estimator(verbose=1)
    print
    print '*'*50
    print
    best_estimator = fit_svc()
    pickle.dump(best_estimator, open(CLASSIFIER_PATH, mode='w'), pickle.HIGHEST_PROTOCOL)
    x1 = ['I really do not know']
    print "Prediction of ", x1[0], ': ', str(best_estimator.predict(x1))

    x1 = ['i would guess yes']
    print "Prediction of ", x1[0], ': ', str(best_estimator.predict(x1))

    x1 = ['i thing no']
    print "Prediction of ", x1[0], ': ', str(best_estimator.predict(x1))
