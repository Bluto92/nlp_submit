from src.utils import database
import pickle
import os

from sklearn import metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.svm import LinearSVC
import sklearn.metrics as skmetric

from nltk.corpus import stopwords
from nltk.stem.snowball import EnglishStemmer
from nltk import word_tokenize

import pprint as pp

# RELATION = u'GENERALIZATION'
stemmer = EnglishStemmer()


def stemming_tokenizer(text):
    stemmed_text = [stemmer.stem(word) for word in word_tokenize(text, language='english')]
    return stemmed_text


def get_best_estimator(relations, verbose=0, train_size=1000, test_size=1000):
    X_train, Y_train, X_test, Y_test = [], [], [], []
    train_size_pr = abs(train_size / len(relations))
    test_size_pr = abs(test_size / len(relations))
    target_names = ['questions', 'yn']

    for relation in relations:
        entries_yn = database.get_yes_no_qas(relation, yes_no=True)
        entries_cl = database.get_yes_no_qas(relation, yes_no=False)

        questions_yn = [entry[0] for entry in entries_yn]
        yn_values_yn = [entry[10] for entry in entries_yn]

        questions_cl = [entry[0] for entry in entries_cl]
        yn_values_cl = [entry[10] for entry in entries_cl]

        if test_size_pr + train_size_pr < len(questions_yn):
            X_train_dyn, X_test_dyn, Y_train_dyn, Y_test_dyn = train_test_split(questions_yn, yn_values_yn,
                                                                                train_size=train_size_pr,
                                                                                test_size=test_size_pr)
        else:
            X_train_dyn, X_test_dyn, Y_train_dyn, Y_test_dyn = train_test_split(questions_yn, yn_values_yn,
                                                                                test_size=0.3)
        if test_size_pr + train_size_pr < len(questions_cl):
            X_train_dcl, X_test_dcl, Y_train_dcl, Y_test_dcl = train_test_split(questions_cl, yn_values_cl,
                                                                                train_size=train_size_pr,
                                                                                test_size=test_size_pr)
        else:
            X_train_dcl, X_test_dcl, Y_train_dcl, Y_test_dcl = train_test_split(questions_cl, yn_values_cl,
                                                                                test_size=0.3)

        X_train += X_train_dcl + X_train_dyn
        Y_train += Y_train_dcl + Y_train_dyn
        X_test += X_test_dcl + X_test_dyn
        Y_test += Y_test_dcl + Y_test_dyn

    vectorizer = TfidfVectorizer(strip_accents=None, preprocessor=None)
    svm = LinearSVC()

    n_grams = [(x, y) for x in xrange(1, 3, 1) for y in xrange(1, 10, 1) if x < y]

    pipeline = Pipeline([
        ('vect', vectorizer),
        ('svm', svm),
    ])

    parameters = {
        'vect__tokenizer': [None, stemming_tokenizer],
        'vect__ngram_range': n_grams,
        # 'vect__analyzer': ['word', 'char'],
        # 'vect__max_df': np.arange(.8, 1., .1),
        # 'vect__min_df': np.arange(0., .2, .1),
        # 'vect__binary': [True, False],
        'vect__lowercase': [True, False],
        # 'vect__sublinear_tf': [True, False],
        'vect__stop_words': [None, stopwords.words("english")],
    }

    grid_search = GridSearchCV(pipeline, parameters, pre_dispatch=10, refit=True,
                               scoring=metrics.make_scorer(skmetric.matthews_corrcoef),
                               error_score=0, n_jobs=8, verbose=verbose, )

    grid_search.fit(X_train, Y_train)
    if verbose >= 1:
        # Print results for each combination of parameters.print
        number_of_candidates = len(grid_search.cv_results_['params'])
        print "Number of candidates: ", number_of_candidates

        print
        print("Best Estimator:")
        pp.pprint(grid_search.best_estimator_)
        print
        print("Best Parameters:")
        pp.pprint(grid_search.best_params_)
        print
        print("Used Scorer Function:")
        pp.pprint(grid_search.scorer_)
        print("Number of Folds:")
        pp.pprint(grid_search.n_splits_)
        print
        print ("Best Score")
        pp.pprint(grid_search.best_score_)
        print
        y_predicted = grid_search.predict(X_test)

        # Evaluate the performance of the classifier on the original Test-Set
        output_classification_report = metrics.classification_report(Y_test, y_predicted, target_names=target_names,
                                                                     digits=2)

        print
        print "Report"
        print output_classification_report
        print

        confusion_matrix = metrics.confusion_matrix(Y_test, y_predicted)
        print
        print("Confusion Matrix: True-Classes X Predicted-Classes")
        print(confusion_matrix)
        print

        # Compute the accuracy classification score normalized (best performance 1)
        accuracy_score_normalized = metrics.accuracy_score(Y_test, y_predicted, normalize=True)
        print
        print("Accurancy Score Normalized: float")
        print(accuracy_score_normalized)
        print

        return grid_search.best_estimator_


def fit_svc(relations, svc=None, test_size=0.0, verbose=0):
    """

    :param Pipeline svc:
    :param test_size:
    :param verbose:
    :return:
    """
    if svc is None:
        svc = get_best_estimator(verbose=verbose, relations=relations)  # type:Pipeline
    X_train, Y_train, X_test, Y_test = [], [], [], []
    for relation in relations:
        entries_yn = database.get_yes_no_qas(relation, yes_no=True)
        entries_cl = database.get_yes_no_qas(relation, yes_no=False)

        questions_yn = [entry[0] for entry in entries_yn]
        yn_values_yn = [entry[10] for entry in entries_yn]

        questions_cl = [entry[0] for entry in entries_cl]
        yn_values_cl = [entry[10] for entry in entries_cl]

        X_train_dyn, X_test_dyn, Y_train_dyn, Y_test_dyn = train_test_split(questions_yn, yn_values_yn,
                                                                            test_size=test_size)
        X_train_dcl, X_test_dcl, Y_train_dcl, Y_test_dcl = train_test_split(questions_cl, yn_values_cl,
                                                                            test_size=test_size)

        X_train += X_train_dcl + X_train_dyn
        Y_train += Y_train_dcl + Y_train_dyn
        X_test += X_test_dcl + X_test_dyn
        Y_test += Y_test_dcl + Y_test_dyn

    svc = svc.fit(X_train, Y_train)

    if test_size != 0.:
        Y_predicted = svc.predict(X_test)

        if verbose >= 1:
            confusion_matrix = metrics.confusion_matrix(Y_test, Y_predicted)
            print
            print("Confusion Matrix: True-Classes X Predicted-Classes")
            print(confusion_matrix)
            print

    return svc


class ClassifierLoader(object):
    def __init__(self, classifier_name):
        self.classifier_name = classifier_name

    def load_classifier(self):
        if os.path.exists('../classifiers/' + self.classifier_name):
            return pickle.load(open('../classifiers/' + self.classifier_name, mode='r'))
        return None


if __name__ == '__main__':
    # if os.path.exists('../classifiers/question_type_classifier.pickle'):
    #     SVC = pickle.load(open('../classifiers/question_type_classifier.pickle', mode='r'))
    # else:
    from ..utils import domains_utils

    SVC = fit_svc(verbose=1, relations=domains_utils.get_all_relations_kbs())
    pickle.dump(SVC, open('../classifiers/question_type_classifier_all.pickle', mode='w+'))

    x1 = ['is the cat an animal?', 'is the car an example vehicle?']
    y_pred1 = SVC.predict(x1)

    x2 = ['who are you?', 'which is an example of car?']
    y_pred2 = SVC.predict(x2)

    print x1, " ", y_pred1
    print x2, " ", y_pred2
