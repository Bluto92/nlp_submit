from src.utils import database, domains_utils
import pickle
import os
import numpy
from sklearn import metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import LinearSVC
from sklearn.linear_model import SGDClassifier
from tqdm import tqdm
from nltk.corpus import stopwords
from nltk.stem.snowball import EnglishStemmer
from nltk import word_tokenize

import pprint as pp

stemmer = EnglishStemmer()


def stemming_tokenizer(text):
    stemmed_text = [stemmer.stem(word) for word in word_tokenize(text, language='english')]
    return stemmed_text


def get_best_classifier(relations):
    classifiers = [MultinomialNB(), LinearSVC(), SGDClassifier()]
    bestimator = None
    bescore = 0.
    with tqdm(total=len(relations)) as td:
        for classifier in classifiers:
            td.desc = str(type(classifier)) + " "
            td.refresh()

            stimator = get_best_estimator(relations=relations, classifier=classifier)  # type: Pipeline
            if bestimator.score > bescore:
                bestimator = stimator

            td.update(1)


# def mlpc_estimate(relations, train_size=2000, test_size=1000):
#     vectorizer = TfidfVectorizer(strip_accents='unicode', tokenizer=word_tokenize, analyzer='word', lowercase=True)
#     mlp = MLPClassifier(verbose=True)
#     pipeline = Pipeline([
#         ('vect', vectorizer),
#         ('clf', mlp)
#     ])
#
#     X_train, Y_train, X_test, Y_test = [], [], [], []
#     train_size_pr = abs(train_size / len(relations))
#     test_size_pr = abs(test_size / len(relations))
#     target_names = []
#     relation2index = {relation: index for index, relation in enumerate(relations)}
#     with tqdm(total=len(relations)) as td:
#         for relation in relations:
#             td.desc = relation + " "
#             td.refresh()
#
#             target_names.append(relation)
#             entries = database.get_all_qas(relation)
#             questions = [entry[0] for entry in entries]
#
#             if test_size_pr + train_size_pr < len(questions):
#                 X_train_rel, X_test_rel = train_test_split(questions,
#                                                            train_size=train_size_pr, test_size=test_size_pr)
#             else:
#                 X_train_rel, X_test_rel = train_test_split(questions, test_size=0.3)
#             Y_train_rel = [relation2index[relation] for _ in numpy.arange(0, len(X_train_rel), 1)]
#             Y_test_rel = [relation2index[relation] for _ in numpy.arange(0, len(X_test_rel), 1)]
#
#             X_train += X_train_rel
#             X_test += X_test_rel
#             Y_train += Y_train_rel
#             Y_test += Y_test_rel
#             td.update(1)
#     print
#
#     pipeline = pipeline.fit(X_train, Y_train, )
#     print pipeline.score(X_test, Y_test)


def get_best_estimator(relations, classifier, verbose=0, train_size=2000, test_size=1000):
    vectorizer = TfidfVectorizer(strip_accents='unicode')
    n_grams = [(x, y) for x in xrange(1, 3, 1) for y in xrange(1, 10, 1) if x < y]

    pipeline = Pipeline([
        ('vect', vectorizer),
        # ('mnb', mnb),
        ('clf', classifier)
    ])

    parameters = {
        'vect__tokenizer': [None, stemming_tokenizer, word_tokenize],
        'vect__ngram_range': n_grams,
        'vect__analyzer': ['word', 'char'],
        # 'vect__max_df': np.arange(.8, 1., .1),
        # 'vect__min_df': np.arange(0., .2, .1),
        # 'vect__binary': [True, False],
        'vect__lowercase': [True, False],
        'vect__sublinear_tf': [True, False],
        'vect__stop_words': [None, stopwords.words("english")],

        # 'clf__n_neighbors': [5, 10, 25, 50],
    }

    grid_search = GridSearchCV(pipeline, parameters, pre_dispatch=10, refit=True,
                               # scoring=metrics.make_scorer(skmetric.cluster.normalized_mutual_info_score),
                               error_score=0, n_jobs=8, verbose=verbose)

    X_train, Y_train, X_test, Y_test = [], [], [], []
    train_size_pr = abs(train_size / len(relations))
    test_size_pr = abs(test_size / len(relations))
    target_names = []
    relation2index = {relation: index for index, relation in enumerate(relations)}
    with tqdm(total=len(relations)) as td:
        for relation in relations:
            td.desc = relation + " "
            td.refresh()

            target_names.append(relation)
            entries = database.get_all_qas(relation)
            questions = [entry[0] for entry in entries]

            if test_size_pr + train_size_pr < len(questions):
                X_train_rel, X_test_rel = train_test_split(questions, train_size=train_size_pr, test_size=test_size_pr)
            else:
                X_train_rel, X_test_rel = train_test_split(questions, test_size=0.3)
            Y_train_rel = [relation2index[relation] for _ in numpy.arange(0, len(X_train_rel), 1)]
            Y_test_rel = [relation2index[relation] for _ in numpy.arange(0, len(X_test_rel), 1)]

            X_train += X_train_rel
            X_test += X_test_rel
            Y_train += Y_train_rel
            Y_test += Y_test_rel
            td.update(1)
    print

    grid_search.fit(X_train, Y_train)
    if verbose >= 1:
        # Print results for each combination of parameters.print
        number_of_candidates = len(grid_search.cv_results_['params'])
        print "Number of candidates: ", number_of_candidates

        print
        print("Best Estimator:")
        pp.pprint(grid_search.best_estimator_)
        print
        print("Best Parameters:")
        pp.pprint(grid_search.best_params_)
        print
        print("Used Scorer Function:")
        pp.pprint(grid_search.scorer_)
        print("Number of Folds:")
        pp.pprint(grid_search.n_splits_)
        print
        print ("Best Score")
        pp.pprint(grid_search.best_score_)
        print
        y_predicted = grid_search.predict(X_test)

        # Evaluate the performance of the classifier on the original Test-Set
        output_classification_report = metrics.classification_report(Y_test, y_predicted,
                                                                     target_names=target_names,
                                                                     digits=2)

        print
        print "Report"
        print output_classification_report
        print

        confusion_matrix = metrics.confusion_matrix(Y_test, y_predicted)
        print
        print("Confusion Matrix: True-Classes X Predicted-Classes")
        print(confusion_matrix)
        print

        # Compute the accuracy classification score normalized (best performance 1)
        accuracy_score_normalized = metrics.accuracy_score(Y_test, y_predicted, normalize=True)
        print
        print("Accurancy Score Normalized: float")
        print(accuracy_score_normalized)
        print

        return grid_search.best_estimator_, accuracy_score_normalized


def fit_classifier(relations, classifier=None, test_size=0.0, verbose=0):
    """

    :param List[str | unicode] relations:
    :param Pipeline classifier:
    :param float test_size:
    :param int verbose:
    :return:
    """
    if classifier is None:
        classifier = get_best_estimator(relations=relations, classifier=SGDClassifier(),
                                        verbose=verbose)  # type: Pipeline

    X_train, Y_train, X_test, Y_test = [], [], [], []
    relation2index = {relation: index for index, relation in enumerate(relations)}
    with tqdm(total=len(relations)) as td:
        for relation in relations:
            td.desc = relation + " "
            td.refresh()

            entries = database.get_all_qas(relation)
            questions = [entry[0] for entry in entries]

            X_train_rel, X_test_rel = train_test_split(questions, test_size=test_size)
            Y_train_rel = [relation2index[relation] for _ in numpy.arange(0, len(X_train_rel), 1)]
            Y_test_rel = [relation2index[relation] for _ in numpy.arange(0, len(X_test_rel), 1)]

            X_train += X_train_rel
            X_test += X_test_rel
            Y_train += Y_train_rel
            Y_test += Y_test_rel
            td.update(1)

    classifier = classifier.fit(X_train, Y_train)

    if test_size != 0.:
        Y_predicted = classifier.predict(X_test)

        if verbose >= 1:
            confusion_matrix = metrics.confusion_matrix(Y_test, Y_predicted)

            print
            print("Confusion Matrix: True-Classes X Predicted-Classes")
            print(confusion_matrix)
            print

        print classifier.score(X_test, Y_test)
    return classifier


class ClassifierLoader(object):
    def __init__(self, classifier_name):
        self.classifiers_dir = '../classifiers/'
        self.classifier_name = classifier_name

    def load_classifier(self):
        """

        :return:
        :rtype: Pipeline
        """
        if os.path.exists(self.classifiers_dir):
            if os.path.exists(self.classifiers_dir + "chosen/" + self.classifier_name):
                return pickle.load(open(self.classifiers_dir + "chosen/" + self.classifier_name, mode='r'))
            elif os.path.exists(self.classifiers_dir + "others/" + self.classifier_name):
                return pickle.load(open(self.classifiers_dir + "others/" + self.classifier_name, mode='r'))
        return None


if __name__ == '__main__':
    classifier_dir = '../classifiers/chosen/'
    # classifier_path = classifier_dir + "relation_classifier_education_0.99.pickle"
    # classifier = pickle.load(open(classifier_path, mode='r'))
    # while True:
    #     question = raw_input("Input a question: ")
    #     print str(classifier.predict([question]))

    # """
    for domain in domains_utils.get_all_domains():
        print domain
        relations = domains_utils.get_relations_for_domain_kbs(domain=domain)
        classifier, best_score = get_best_estimator(relations=relations, classifier=SGDClassifier(), verbose=1)

        # classifiers_dir = '../classifiers/'
        # score = "%.2f" % best_score

        # classifier_path = classifiers_dir + 'relation_classifier_' + \
        #                   domain.replace(' ', '_').lower() + '_' + score + '.pickle'
        # pickle.dump(classifier, open(classifier_path, mode='w+'), protocol=pickle.HIGHEST_PROTOCOL)

        # clf = fit_classifier(relations=domains_utils.get_all_relations_kbs(), classifier=classifier,
        #                      verbose=1, test_size=.3)
        print
        # """
