from matplotlib import pyplot
import seaborn
seaborn.set()
import sys
import scipy.stats
from utils import ioutils, domains_utils


def data_visualization(relations=domains_utils.get_all_relations_kbs(), lengths_questions=None, lengths_answers=None,
                       qxlimit=sys.maxint, axlimit=sys.maxint):
    if lengths_questions is None or lengths_answers is None:
        lengths_questions, lengths_answers = ioutils.get_question_answer_lengths(relations=relations)

    import pprint
    print
    print "Length Answer"
    pprint.pprint(lengths_answers)
    print

    fig, axs = pyplot.subplots(3, 2, sharey='row', sharex='row')

    # X Ticks
    x_tick_max = max(lengths_questions.keys() + lengths_answers.keys())
    x_peak = 0
    y_peak = 0
    for x, y in lengths_questions.items():
        if y > y_peak:
            x_peak = x
            y_peak = y

    # Bars - Linear Scale
    ax00 = axs[0][0]
    ax01 = axs[0][1]

    cat_questions_x = [x for x, y in lengths_questions.items() if x <= qxlimit]
    cat_questions_y = [y for x, y in lengths_questions.items() if x <= qxlimit]

    cat_answers_x = [x for x, y in lengths_answers.items() if x <= axlimit]
    cat_answers_y = [y for x, y in lengths_answers.items() if x <= axlimit]

    x_tick_max = max(cat_answers_x + cat_questions_x)
    x_ticks = range(1, x_tick_max + 1, 1) + [x_peak]
    ax00.set_xticks(x_ticks)
    ax01.set_xticks(x_ticks)

    ax00.bar(cat_questions_x,
             cat_questions_y,
             width=0.2, color='b')

    ax01.bar(cat_answers_x,
             cat_answers_y,
             width=0.2, color='r')

    # Bars - Ln scale
    ax10 = axs[1][0]
    ax11 = axs[1][1]

    ax10.set_yscale('log')

    ax10.set_xticks(x_ticks)
    ax11.set_xticks(x_ticks)

    ax10.bar(cat_questions_x,
             cat_questions_y,
             width=0.2, color='b')

    ax11.bar(cat_answers_x,
             cat_answers_y,
             width=0.2, color='r')

    # Line - Linear Scale

    ax20 = axs[2][0]
    ax21 = axs[2][1]

    from copy import deepcopy
    distr_q = deepcopy(lengths_questions)
    distr_a = deepcopy(lengths_answers)

    print "Question"
    distr_q_x, distr_q_y = reject_outliers(distr_q)

    print
    print "Answer"
    distr_a_x, distr_a_y = reject_outliers(distr_a)

    ax20.plot(distr_q_x, distr_q_y, color='b')
    ax21.plot(distr_a_x, distr_a_y, color='r')

    x_ticks = range(1, x_tick_max + 1, 1) + [x_peak, x_tick_max]
    ax20.set_xticks(x_ticks)
    ax21.set_xticks(x_ticks)

    pyplot.show()


def best_interval(relation):
    """

    :param str | unicode relation:
    :return:
    :rtype: Tuple[int, int, int, int]
    """
    lengths_questions, lengths_answers = ioutils.get_question_answer_lengths(relations=[relation])
    x_q, _ = reject_outliers(lengths_questions)
    x_a, _ = reject_outliers(lengths_answers)

    max_q, max_a = max(x_q), max(x_a)
    min_q, min_a = min(x_q), min(x_q)
    print "Best Question Interval is ", str(min_q), "-", str(max_q), \
        " and Answer Interval is ", str(min_a), "-", str(max_a)
    return min_q, max_q, min_a, max_a


def reject_outliers(data):
    """

    :param dict[x, y] data:
    :return :
    :rtype: Tuple[numpy.array, numpy.array]
    """

    import numpy
    mean = numpy.mean(numpy.asarray(data.values(), dtype=int))
    dev_std = numpy.std(numpy.asarray(data.values(), dtype=int))

    print "Mean: ", str(mean)
    print "Standard Deviation: ", str(dev_std)

    lim = mean - dev_std
    if lim < 0:
        lim *= -1

    print "Limit: ", str(lim)

    x_max = 0
    for x, occ in data.items():
        if occ > lim:
            x_max = x

    data_x, data_y = [], []
    x_sorted = sorted(data.keys(), reverse=True)
    for x in x_sorted:
        if x <= x_max:
            data_x.append(x)
            data_y.append(data[x])

    return numpy.asarray(data_x, dtype=int), numpy.asarray(data_y, dtype=int)


def extract_probability_distribution(relations=domains_utils.get_all_relations_kbs()):
    dist_names = ['gamma', 'beta', 'lognorm', 'rayleigh', 'norm', 'pareto']
    lengths_questions, lengths_answers = ioutils.get_question_answer_lengths(relations=relations)
    size = 30000

    import scipy.stats
    data_questions = []
    max_x = 0
    for q_length, l_occ in lengths_questions.items():
        lq_arr = scipy.asarray([q_length, l_occ], dtype=int)
        data_questions.append(lq_arr)
        if q_length > max_x:
            max_x = q_length

    # data_answers = []
    # for a_length, a_occ in lengths_answers.items():
    #     la_arr = scipy.asarray([a_length, a_occ], dtype=int)
    #     data_answers.append(la_arr)

    h = pyplot.hist(data_questions, bins=max_x + 1)

    for dist_name in dist_names:
        dist = getattr(scipy.stats, dist_name)
        param = dist.fit(data_questions)
        pdf_fitted = dist.pdf(scipy.arange(size), *param[:-2], loc=param[-2],
                              scale=param[-1]) * size

        pyplot.plot(pdf_fitted, label=dist_name)
        pyplot.xlim(0, max_x)

    pyplot.legend(loc='upper right')
    pyplot.show()


if __name__ == "__main__":
    # data_visualization(relations=[u'GENERALIZATION'])

    # extract_probability_distribution(relations=[u'COLOR'])
    from utils import marcods
    stats = marcods.stats_marco()
    data_visualization(lengths_questions=stats['queries'], lengths_answers=stats['answers'], qxlimit=20, axlimit=25)
