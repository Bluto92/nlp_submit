from matplotlib import pyplot
import seaborn
from ..utils import ioutils
from nltk.tokenize import word_tokenize


if __name__ == "__main__":
    lengths_questions, lengths_answers = {}, {}

    for data_entry in ioutils.generator_data_from_chunks():
        question = data_entry['question']
        answer = data_entry['entry']
        relation = data_entry['relation']

        question_tokens = word_tokenize(question)
        answer_tokens = word_tokenize(answer)

        question_number_words = len(question_tokens)
        answer_number_words = len(answer_tokens)

        current_qnw = lengths_questions.get(question_number_words, 0)
        current_anw = lengths_answers.get(answer_number_words, 0)

        lengths_questions[question_number_words] = current_qnw + 1
        lengths_answers[answer_number_words] = current_anw + 1

    import numpy
    resbar = pyplot.bar(numpy.arange(len(lengths_questions)),
                        lengths_questions.values())
    pyplot.show()

