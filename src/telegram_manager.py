import telepot
from telepot.loop import MessageLoop
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton
import pprint as pp
from time import sleep
from querying import Answerer, AnswerData
from enriching import Enricher, EnrichingData
from utils.database import KBDataWrapper
from shared_kb_manager import KBUpdater

# noinspection PyUnresolvedReferences
from classifiers.question_type_classifier import stemming_tokenizer

from utils import domains_utils

import logging

logging.basicConfig(filename='../logs/tgrambot.log', level=logging.DEBUG,
                    format='%(asctime)s - %(name)s -  %(message)s')
logger = logging.getLogger(__name__)

# GLOBALS
Status = {}
BotAnswerer = Answerer()
BotEnricher = Enricher()  # type: Enricher
Verbosity = {}
DomainChoice = {}
chat2data = {}

# print "Bot info"
bot = telepot.Bot('436238228:AAHT5hAWCKvCk-rQdCI2kXVnF5P3V6SgdFo')
domains = {"/a": "Education", "/b": "Farming", "/c": "Mathematics", "/d": "History",
           "/e": "Literature and Theatre", "/f": "Health and Medicine"}


def handle(msg):
    global Status  # type: dict
    global BotAnswerer
    global DomainChoice
    global BotEnricher
    global Verbosity

    print "Handling Message: "
    pp.pprint(msg)
    content_type, chat_type, chat_id = telepot.glance(msg)
    print telepot.glance(msg)
    text = msg.get('text', '')  # type: str
    # noinspection PyBroadException
    try:

        if content_type != 'text':
            bot.sendMessage(chat_id=chat_id,
                            text=u"Sorry, only Text messages are allowed")
            return
        if is_command(text):
            if text.lower().startswith(u'/verbose '):
                value = text.split()[1]
                if value.isdigit():
                    Verbosity[chat_id] = int(value) if int(value) <= 5 else 5
                    bot.sendMessage(chat_id=chat_id, text="Verbosity set to " + str(Verbosity[chat_id]),
                                    reply_markup=None)
                    print "Set verbosity to ", Verbosity
                    return
            elif text.lower() == "/relations":
                if chat_id in DomainChoice:
                    relations = domains_utils.get_relations_for_domain_kbs(domains[DomainChoice[chat_id]])
                    response = u'The relation in the domain "' + domains[DomainChoice[chat_id]] \
                               + u'" are the following: '
                    for relation in relations:
                        nice_relation = relation[0].upper() + relation.lower()[1:]
                        response += nice_relation + u", "
                else:
                    response = u'Please, select a domain first'
                bot.sendMessage(chat_id=chat_id, text=response.rstrip(', '), reply_markup=None)

            elif text.lower() == "/help":
                help_message = "/help: displays this help\n" \
                               "/relations: displays the relation for the selected domain\n" \
                               "/stop: stops the workflow\n" \
                               "/verbose: set the verbosity of the answer [0, 5] (DEBUG)\n"
                bot.sendMessage(chat_id=chat_id, text=help_message, reply_markup=None)
            elif text.lower() == "/stop":
                Status[chat_id] = 0
                chat2data.pop(chat_id, None)
                return

        if Status.setdefault(chat_id, 0) == 0:
            kb_markup1 = ReplyKeyboardMarkup(keyboard=[[
                KeyboardButton(text="/Querying"),
                KeyboardButton(text="/Enriching")]],
                one_time_keyboard=True)

            bot.sendMessage(chat_id=chat_id,
                            text=u"Welcome Man! What do you want to do?",
                            reply_markup=kb_markup1, )
            Status[chat_id] += 1

        elif Status[chat_id] == 1:
            if text == "/Querying" or text == "/Enriching":
                kb_markup2 = ReplyKeyboardMarkup(
                    keyboard=[[KeyboardButton(text=x + ". " + y)] for x, y in sorted(domains.items())],
                    one_time_keyboard=True)

                bot.sendMessage(chat_id=chat_id,
                                text=text[1:] + u" chosen. Good, now choose a domain please.",
                                reply_markup=kb_markup2)
                Status[chat_id] = 2 if text == "/Querying" else 3

        # Quering Stuff
        elif (Status[chat_id] == 2 or Status[chat_id] == 3) and is_command(text):
            comm = text[:2]
            if comm in domains.keys():

                DomainChoice[chat_id] = comm
                if Status[chat_id] == 2:
                    response = u"OK. Ask me what you want!"
                    bot.sendMessage(chat_id=chat_id, text=response, reply_markup=None)
                    Status[chat_id] = 21
                else:
                    Status[chat_id] = 31
                    enriching_data = BotEnricher.ask_question(domains[DomainChoice[chat_id]])  # type: EnrichingData
                    bot.sendMessage(chat_id=chat_id,
                                    text=enriching_data.question)
                    chat2data[chat_id] = enriching_data
            else:
                response = u"Cannot understand command: " + comm
                bot.sendMessage(chat_id=chat_id, text=response, )

        # Enriching Stuff
        elif Status[chat_id] == 31:
            # retrieve the data from chat_id
            enriching_data = chat2data[chat_id]  # type: EnrichingData
            enriching_data.user_answer = text
            enriching_data = BotEnricher.consume_answer(enriching_data)  # type: EnrichingData
            if enriching_data.validated_object:
                response = u"Ah! Okay, I understood."
            else:
                response = u"Hum, you know? I didn't understand your answer!"

            if Verbosity.setdefault(chat_id, 0) > 0:
                response += u'\nRelation: ' + enriching_data.relation
                response += u'\nSubject: ' + enriching_data.subject
                response += u'\nSubject Synset: ' + enriching_data.subject_synset
                response += u'\nObject: ' + enriching_data.validated_object
                response += u'\nObject Synset: ' + enriching_data.validated_object_synset
                if enriching_data.yes_no:
                    response += u'\nYes No Maybe: ' + enriching_data.ynm_prediction

            bot.sendMessage(chat_id=chat_id, text=response, )
            enriching_data = BotEnricher.ask_question(domains[DomainChoice[chat_id]])  # type: EnrichingData
            bot.sendMessage(chat_id=chat_id,
                            text=enriching_data.question)
            chat2data[chat_id] = enriching_data

        elif Status[chat_id] == 21 and not is_command(text):
            domain = domains[DomainChoice[chat_id]]
            answer_data = BotAnswerer.respond(domain=domain, question=text)  # type: AnswerData
            answer = answer_data.answer  # type: unicode
            if Verbosity.setdefault(chat_id, 0) > 0:
                answer += u"\t" + answer_data.tag + u"\n"
                answer += u"\nRelation: " + answer_data.relation
                if answer_data.object_validated:
                    answer += u"\nValidated Object: " + unicode(answer_data.validated_object)
                else:
                    answer += u"\nObject: " + unicode(answer_data.object)

                if answer_data.subject_validated:
                    answer += u"\nValidated Subject: " + unicode(answer_data.validated_subject)
                else:
                    answer += u"\nSubject: " + unicode(answer_data.subject)

                if answer_data.tag == u'[MSE]':
                    answer += u"\nJaccard Score" + unicode(answer_data.answer_mse_score)

                if Verbosity[chat_id] > 2:
                    answer += u"\nSubject Synset: " + unicode(answer_data.subject_synset)
                    answer += u"\nObject Synset: " + unicode(answer_data.object_synset)

                if Verbosity[chat_id] > 3:
                    answer += u"\nSubject Indexes: " + unicode(answer_data.subject_indexes)
                    answer += u"\nObject Indexes: " + unicode(answer_data.object_indexes)

                if Verbosity[chat_id] == 5:
                    basis = answer_data.answer_basis
                    if len(basis) > 0:
                        answer += u"\nQA Basis: "
                        for index, base in enumerate(basis):  # type: KBDataWrapper
                            answer += "\t" + str(index) + "-\t" + unicode(base.question) + "\n"
                            answer += "\t" + str(index) + "-\t" + unicode(base.answer) + "\n\n"

            bot.sendMessage(chat_id=chat_id, text=answer, )

    except Exception, e:
        print e
        error_message = u"I'm sorry I don't feel very well... please start the procedure again"
        bot.sendMessage(chat_id=chat_id, text=error_message)


def is_command(text):
    """

    :param str | unicode text:
    :return:
    :rtype: bool
    """
    return text.startswith(u'/') and len(text) > 1


if __name__ == '__main__':
    logger.debug(u"*** Starting Boot phase")
    MessageLoop(bot, {'chat': handle}).run_as_thread()
    logger.debug(u"*** Boot phase ended")

    print u"BOT IS READY TO WORK"

    # start Knowledge Base Updater
    print "Creating Updater"
    kb_updater = KBUpdater()

    # start the Pattern Miner
    # print "Importing and launching Pattern Miner "
    # from ai.patterns_extraction import PatternMinerController  # runtime import
    # pattern_miner_controller = PatternMinerController(timer_interval=50)
    # print "Pattern Miner Controller Created"

    print "Starting KBU"
    kb_updater.start()
    # print "Starting PMC"
    # pattern_miner_controller.start()
    while True:
        sleep(10)
