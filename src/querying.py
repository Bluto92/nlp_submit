# Relation Classifiers / Question Type Classifier
from classifiers.relation_classifier import ClassifierLoader
# noinspection PyUnresolvedReferences
from sklearn.pipeline import Pipeline
# noinspection PyUnresolvedReferences
from classifiers.question_type_classifier import stemming_tokenizer

# Main Sense Interceptor
from ai.main_sense_interceptor import Relation2Stopwords, best_candidate, filter_sent, jaccard_similarity

# RNN
from rnn import subj_obj_rnn, glovembedder_runtime
import keras
import pickle
import numpy

# Generics
from utils import domains_utils, database, data_refactor, babelnet_client, sentence_manipulation, patterns_manager
from nltk.tokenize import WordPunctTokenizer
from utils.database import KBDataWrapper
import os
import regex
import spacy
import ujson
import requests
import random
import logging

logging.basicConfig(filename='../logs/tgrambot.log', level=logging.DEBUG,
                    format='%(asctime)s - %(name)s -  %(message)s')
logger = logging.getLogger(__name__)

# Constants
CLASSIFIERS_DIR = "../classifiers/chosen/"

YESNO_RNN_DIR = "../models/yesno/glove_emb/"
SORNN_DIR = "../models/sofinder/questions/all/"
SORNN_NAME = "subj_obj_finder_questions_all-00-a0.9846-l0.0822-va0.9924-vl0.0746-f1-Obj0.9718-f1-Subj0.9899.keras"


class Answerer(object):
    def __init__(self):
        self.relation2stopwords = Relation2Stopwords

        logger.debug("Loading Relation Classifiers")
        self.RelationClassifiers = {domain: ClassifierLoader(self.get_classifier_name(domain=domain)).load_classifier()
                                    for domain in domains_utils.CHOSEN_DOMAINS}

        logger.debug("Loading Question Type Classifier")
        self.QuestionTypeClassifier = ClassifierLoader('question_type_classifier_all.pickle').load_classifier()

        logger.debug("Loading Subj Obj RNN")
        self.SubjObjRNN = keras.models.load_model(SORNN_DIR + SORNN_NAME)
        self.SubjObjRNN.predict(numpy.zeros((1, 1, 300), dtype=numpy.float32))

        logger.debug("Loading Spacy")
        self.nlp = spacy.load('en_core_web_md')

        self.tokenize = WordPunctTokenizer().tokenize
        self.yes_no_rnns = {}  # loaded at runtime

    @staticmethod
    def get_classifier_name(domain):
        """

        :param str domain:
        :return:
        """
        for filename in os.listdir(CLASSIFIERS_DIR):
            if filename.startswith("relation_classifier_" + domain.lower().replace(' ', '_')):
                return filename
        return None

    def answer_full_query_recognition(self, answer_data):
        """

        :param AnswerData answer_data:
        :return:
        """

        results = database.get_by_question(relation=answer_data.relation, question=answer_data.question_original)
        if len(results) == 0:
            results = database.get_by_question(relation=answer_data.relation, question=answer_data.question_clean)

        if len(results) == 0:
            results = database.get_by_question(relation=answer_data.relation, question=answer_data.question_marked)

        if len(results) == 0:
            results = database.get_by_question(relation=answer_data.relation, question=answer_data.question_marked,
                                               lower=True)

        if len(results) > 0:
            answers = [KBDataWrapper(answer_data.relation, result) for result in results]
            answer_data.tag = "[FQR]"
            return self.choose_answer(answer_data, answers)

        return answer_data

    @staticmethod
    def disambiguate_sentence(text):
        babelfy_url = u'https://babelfy.io/v1/disambiguate'
        # params = 'text=' + text + '&lang=EN&key=KEY'  # + KEY
        params = u'text=' + text + u'&lang=EN&key=' + babelnet_client.KEY
        get_res = requests.get(unicode(babelfy_url), params=unicode(params))
        response_text = get_res.text
        if get_res.status_code == 200:
            decoded = ujson.decode(response_text)
            if u'message' in decoded:
                raise StopIteration(decoded['message'])
        else:
            raise Exception(get_res.text)

        return ujson.decode(response_text)

    def validate_sentence(self, sub_sentence, babelnet=True):
        """

        :param str | unicode sub_sentence:
        :param bool babelnet:
        :return: Returns True if Spacy Entities or Babelnet recognize the sub-sentence
        :rtype: bool
        """
        logger.debug("Validating sentence: " + sub_sentence)
        doc = self.nlp(sub_sentence)
        for entity in doc.ents:
            if entity.lower_ == sub_sentence.lower():
                return True

        if babelnet:
            disambiguated_sentence = Answerer.disambiguate_sentence(sub_sentence)
            logging.debug('[BABELNET CLIENT] sentence: ' + sub_sentence + ' len: ' + str(len(sub_sentence)))
            for token in disambiguated_sentence:
                token_start_index = token['charFragment']['start']
                token_stop_index = token['charFragment']['end']
                logging.debug(
                    '[BABELNET CLIENT] start: ' + str(token_start_index) + ' end: ' + str(token_stop_index))

                if token_start_index == 0 and token_stop_index == len(sub_sentence) - 1:
                    return True
            return False

        return False

    @staticmethod
    def answers_by_subj_obj(answer_data):
        """

        :param AnswerData answer_data:
        :return:
        """
        subj, obj = answer_data.subject, answer_data.object
        results = None
        logger.debug(
            u"Answer by SubjObj: Relation " + answer_data.relation + u" Subj: " + unicode(subj) + u" Obj: " + unicode(
                obj))
        if subj and obj:
            results = database.get_by_subj_obj(answer_data.relation, subj, obj, yes_no=True)
            if len(results) == 0:
                results = database.get_by_subj_obj(answer_data.relation, subj, obj, yes_no=False)
                if len(results) > 0:
                    return [u'yes']
        elif subj:
            results = database.get_by_subj(answer_data.relation, subj, yes_no=False)
        elif obj:
            results = database.get_by_obj(answer_data.relation, obj, yes_no=False)

        if results and len(results) > 0:
            answers = [KBDataWrapper(relation=answer_data.relation, kbdata=result) for result in results]
            logger.debug(u"Answer by SubjObj: Answers: " + unicode(answers))
            return answers

        logger.debug(u'No answers for SubjObj (' + unicode(subj) + u', ' + unicode(obj) + ')')
        return None

    def answer_pattern(self, answer_data, validation=True):
        """

        :param AnswerData answer_data:
        :param bool validation:
        :return:
        """
        patterns, pattern2regex = data_refactor.get_patterns_lowered(answer_data.relation)
        question = answer_data.question_marked_lower
        for pattern in patterns:
            pattern_regex = pattern2regex[pattern]
            if regex.match(pattern_regex, question):
                answer_data.subject, answer_data.object = data_refactor.extract_subj_obj_from_sentence(question,
                                                                                                       pattern,
                                                                                                       lower=False,
                                                                                                       cased=True)
                if not answer_data.subject and not answer_data.object:
                    continue

                logger.debug("Answer Pattern: found subj : " + unicode(answer_data.subject) + ", obj: " + unicode(
                    answer_data.object))

                if validation:
                    validation_result = self.check_and_strip(user_answer=answer_data)
                    if (answer_data.subject and not validation_result[0]) or (
                                answer_data.object and not validation_result[1]) or (
                                not answer_data.subject and not answer_data.object):
                        # if the extraction was unsuccessful check next pattern
                        continue
                    elif not (answer_data.subject or answer_data.object):
                        logger.debug("Answer Pattern: pattern validated: " + unicode(pattern))
                        logger.debug("Answer Pattern: validated subj : " + unicode(answer_data.subject))
                        logger.debug("Answer Pattern: validated obj: " + unicode(answer_data.object))

                        new_pattern = patterns_manager.build_pattern(sentence=question,
                                                                     subj=answer_data.subject,
                                                                     obj=answer_data.object)

                        pattern = new_pattern
                        logger.debug("Answer Pattern: Pattern built: " + new_pattern)

                        # enrich patterns pool
                        if new_pattern:
                            patterns_manager.add_pattern(pattern=new_pattern, relation=answer_data.relation)

                # store question pattern in process data
                answer_data.matched_pattern = pattern
                # look for an answer
                answers = self.answers_by_subj_obj(answer_data=answer_data)

                if answers:
                    return self.choose_answer(answer_data=answer_data, answers=answers)
                else:
                    break

        logger.debug("Answer Pattern: subj : " + unicode(answer_data.subject))
        logger.debug("Answer Pattern: obj: " + unicode(answer_data.object))
        return answer_data

    @staticmethod
    def may_be_disambiguated(answer_data):
        """
        Tells whether an analyzed question may be disambiguated.
        More precisely in the workflow it is useful only if we were able to extract the needed information.
        It is useful if it is a Yes/No question and we identified both Subject and Object, or if it is not then we need
        only one of them.

        :param AnswerData answer_data:
        :return:
        :rtype: bool
        """

        return (answer_data.yes_no and answer_data.subject_validated and answer_data.object_validated) or (
            not answer_data.yes_no and (answer_data.subject_validated or answer_data.object_validated))

    def disambiguate_question(self, answer_data):
        """

        :param AnswerData answer_data:
        :return:
        """
        question = answer_data.question_clean

        # disambiguate question
        # answer_data.disambiguated_question = self.disambiguate_sentence(question)

        if answer_data.subject_validated:
            subj = answer_data.validated_subject

            splits = regex.split('(.*?)' + subj.lower() + '(.*?)', question.lower())  # type: list
            while u'' in splits:
                splits.remove(u'')
            answer_data.subject_indexes['start'] = len(splits[0])
            answer_data.subject_indexes['end'] = len(splits[0]) + len(subj) - 1
            logger.debug(u"Subject Validated Start Index: " + unicode(answer_data.subject_indexes.get('start', -1)))
            logger.debug(u"Subject Validated End Index: " + unicode(answer_data.subject_indexes.get('end', -1)))

        if answer_data.object_validated:
            obj = answer_data.validated_object

            splits = regex.split('^(.*?) ' + obj.lower() + ' (.*?)$', question.lower())
            while u'' in splits:
                splits.remove(u'')
            answer_data.object_indexes['start'] = len(splits[0])
            answer_data.object_indexes['end'] = len(splits[0]) + len(obj) - 1

            logger.debug(u"Subject Validated Start Index: " + unicode(answer_data.object_indexes.get('start', -1)))
            logger.debug(u"Subject Validated End Index: " + unicode(answer_data.object_indexes.get('end', -1)))

        if answer_data.object_validated or answer_data.subject_validated:
            disambiguated_question = self.disambiguate_sentence(question)

            for token in disambiguated_question:
                token_start_index = token['charFragment']['start']
                token_stop_index = token['charFragment']['end']
                logging.debug(
                    '[BABELNET CLIENT] start: ' + str(token_start_index) + ' end: ' + str(token_stop_index))
                if answer_data.subject_validated and answer_data.subject_indexes['start'] == token_start_index and \
                        answer_data.subject_indexes['end'] == token_stop_index:
                    answer_data.subject_synset = token['babelSynsetID']

                if answer_data.object_validated and answer_data.object_indexes['start'] == token_start_index and \
                        answer_data.object_indexes['end'] == token_stop_index:
                    answer_data.object_synset = token['babelSynsetID']

        return answer_data

    def choose_answer(self, answer_data, answers):
        """

        :param AnswerData answer_data:
        :param List[KBDataWrapper] answers:
        :return:
        """
        # Here we ave to choose among multiple possible questions.
        # find validated_subject index
        if self.may_be_disambiguated(answer_data=answer_data):
            logger.debug("Choose Answer: it may be disambiguated!")
            answer_data = self.disambiguate_question(answer_data=answer_data)
            for answer in answers:
                if answer_data.validated_subject and answer_data.validated_object and \
                                answer_data.subject_synset == answer.subject_synset and \
                                answer_data.object_synset == answer.object_synset:
                    answer_data.answer = answer.answer
                    answer_data.answer_basis = [answer]
                    logger.debug("Choose Answer: answer that shares synset is present: " + answer_data.answer)
                    return answer_data

                elif (answer_data.validated_subject and answer_data.subject_synset == answer.subject_synset) or \
                        (answer_data.validated_object and answer_data.object_synset == answer.object_synset):
                    answer_data.answer = answer.answer
                    answer_data.answer_basis = [answer]
                    logger.debug("Choose Answer: answer that shares synset is present: " + answer_data.answer)
                    return answer_data
            logger.debug("Choose Answer: answer that shares synset is not present")

        if answers and len(answers) > 1:
            logger.debug("Choose Answer: choosing among various answers: calculating Jaccard Similarity")
            tokenizer = WordPunctTokenizer()
            uq_tokens = filter_sent(tokens=tokenizer.tokenize(answer_data.question_unmarked_lower),
                                    relation=answer_data.relation)
            jaccard2answer = {}
            for answer in answers:
                qa_tokens = filter_sent(tokens=tokenizer.tokenize(answer.question), relation=answer_data.relation)
                j_sim = jaccard_similarity(uq_tokens, qa_tokens)
                jaccard2answer[j_sim] = jaccard2answer.get(j_sim, []) + [answer]

            max_similarity = max(jaccard2answer.keys())
            most_probable_answers = jaccard2answer[max_similarity]
            if len(most_probable_answers) == 1:
                answer_data.answer = most_probable_answers[0].answer
                logger.debug("Choose Answer: most probable answer is " + answer_data.answer)
            else:  # choose at random among them
                answer_data.answer_basis = most_probable_answers
                random_answer_pos = random.randint(0, len(most_probable_answers) - 1)
                answer_data.answer = most_probable_answers[random_answer_pos].answer
                logger.debug("Choose Answer: (choose first) most probable answer is " + answer_data.answer)
        elif answers and len(answers) == 1:
            answer_data.answer = answers[0].answer

        logger.debug("Choose Answer: Returning: " + answer_data.answer)
        return answer_data

    @staticmethod
    def answer_main_sense(answer_data, yes_no=False):
        """
        :param AnswerData answer_data:
        :param bool yes_no:
        :return:
        """
        answer, score = best_candidate(user_question=answer_data.question_unmarked_lower,
                                       relation=answer_data.relation, yes_no=yes_no)
        if answer:
            answer_data.answer = answer
            answer_data.answer_mse_score = score
        return answer_data

    def answer_yes_no_rnn(self, relation, question):
        if relation not in self.yes_no_rnns.keys():
            rnn_dir = YESNO_RNN_DIR + relation.lower() + "/"
            model, w2i = None, None
            for filename in os.listdir(rnn_dir):
                if filename.endswith('.keras') and relation.lower() in filename:
                    model = keras.models.load_model(rnn_dir + filename)
                elif filename.endswith('w2i.pickle'):
                    w2i = pickle.load(open(rnn_dir + filename, mode='r'))

            self.yes_no_rnns[relation] = (model, w2i)

            if not model or not w2i:
                raise StandardError
        else:
            model, w2i = self.yes_no_rnns[relation]

        i2w = {index: word for word, index in w2i.items()}
        encoded_question = glovembedder_runtime.encode_sentence(question)
        answer_predicted = model.predict(encoded_question)

        return i2w[numpy.argmax(answer_predicted[0])]

    def subj_obj_prediction(self, relation, question, validate=True):
        subj_obj_prediction = self.SubjObjRNN.predict(glovembedder_runtime.encode_sentence(question))
        word_predictions = []
        for word_prediction in subj_obj_prediction[0]:
            word_predictions.append(subj_obj_rnn.INDEX2TAG[numpy.argmax(word_prediction)])

        objects, subjects = [], []
        obj, subj = u'', u''
        for word, prediction in zip(self.tokenize(question), word_predictions):
            logger.debug("[SOP] " + unicode(word) + " " + unicode(prediction))
            if prediction == subj_obj_rnn.SUBJ_TAG:
                subj += word + u' '
                logger.debug("[SOP] Detected Subject word: " + unicode(word) + " " + unicode(prediction))
            elif subj != u'':
                subjects.append(subj.rstrip())
                logger.debug("[SOP] Detected end of Subject: " + unicode(subj.rstrip()))
                subj = u''

            if prediction == subj_obj_rnn.OBJ_TAG:
                obj += word + u' '
                logger.debug("[SOP] Detected Object word: " + unicode(word) + " " + unicode(prediction))
            elif obj != u'':
                objects.append(obj.rstrip())
                logger.debug("[SOP] Detected end of Object: " + unicode(obj.rstrip()))
                obj = u''

        if subj != u'':
            subjects.append(subj)
        if obj != u'':
            objects.append(obj)

        logger.debug("[SOP] Subjects: " + unicode(subjects))
        logger.debug("[SOP] Objects: " + unicode(objects))

        # check invalid results
        if (len(subjects) > 1 or len(objects) > 1) or (len(subjects) == 0 and len(objects) == 0):
            logger.debug("[SOP] None or More than one Object/Subject found")

        subj = subjects[0] if len(subjects) == 1 else None
        obj = objects[0] if len(objects) == 1 else None

        # # validate sub/obj
        # if validate:
        #     subj, obj = None, None
        #     if len(subjects) == 1:
        #         subj = subjects[0]
        #         if not database.is_known(relation, subj) or not self.validate_sentence(subj):
        #             logger.debug("[SOP] Subject does not pass the validation checks")
        #             subj = None
        #
        #     if len(objects) == 1:
        #         obj = objects[0]
        #         if not database.is_known(relation, obj) or not self.validate_sentence(obj):
        #             logger.debug("[SOP] Object does not pass the validation checks")
        #             obj = None

        return subj, obj

    def subj_obj_prediction_answer(self, answer_data):
        subj_obj = self.subj_obj_prediction(question=answer_data.question_unmarked, relation=answer_data.relation)
        if subj_obj[0] or subj_obj[1]:
            subj, obj = subj_obj[0], subj_obj[1]
            logger.debug("Predicting Subj/Obj: found subj " + unicode(subj) + ", obj: " + unicode(obj))
            answer_data.subject, answer_data.object = subj, obj
            answers = self.answers_by_subj_obj(answer_data=answer_data)
            if answers:
                answer_data = self.choose_answer(answer_data=answer_data, answers=answers)
                logger.debug("RNN-SOX Answer: " + answer_data.answer)
                return answer_data

            else:
                logger.debug("RNN-SOX Answer FAILED, Trying checking and stripping")
                validation_result = self.check_and_strip(user_answer=answer_data)
                if (answer_data.subject or not validation_result[0]) or (
                            answer_data.object or not validation_result[1]):
                    answers = self.answers_by_subj_obj(answer_data=answer_data)
                    if answers:
                        answer_data = self.choose_answer(answer_data=answer_data, answers=answers)
                        logger.debug("RNN-SOX[CS] Answer: " + answer_data.answer)
                        return answer_data
        return answer_data

    def check_and_strip(self, user_answer, babelnet=True):
        """

        :param babelnet:
        :param AnswerData user_answer:
        :return:
        :rtype: Tuple[bool]
        """
        validation = (False, False)
        if user_answer.subject:
            logger.debug("CheckAndStrip: Checking subject: " + user_answer.subject)
            # check if the given subject may be a plausible subject
            user_answer.subject_known = database.is_known(user_answer.relation, user_answer.subject)
            user_answer.subject_validated = self.validate_sentence(user_answer.subject, babelnet=babelnet)

            # if not try to strip from start and end the stopwords
            if not (user_answer.subject_known or user_answer.subject_validated):
                user_answer.subject = sentence_manipulation.strip(user_answer.subject,
                                                                  Relation2Stopwords[user_answer.relation])

                logger.debug("CheckAndStrip: Stripped subject: " + user_answer.subject)

                # check again
                if user_answer.subject:
                    user_answer.subject_known = database.is_known(user_answer.relation, user_answer.subject)
                    user_answer.subject_validated = self.validate_sentence(user_answer.subject, babelnet=babelnet)

        if not (user_answer.subject_known or user_answer.subject_validated):
            user_answer.subject = None
            logger.debug("CheckAndStrip: Could not extract a valid subject")
        else:
            user_answer.validated_subject = user_answer.subject
            logger.debug("CheckAndStrip: Valid Subject: " + user_answer.subject)
            validation = (True, validation[1])

        if user_answer.object:
            # check if the given object may be a plausible object
            user_answer.object_known = database.is_known(user_answer.relation, user_answer.object)
            user_answer.object_validated = self.validate_sentence(user_answer.object, babelnet=babelnet)

            # if not try to strip from start and end the stopwords
            if not (user_answer.object_known or user_answer.object_validated):
                user_answer.object = sentence_manipulation.strip(user_answer.object,
                                                                 Relation2Stopwords[user_answer.relation])
                # check again
                if user_answer.object:
                    user_answer.object_known = database.is_known(user_answer.relation, user_answer.object)
                    user_answer.object_validated = self.validate_sentence(user_answer.object, babelnet=babelnet)

        if not (user_answer.object_known or user_answer.object_validated):
            user_answer.object = None
            logger.debug("CheckAndStrip: Could not extract a valid object")
        else:
            user_answer.validated_object = user_answer.object
            logger.debug("CheckAndStrip: Valid Object: " + user_answer.object)
            validation = (validation[0], True)

        return validation

    def respond(self, domain, question):
        """

        :param str | unicode domain:
        :param str | unicode question:
        :return:
        :rtype: AnswerData
        """
        # initialize session
        logger.debug('Question: ' + question)
        logger.debug('Domain: ' + domain)
        UserAnswer = AnswerData(domain, question)

        # predict relation
        relation_clf = self.RelationClassifiers[domain]
        relations = domains_utils.get_relations_for_domain_kbs(domain)
        prediction = relation_clf.predict([question])
        logger.debug('Prediction: ' + str(prediction))
        UserAnswer.relation = relations[prediction[0]]
        logger.debug('Predicted Relation: ' + UserAnswer.relation)

        # check if the question is in the database
        UserAnswer = self.answer_full_query_recognition(UserAnswer)
        if UserAnswer.answer:
            logger.debug(UserAnswer.answer + UserAnswer.tag)
            return UserAnswer
        logger.debug("Full Query Resolution [FAILED]")

        # check pattern
        """
        Pattern Recognition can match both YesNo and Complex questions, i.e. questions to which the answer must be a 
        'yes' or a 'no' (e.g. are lemons yellow?) and question to which the answer must be a definition 
        (e.g. what is the color of lemons?).   
        """
        UserAnswer = self.answer_pattern(UserAnswer)
        if UserAnswer.answer:
            print "found answer ", UserAnswer.answer
            logger.debug(UserAnswer.answer + "\t[PRN]")
            UserAnswer.tag = "[PRN]"
            return UserAnswer
        # print "[PRN] Answer not found"
        logger.debug("Looking for Pattern [FAILED]")
        logger.debug("Subj: " + unicode(UserAnswer.subject) + " Obj: " + unicode(UserAnswer.object))

        # Predict Question Type
        UserAnswer.yes_no = bool(self.QuestionTypeClassifier.predict([question])[0])
        if UserAnswer.yes_no:
            logger.debug("It is a Yes No Question")
        else:
            logger.debug("It is a Complex Question")

        # If the pattern extraction were not successful, store the question in the database of unmatched-queries
        if not UserAnswer.subject and not UserAnswer.object:
            logger.debug(
                "Storing Unmatched query: (" + UserAnswer.relation + ", " + question + ", " + unicode(
                    bool(UserAnswer.yes_no)) + ")")
            database.add_unmatched_query(predicted_relation=UserAnswer.relation, question=question,
                                         yes_no=bool(UserAnswer.yes_no))

        """
        If we were not able to extract any valid information about the subject or object in the question,
        we can try to guess them with the Subject-Object-RNN and then validate them.
        """
        if not UserAnswer.validated_subject and not UserAnswer.validated_object:
            UserAnswer = self.subj_obj_prediction_answer(answer_data=UserAnswer)
            if UserAnswer.answer:
                logger.debug("Predicting Subj/Obj: Answer: " + UserAnswer.answer)
                UserAnswer.tag = "[RNN-SOX-CS]"
                return UserAnswer
            else:
                logger.debug("Predicting Subj/Obj: [FAILED]")
            logger.debug("Subj: " + unicode(UserAnswer.subject) + " Obj: " + unicode(UserAnswer.object))

        """
        We do apply MainSense only to Complex Questions, not YesNo ones.
        This is because in the latter case the questions contains both subject and object and the main sense may be 
        very imprecise. Especially in the case where a question may also have sense swapping the Subject and Object 
        parts, in this case it would be not possible to distinguish the right answer with this approach
        """
        if UserAnswer.yes_no:
            # it is possible now to ``Guess'' using the trained YesNo RNN
            UserAnswer.answer = "I would guess " + self.answer_yes_no_rnn(UserAnswer.relation,
                                                                          UserAnswer.question_original)
            if UserAnswer.subject_validated and UserAnswer.object_validated:
                logger.debug('')
                database.add_no_answer_question(UserAnswer.relation, UserAnswer.question_original,
                                                UserAnswer.validated_subject, UserAnswer.validated_object,
                                                UserAnswer.subject_synset, UserAnswer.object_synset,
                                                UserAnswer.yes_no, UserAnswer.matched_pattern)
            UserAnswer.tag = "[RNN]"
            return UserAnswer
        else:
            """
            If the pattern extraction failed, we can try a Main Sense Approach
            but if the score of the answer (jaccard similarity) is low ( < .5 ) then it could be preferable to
            guess the answer using the Yes No RNN
            """
            # extract main sense
            if not UserAnswer.validated_subject and not UserAnswer.validated_object:
                UserAnswer = self.answer_main_sense(UserAnswer, yes_no=False)
                if UserAnswer.answer and UserAnswer.answer_mse_score > 0.5:
                    UserAnswer.tag = "[MSE]"
                    return UserAnswer

            """
            Send a Sorry message
            """
            logger.debug(
                u"Subj: " + unicode(UserAnswer.validated_subject) + u" Obj: " + unicode(UserAnswer.validated_object))

            base_answer = u"I'm Sorry, I do not know what to answer to your question about "
            if UserAnswer.validated_subject or UserAnswer.validated_object:
                UserAnswer = self.disambiguate_question(UserAnswer)
                if UserAnswer.validated_subject and UserAnswer.validated_object:
                    base_answer += UserAnswer.validated_subject + u" and " + UserAnswer.validated_object
                elif UserAnswer.validated_subject:
                    base_answer += UserAnswer.validated_subject
                elif UserAnswer.validated_object:
                    base_answer += UserAnswer.validated_object
                UserAnswer.answer = base_answer + u"."

                # add to database for enriching purpose
                database.add_no_answer_question(UserAnswer.relation, UserAnswer.question_original,
                                                UserAnswer.validated_subject, UserAnswer.validated_object,
                                                UserAnswer.subject_synset, UserAnswer.object_synset,
                                                UserAnswer.yes_no, UserAnswer.matched_pattern)
            else:
                UserAnswer.answer = u"I'm Sorry, I didn't catch the point."

            UserAnswer.tag = u"[SORRY]"
            return UserAnswer


class AnswerData(object):
    def __init__(self, domain, question):
        self.question_original = question.rstrip('\n ')
        self.question_clean = regex.sub(' +', ' ', self.question_original.lstrip().rstrip())
        self.question_marked = self.question_clean.rstrip('? ') + u' ?'
        self.question_unmarked = self.question_clean.rstrip('? ')
        self.question_clean_lower = self.question_clean.lower()
        self.question_marked_lower = self.question_marked.lower()
        self.question_unmarked_lower = self.question_unmarked.lower()
        self.disambiguated_question = None
        self.answer = None
        self.domain = domain
        self.tag = None
        self.relation = None
        self.object = None
        self.subject = None
        self.yes_no = None
        self.subject_synset = None
        self.object_synset = None
        self.subject_validated = False
        self.object_validated = False
        self.subject_known = False
        self.object_known = False
        self.validated_subject = None
        self.validated_object = None
        self.answer_mse_score = 0.
        self.subject_indexes = {}
        self.object_indexes = {}
        self.answer_basis = []  # the knowledge used to extract this answer -> Debug purposes
        self.matched_pattern = None


if __name__ == '__main__':
    ErrorProne = Answerer()
    Domain = 'Education'
    print 'Available Relations', unicode(domains_utils.get_relations_for_domain_kbs(Domain))

    while True:
        try:
            Question = unicode(raw_input('Ask Me something: '))
            Answer = (ErrorProne.respond(domain=Domain, question=Question))
            print Answer.answer, " -- ", Answer.tag
            print "Subject and synset", Answer.validated_subject, Answer.subject_synset
            print "Object and synset", Answer.validated_object, Answer.object_synset
            print "Subject Indexes ", Answer.subject_indexes
            print "Object Indexes ", Answer.object_indexes
            print "Disambiguated Question ", Answer.disambiguated_question

        except Exception, e:
            print e
