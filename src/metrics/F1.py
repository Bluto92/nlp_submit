import numpy


def f1_score(y_gold, y_pred, expected):
    results = {}
    tp, fp, fn = 0, 0, 0
    for gold_sentences, pred_sentences in zip(y_pred, y_gold):
        for gold_sentence, pred_sentence in zip(gold_sentences, pred_sentences):
            for true, pred in zip(gold_sentence, pred_sentence):
                pred = numpy.argmax(pred)
                true = numpy.argmax(true)
                if true == expected:
                    if pred == true:
                        tp += 1
                    else:
                        fn += 1
                elif pred == expected:
                    fp += 1

    precision, recall, f1 = .0, .0, .0
    if tp + fp != 0 and tp + fn != 0:
        precision = float(tp) / (tp + fp)
        recall = float(tp) / (tp + fn)
        f1 = 2 * (precision * recall) / (precision + recall)

    results['precision'] = precision
    results['recall'] = recall
    results['f1'] = f1

    return results

