class LexemeInterface:
    def __init__(self, word, vect, rank):
        """

        :param str word:
        :param numpy.array vect:
        :param int rank:
        """

        self.word = word
        self.orth_ = unicode(self.word)
        self.lower_ = unicode(self.word).lower()
        self.vector = vect
        self.rank = rank

    def __str__(self):
        return self.lower_
