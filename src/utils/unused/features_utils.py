from data_iterators import RelationsDataIterator
from LexemeInterface import LexemeInterface
from tqdm import tqdm
import numpy
import ujson
import glob
import domains_utils
import data_utils

MAX_QST = 17
MAX_RSP = 8

WORD_PAD = LexemeInterface('$PAD$', numpy.zeros(300, dtype=numpy.float), rank=0)
WORD_UNK = LexemeInterface('$UNK$', numpy.zeros(300, dtype=numpy.float), rank=1)
WORD_SSN = LexemeInterface('$SSN$', numpy.zeros(300, dtype=numpy.float), rank=2)
WORD_EOS = LexemeInterface('$EOS$', numpy.zeros(300, dtype=numpy.float), rank=3)

DATA_DIR = "../data/"
CHUNKS_DIR = DATA_DIR + "chunks/"
DATA_CNK_PREFIX = "data_chunk_"

""" DATA EXTRACTION """


def generator_question_answer_per_rel(relation, per_rel_dir=data_utils.DATA_REL_DIR,
                                      answer_min_length=1, question_min_length=1):
    for data_entry in data_utils.parse_data_per_relation(relation=relation, per_rel_dir=per_rel_dir,
                                                         question_min_length=question_min_length,
                                                         answer_min_length=answer_min_length):
        yield data_entry['question'].lower(), data_entry['answer'].lower()


def generator_question_answer(chunks_dir=CHUNKS_DIR):
    for filepath in glob.glob1(chunks_dir, DATA_CNK_PREFIX + "*.txt"):
        data_file = open(chunks_dir + filepath, 'r')
        data_list = ujson.load(data_file)
        for data_entry in data_list:  # type: dict
            yield data_entry['question'].lower(), data_entry['answer'].lower()


def generator_question_answer_docs(nlp, chunks_dir=CHUNKS_DIR):
    for question, answer in generator_question_answer(chunks_dir=chunks_dir):
        yield nlp(question), nlp(answer)


def generator_question_answer_tokenizer(tokenizer, relation, remove_punct=None, per_rel_dir=data_utils.DATA_REL_DIR,
                                        answer_min_length=1, question_min_length=1):
    """

    :param tokenizer:
    :param relation:
    :param remove_punct:
    :param per_rel_dir:
    :param answer_min_length:
    :param question_min_length:
    :return:
    """
    for question, answer in generator_question_answer_per_rel(relation=relation, per_rel_dir=per_rel_dir,
                                                              question_min_length=question_min_length,
                                                              answer_min_length=answer_min_length):
        question = question.strip(remove_punct)
        answer = answer.strip(remove_punct)
        tokenized_question = tokenizer.tokenize(question)
        tokenized_answer = tokenizer.tokenize(answer)

        yield tokenized_question, tokenized_answer


def get_index2word_unicode(vocab):
    index2word = {WORD_PAD.rank: WORD_PAD.lower_, WORD_UNK.rank: WORD_UNK.lower_,
                  WORD_EOS.rank: WORD_EOS.lower_, WORD_SSN.rank: WORD_SSN.lower_}

    base = len(index2word.keys())

    from tqdm import tqdm
    vocab = set(vocab)
    with tqdm(total=len(vocab)) as tqdm:
        for count, word in enumerate(vocab):
            index2word[base + count] = word
            tqdm.update(1)

    # print "I2W built:\n\tmaxID: ", max(index2word.keys()), "\n\tw2i entries: ", str(len(index2word.keys()))
    return index2word


def get_word2index_unicode(vocab):
    word2index = {WORD_PAD.lower_: WORD_PAD.rank,
                  WORD_UNK.lower_: WORD_UNK.rank,
                  WORD_EOS.lower_: WORD_EOS.rank,
                  WORD_SSN.lower_: WORD_SSN.rank}

    base = len(word2index.keys())

    from tqdm import tqdm
    vocab = set(vocab)
    with tqdm(total=len(vocab)) as tqdm:
        for count, word in enumerate(vocab):
            word2index[word] = base + count
            tqdm.update(1)

    # print "W2I built:\n\tmaxID: ", max(word2index.values()), "\n\tw2i entries: ", str(len(word2index.keys()))
    return word2index


def get_word2index_spacy(vocab):
    word2index = {WORD_PAD.lower_: WORD_PAD.rank,
                  WORD_UNK.lower_: WORD_UNK.rank,
                  WORD_EOS.lower_: WORD_EOS.rank,
                  WORD_SSN.lower_: WORD_SSN.rank}
    counter = 4
    from tqdm import tqdm

    with tqdm(total=len(vocab)) as tqdm:
        for lex in vocab:
            if lex.has_vector and lex.lower_ not in word2index.keys():
                word2index[lex.lower_] = counter
                counter += 1
                tqdm.update(1)
    return word2index


def get_index2word_from_w2i(w2i):
    return {index: word for word, index in w2i.items()}


def get_word2index_from_i2w(i2w):
    return {word: index for index, word in i2w.items()}


def get_tokenized_sentence_spacy(nlp, sentence):
    return nlp(sentence)


def get_features_x(x, w2i, max_length_q=MAX_QST, rotate=True, ssn=True, eos=True):
    max_length_q = len(x)
    features_q = numpy.zeros((1, max_length_q,), dtype=numpy.int32)
    increment = 0
    if ssn:
        features_q[0, 0] = w2i[WORD_SSN.lower_]
        increment = 1

    for j, token in enumerate(x[:max_length_q - 1]):
        if token in w2i.keys():
            features_q[0, j + increment] = w2i[token.lower()]
        else:
            features_q[0, j + increment] = WORD_UNK.rank

    if eos:
        if len(x) >= max_length_q - 1:
            features_q[0, max_length_q - 1] = w2i[WORD_EOS.lower_]
        else:
            features_q[0, len(x) + increment] = w2i[WORD_EOS.lower_]

    try:
        # CHECKS
        start = False
        end = False
        for ind in features_q[0]:
            if ind == WORD_SSN.rank:
                start = True
            if ind == WORD_EOS.rank:
                end = True

                assert ((not ssn or start) and not (not eos or end)) or (
                    (not ssn or start) and (not eos or end))
                if start and end:
                    break

        assert ((not ssn or start) and (not eos or end))

    except AssertionError:
        print "Error"
        pass

    if not rotate:
        return features_q

    """ ROTATE """
    features_qr = numpy.zeros(features_q.shape, dtype=numpy.int32)
    for i, v in enumerate(features_q[0]):
        features_qr[0, features_q.shape[1] - i - 1] = v

    return features_qr


def parse_features_spacy(x, w2i, max_length_q=MAX_QST, rotate=True, ssn=True, eos=True):
    parsed = []
    for j, token in enumerate(x):
        if token.lower_ in w2i.keys() and token.has_vector:
            parsed.append(token.lower_)

    return get_features_x(parsed, w2i, max_length_q=max_length_q, rotate=rotate, ssn=ssn, eos=eos)


def get_features_y_spacy(y, w2i, max_length_a=MAX_RSP, ssn=True, eos=True):
    """

    :param y:
    :param w2i:
    :param max_length_a:
    :param ssn:
    :param eos:
    :return:
    """

    """ ANSWERS """
    features_a = numpy.zeros((1, max_length_a, len(w2i)), dtype=numpy.float32)
    increment = 0
    if ssn:
        features_a[0, 0, w2i[WORD_SSN.lower_]] = 1.
        increment = 1

    for j, token in enumerate(y[:max_length_a - 1]):
        if token.lower() in w2i.keys():
            features_a[0, j + increment, w2i[token.lower()]] = 1
        else:
            features_a[0, j + increment, WORD_UNK.rank] = 1

    if eos:
        if len(y) >= max_length_a - 1:
            features_a[0, max_length_a - 1] = numpy.zeros(len(w2i), dtype=numpy.float32)
            features_a[0, max_length_a - 1, w2i[WORD_EOS.lower_]] = 1
        else:
            features_a[0, len(y) + increment, w2i[WORD_EOS.lower_]] = 1
    try:
        # CHECKS
        start = False
        end = False
        for ind in features_a[0]:
            if numpy.argmax(ind) == WORD_SSN.rank:
                start = True
            if numpy.argmax(ind) == WORD_EOS.rank:
                end = True

            assert ((not ssn or start) and not (not eos or end)) or (
                (not ssn or start) and (not eos or end))
            if start and end:
                break

        assert ((not ssn or start) and (not eos or end))

    except AssertionError:
        trf = [numpy.argmax(x) for x in features_a[0]]
        print "Error: ", trf
        pass

    return features_a


def get_features_y(y, w2i, max_length_a=MAX_RSP, ssn=True, eos=True):
    """

    :param y:
    :param w2i:
    :param max_length_a:
    :param ssn:
    :param eos:
    :return:
    """

    """ ANSWERS """
    features_a = numpy.zeros((1, max_length_a, len(w2i)), dtype=numpy.float32)
    increment = 0
    if ssn:
        features_a[0, 0, w2i[WORD_SSN.lower_]] = 1.
        increment = 1

    for j, token in enumerate(y[:max_length_a - 1]):
        if token.lower() in w2i.keys():
            token_pos = w2i[token.lower()]
            features_a[0, j + increment, token_pos] = 1
        else:
            features_a[0, j + increment, WORD_UNK.rank] = 1

    if eos:
        if len(y) >= max_length_a - 1:
            features_a[0, max_length_a - 1] = numpy.zeros(len(w2i), dtype=numpy.float32)
            features_a[0, max_length_a - 1, w2i[WORD_EOS.lower_]] = 1
        else:
            features_a[0, len(y) + increment, w2i[WORD_EOS.lower_]] = 1
    try:
        # CHECKS
        start = False
        end = False
        for ind in features_a[0]:
            if numpy.argmax(ind) == WORD_SSN.rank:
                start = True
            if numpy.argmax(ind) == WORD_EOS.rank:
                end = True

            assert ((not ssn or start) and not (not eos or end)) or (
                (not ssn or start) and (not eos or end))
            if start and end:
                break

        assert ((not ssn or start) and (not eos or end))

    except AssertionError:
        trf = [numpy.argmax(x) for x in features_a[0]]
        print "Error: ", trf
        pass

    return features_a


def get_feature_xy(x, y, w2iq, w2ia, max_length_q=MAX_QST, max_length_a=MAX_RSP, rotate=True, ssn=True, eos=True):
    """

    :param w2iq:
    :param x:
    :param y:
    :param max_length_q:
    :param max_length_a:
    :param rotate:
    :param ssn:
    :param eos:
    :return:
    """

    # i2wq = get_index2word_from_w2i(w2i=w2iq)
    # i2wa = get_index2word_from_w2i(w2i=w2ia)

    # QUESTIONS
    features_q = get_features_x(x=x, w2i=w2iq, max_length_q=max_length_q, rotate=rotate, ssn=ssn, eos=eos)
    # qrf = [i2wq[qq] for qq in features_q[0] if qq in i2wq]

    # ANSWERS
    features_a = get_features_y(y=y, w2i=w2ia, max_length_a=max_length_a, ssn=ssn, eos=True)
    # trf = [numpy.argmax(x) for x in features_a[0]]
    # trfw = [i2wa[x] for x in trf]

    return features_q, features_a


def get_feature_xy_spacy(x, y, w2i, max_length_q=MAX_QST, max_length_a=MAX_RSP, rotate=True, ssn=True,
                         eos=True):
    """

    :param w2i:
    :param x:
    :param y:
    :param max_length_q:
    :param max_length_a:
    :param rotate:
    :param ssn:
    :param eos:
    :return:
    """

    # QUESTIONS
    features_q = parse_features_spacy(x=x, w2i=w2i, max_length_q=max_length_q, rotate=rotate, ssn=ssn, eos=eos)

    # ANSWERS
    features_a = get_features_y_spacy(y=y, w2i=w2i, max_length_a=max_length_a, ssn=ssn, eos=eos)

    return features_q, features_a


def get_features_generator_spacy(nlp, w2i, max_length_q=MAX_QST, max_length_a=MAX_RSP, train=True, test_size=0.25,
                                 chunks_dir=CHUNKS_DIR, relations=domains_utils.get_all_relations(),
                                 eos=True, rotate=True, ssn=True):
    for relation in relations:
        iterator = RelationsDataIterator(relations=[relation], chunk_dir=chunks_dir)
        train_size = numpy.ceil(iterator.data_size() * (1 - test_size))
        for counter, (doc_q, doc_a) in enumerate(
                generator_question_answer_docs(nlp=nlp, chunks_dir=chunks_dir)):
            if counter < train_size:
                if train:
                    yield get_feature_xy_spacy(doc_q, doc_a, w2i=w2i, max_length_q=max_length_q,
                                               max_length_a=max_length_a,
                                               eos=eos, rotate=rotate, ssn=ssn)

            elif not train:
                yield get_feature_xy_spacy(doc_q, doc_a, w2i=w2i, max_length_q=max_length_q,
                                           max_length_a=max_length_a,
                                           eos=eos, rotate=rotate, ssn=ssn)


def get_features_list_tokenizer(tokenizer, w2i, max_length_q=MAX_QST, max_length_a=MAX_RSP, train=True,
                                answer_min_length=0, question_min_length=0, test_size=0.25,
                                relations=domains_utils.get_all_relations(), remove_punct=None,
                                eos=True, rotate=True, ssn=True):
    per_rel_dir = "/home/fra/Projects/Universita/NLP/FinalProject/data/per_relation_refactored/"
    for relation in relations:
        train_size = numpy.ceil(data_utils.get_relation_size(relation=relation, answer_min_length=answer_min_length,
                                                             question_min_length=question_min_length,
                                                             per_rel_dir=per_rel_dir) * (1 - test_size))

        features_x = []
        features_y = []
        description = relation[0].upper() + relation[1:].lower() + ": extracting data"
        with tqdm(total=train_size, desc=description) as td:
            for counter, (x, y) in enumerate(generator_question_answer_tokenizer(
                    tokenizer=tokenizer, remove_punct=remove_punct, relation=relation,
                    question_min_length=question_min_length, answer_min_length=answer_min_length,
                    per_rel_dir=per_rel_dir)):

                td.update(1)
                if (counter <= train_size and train) or (counter > train_size and not train):
                    x, y = get_feature_xy(x=x, y=y, w2iq=w2i, max_length_q=max_length_q, max_length_a=max_length_a,
                                          eos=eos, rotate=rotate, ssn=ssn)
                    features_x.append(x)
                    features_y.append(y)

        return features_x, features_y


def get_features_generator_tokenizer(tokenizer, w2iq, w2ia, max_length_q=MAX_QST, max_length_a=MAX_RSP, train=True,
                                     answer_min_length=0, question_min_length=0, test_size=0.25,
                                     relations=domains_utils.get_all_relations(), remove_punct=None,
                                     eos=True, rotate=True, ssn=True):
    per_rel_dir = "/home/fra/Projects/Universita/NLP/FinalProject/data/per_relation_refactored/"
    for relation in relations:
        train_size = numpy.ceil(data_utils.get_relation_size(relation=relation, answer_min_length=answer_min_length,
                                                             question_min_length=question_min_length,
                                                             per_rel_dir=per_rel_dir) * (1 - test_size))

        for counter, (x, y) in enumerate(generator_question_answer_tokenizer(
                tokenizer=tokenizer, remove_punct=remove_punct, relation=relation,
                question_min_length=question_min_length, answer_min_length=answer_min_length,
                per_rel_dir=per_rel_dir)):

            if counter < train_size:
                if train:
                    yield get_feature_xy(x=x, y=y, w2iq=w2iq, w2ia=w2ia, max_length_q=max_length_q, max_length_a=max_length_a,
                                         eos=eos, rotate=rotate, ssn=ssn)

            elif not train:
                yield get_feature_xy(x=x, y=y, w2iq=w2iq, w2ia=w2ia, max_length_q=max_length_q, max_length_a=max_length_a,
                                     eos=eos, rotate=rotate, ssn=ssn)


def get_features_size(train=True, test_size=0.25, relations=domains_utils.get_all_relations(),
                      answer_min_length=0, question_min_length=0, per_rel_dir=data_utils.DATA_REL_DIR):
    data_size = 0
    for relation in relations:
        data_size += data_utils.get_relation_size(relation=relation, answer_min_length=answer_min_length,
                                                  question_min_length=question_min_length, per_rel_dir=per_rel_dir)
    if train:
        return numpy.ceil(data_size * (1 - test_size))
    else:
        return numpy.floor(data_size * test_size)


def test_features_xy(w2i, i2w, max_length_q=500, max_length_a=500, rotate=True, ssn=True, eos=True, limit=20,
                     remove_punct=None):
    from nltk.tokenize import WordPunctTokenizer
    tokenizer = WordPunctTokenizer()

    for counter, (x, y) in enumerate(
            generator_question_answer_tokenizer(tokenizer=tokenizer, remove_punct=remove_punct)):
        if counter > limit:
            break

        feats_x = get_features_x(x=x, w2i=w2i, max_length_q=max_length_q, rotate=False, ssn=ssn, eos=eos)
        x_pos = 0
        for feat_x in feats_x[0]:
            if feat_x != WORD_EOS.rank and feat_x != WORD_PAD.rank and feat_x != WORD_UNK.rank and feat_x != WORD_SSN.rank:
                x_w = x[x_pos]
                x_pos += 1
                if i2w[feat_x] != x_w:
                    print "i2w[feat_x]: ", i2w[feat_x]
                    print "x_w: ", x_w
                assert i2w[feat_x] == x_w

        feats_y = get_features_y(y=y, w2i=w2i, max_length_a=max_length_a, ssn=ssn, eos=eos)
        y_pos = 0
        for feat_y in feats_y[0]:
            y_rank = numpy.argmax(feat_y)
            if y_rank != WORD_EOS.rank and y_rank != WORD_PAD.rank \
                    and y_rank != WORD_UNK.rank and y_rank != WORD_SSN.rank:
                y_w = y[y_pos]
                y_pos += 1
                if i2w[y_rank] != y_w:
                    print "y_rank: ", y_rank
                    print "i2w[y_rank]: ", i2w[y_rank]
                    print "y_w: ", y_w
                    for index, word in i2w.items():
                        if word == y_w:
                            print "Index2Word index for ", word, " is ", str(index), ". Given: ", str(y_rank)
                assert i2w[y_rank] == y_w

        pass

    print "Test Passed"


if __name__ == '__main__':
    Additional_words = [u'$PAD$'.lower(), u'$UNK$'.lower(), u'$SSN$'.lower(), u'$EOS$'.lower()]
    import data_utils

    Vocabulary = data_utils.extract_vocabulary_from_data()

    word2index = get_word2index_unicode(vocab=Vocabulary)
    index2word = {index: word for word, index in word2index.items()}

    for word, index in word2index.items():
        if word != index2word[index]:
            print "Error w2i-i2w"
            print "w2i: ", word, " -> ", index
            print "i2w: ", index, " -> ", index2word[index]
            for ii, ww in index2word.items():
                if ww == word:
                    print "i2w: ", ii, " -> ", ww

        assert word == index2word[index]
    print "Index2Word from Word2Index correctly created"
    print

    test_features_xy(w2i=word2index, i2w=index2word)
