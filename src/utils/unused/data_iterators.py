import glob
import ujson
import pprint as pp


class DataIterator:
    DATA_DIR = "../data/"
    CHUNKS_DIR = DATA_DIR + "chunks/"

    def __init__(self, chunks_dir=CHUNKS_DIR):
        self.chunks_dir = chunks_dir
        pass

    def __iter__(self):
        for file_path in glob.glob1(self.chunks_dir, "data_chunk_*.txt"):
            data_file = open(self.chunks_dir + file_path, 'r')
            data_list = ujson.load(data_file)
            for data_entry in data_list:  # type: dict
                for key, value in data_entry.items():
                    if type(value) == str or type(value) == unicode:
                        data_entry[key] = value.lower()
                yield data_entry

    def data_size(self):
        data_size = 0
        for file_path in glob.glob1(self.chunks_dir, "data_chunk_*.txt"):
            data_file = open(self.chunks_dir + file_path, 'r')
            data_list = ujson.load(data_file)
            data_size += len(data_list)
        return data_size


class RelationsDataIterator(DataIterator):

    def __init__(self, relations, chunk_dir=DataIterator.CHUNKS_DIR):
        """

        :param list[str] relations:
        :param str chunk_dir:
        """
        self.relations = relations
        DataIterator.__init__(self, chunks_dir=chunk_dir)

    def __iter__(self):
        for file_path in glob.glob1(self.chunks_dir, "data_chunk_*.txt"):
            data_file = open(self.chunks_dir + file_path, 'r')
            data_list = ujson.load(data_file)
            for data_entry in data_list:  # type: dict
                if data_entry['relation'] in self.relations:
                    for key, value in data_entry.items():
                        if type(value) == str or type(value) == unicode:
                            data_entry[key] = value.lower()

                    yield data_entry

    def data_size(self):
        data_size = 0
        for file_path in glob.glob1(self.chunks_dir, "data_chunk_*.txt"):
            data_file = open(self.chunks_dir + file_path, 'r')
            data_list = ujson.load(data_file)
            for data_entry in data_list:  # type: dict
                if data_entry['relation'] in self.relations:
                    data_size += 1
        return data_size


if __name__ == "__main__":
    iterator = RelationsDataIterator(['COLOR'])

    print "Data Size: ", str(iterator.data_size)
    print
    counter = 0
    for de in iterator:

        if counter >= 5:
            break
        counter += 1
        pp.pprint(de, depth=1)
        print

    print "END"
