import babelnet_client
import data_utils
import domains_utils
from tqdm import tqdm
import os
import ujson
from nltk.tokenize import WordPunctTokenizer

# nlp = spacy.load('en')
SUBJ_PLACEHOLDER = u'$X$'
OBJ_PLACEHOLDER = u'$Y$'

DATA_DIR = "../data/"
PATTERNS_DIR = DATA_DIR + "patterns/"
PATTERNS_RFCTR_DIR = PATTERNS_DIR + "refactoring"
DATA_REFACTORED_DIR = DATA_DIR + "per_relation_refactored/"


def subj_obj_replace(sentence, subj, obj):
    sentence_clean = u''
    sentence_docs = WordPunctTokenizer().tokenize(sentence)
    for sentence_token in sentence_docs:
        if sentence_token.lower() == subj.lower():
            sentence_clean += SUBJ_PLACEHOLDER + u" "
        elif sentence_token.lower() == obj.lower():
            sentence_clean += OBJ_PLACEHOLDER + u" "
        else:
            sentence_clean += sentence_token.lower() + u" "

    return sentence_clean.rstrip(' ')


def save_patterns_for_relations(relations=domains_utils.get_all_relations_kbs(), patterns_dir=PATTERNS_DIR):
    with tqdm(total=len(relations)) as td:
        for relation in relations:
            td.desc = relation
            td.refresh()

            import codecs
            pattern_file = codecs.open(patterns_dir + relation.lower() + ".tsv", mode='w+', encoding='utf8')
            patterns = [q + u"\t" + a + u"\n" for q, a in sorted(pattern_extraction(relation=relation))]
            pattern_file.writelines(patterns)
            pattern_file.close()

            td.update(1)


def pattern_extraction(relation, max_tries=False):
    rel_data = data_utils.parse_data_per_relation(relation=relation)
    qa_patterns = {}

    for data in rel_data:
        question = data['question']  # type: str
        answer = data['answer']  # type: str

        subj = data['c1']  # type: str
        obj = data['c2']  # type: str

        qa_pattern = pattern_identification(question, answer, subj, obj, max_tries=max_tries)
        if qa_pattern is not None:
            qa_patterns[qa_pattern] = qa_patterns.get(qa_pattern, 0) + 1

    return qa_patterns


def get_subj_obj_clean(subj_obj):
    subj_obj_splits = subj_obj.split('::')
    if len(subj_obj_splits) == 2:
        subj_obj = subj_obj_splits[0]  # type: str
    elif len(subj_obj_splits) > 2:
        return None
    elif babelnet_client.is_valid_synset(subj_obj):
        subj_obj = babelnet_client.retrieve_lemma_for_synset(synsetId=subj_obj, max_tries=max_tries)
        if subj_obj is None:
            return None
    else:
        return None

    return subj_obj


def pattern_identification(question, answer, subj, obj, max_tries=False):
    subj = get_subj_obj_clean(subj)
    obj = get_subj_obj_clean(obj)

    if subj is None or obj is None:
        return None

    question_clean = subj_obj_replace(question, subj, obj)
    answer_clean = subj_obj_replace(answer, subj, obj)

    skip = False
    for word in question_clean.split(" ") + answer_clean.split(" "):
        if word == u'"':
            skip = True
            break
    if not skip:
        obj_question = 0
        subj_question = 0

        for word in question_clean.split(" "):
            if word == SUBJ_PLACEHOLDER:
                subj_question += 1
            elif word == OBJ_PLACEHOLDER:
                obj_question += 1

        obj_answer = 0
        subj_answer = 0

        for word in answer_clean.split(" "):
            if word == SUBJ_PLACEHOLDER:
                subj_answer += 1
            elif word == OBJ_PLACEHOLDER:
                obj_answer += 1

        # skip wrongly formatted questions
        if (0 <= subj_question <= 1 and 0 <= subj_answer <= 1 and 0 <= obj_question <= 1 and
           0 <= obj_answer <= 1 <= subj_question + subj_answer <= 2 and 1 <= obj_question + obj_answer <= 2):
            return question_clean, answer_clean


def get_refactor_patterns(relation):
    patterns_filename = PATTERNS_RFCTR_DIR + "/" + relation.lower() + ".tsv"
    rel_patterns_refactor_file = open(patterns_filename, mode='r')

    original_refactors = {}
    original_question, original_answer = None, None
    mutex = True
    for line in rel_patterns_refactor_file:
        if "\t" not in line:
            continue
        if mutex:
            original = line.strip("\n")
            original_answer = unicode(original.split("\t")[1]).rstrip(' ')
            original_question = unicode(original.split("\t")[0]).rstrip(' ')
            mutex = False
        else:
            refactor = line.strip("\n")
            refactor_answer = unicode(refactor.split("\t")[1]).rstrip(' ')
            refactor_question = unicode(refactor.split("\t")[0]).rstrip(' ')
            original_refactors[(original_question, original_answer)] = (refactor_question, refactor_answer)
            mutex = True

    return original_refactors


def data_refactor(relations=domains_utils.get_all_relations_kbs(), refactored_data_dir=DATA_REFACTORED_DIR,
                  max_tries=False):
    if not os.path.exists(refactored_data_dir):
        os.mkdir(refactored_data_dir)
    counter = 0
    from tqdm import tqdm
    for relation in relations:
        o_r = get_refactor_patterns(relation)
        refactored_data_entries = []
        with tqdm(total=data_utils.get_relation_size(relation=relation)) as td:
            for data_entry in data_utils.parse_data_per_relation(relation):
                td.update(1)
                refactored = False
                question = data_entry['question']
                answer = data_entry['answer']

                # check non-emptiness
                if not question or not answer:
                    counter += 1
                    continue

                subj = data_entry['c1']  # type:str
                obj = data_entry['c2']  # type:str

                qa_pattern = pattern_identification(question, answer, subj, obj, max_tries=max_tries)

                if qa_pattern is not None and qa_pattern in o_r.keys():
                    subj = get_subj_obj_clean(subj)
                    obj = get_subj_obj_clean(obj)

                    refactor_pattern = o_r[qa_pattern]

                    ref_pat_question = refactor_pattern[0]
                    ref_pat_answer = refactor_pattern[1]

                    refactored_question = u""
                    for word in ref_pat_question.split(" "):
                        if word == SUBJ_PLACEHOLDER:
                            refactored_question += subj + u" "
                        elif word == OBJ_PLACEHOLDER:
                            refactored_question += obj + u" "
                        else:
                            refactored_question += word + u" "

                    refactored_question.rstrip(u" ")

                    refactored_answer = u""
                    for word in ref_pat_answer.split(" "):
                        if word == SUBJ_PLACEHOLDER:
                            refactored_answer += subj + u" "
                        elif word == OBJ_PLACEHOLDER:
                            refactored_answer += obj + u" "
                        else:
                            refactored_answer += word + u" "

                    refactored_answer.rstrip(u' ')

                    data_entry['question'] = refactored_question
                    data_entry['answer'] = refactored_answer

                    refactored = True

                answer = data_entry['answer'].rstrip(' ')

                # add . at the end of answer
                if not answer.endswith(u'.'):
                    answer += u' .'
                    data_entry['answer'] = answer
                    refactored = True

                refactored_data_entries.append(data_entry)

                if refactored:
                    counter += 1

        per_rel_file = open(DATA_REFACTORED_DIR + relation.lower() + ".txt", mode='w+')
        ujson.dump(refactored_data_entries, per_rel_file, ensure_ascii=False)
        per_rel_file.close()

        print "Refactored: ", str(counter), " data entries!"

    pass


# def add_answer_punct_end(relation, refactored_data_dir=DATA_REFACTORED_DIR):
#     refactored_data_entries = []
#     tokenizer = WordPunctTokenizer()
#     with tqdm(total=data_utils.get_relation_size(relation)) as td:
#         for data_entry in data_utils.parse_data_per_relation(relation, refactored_data_dir):
#             answer = data_entry['answer']  # type: str
#             tokens = tokenizer.tokenize(answer)
#             if not tokens or tokens[-1] != u'.':
#                 answer += u' .'
#
#             refactored_data_entries.append(data_entry)
#             td.update(1)
#
#     ujson.dump(refactored_data_entries, open(refactored_data_dir + '/' + relation.lower() + '.txt', mode='w+'),
#                ensure_ascii=False)


if __name__ == "__main__":
    relation = u'GENERALIZATION'
    # relation = u'ACTIVITY'

    # tokenizer = WordPunctTokenizer()
    # rel_data = data_utils.parse_data_per_relation(relation=u'COLOR')
    # for data in rel_data:
    #     question = data['question']
    #     answer = data['answer']
    #     subj = data['c1'].split('::')[0]
    #     obj = data['c2'].split('::')[0]
    #
    #     pattern_identification(question, answer, subj, obj)

    # for QuestionPattern, AnswerPattern in pattern_extraction(u'SPECIALIZATION'):
    #     print QuestionPattern, "\t", AnswerPattern

    # save_patterns_for_relations(relations=[u'GENERALIZATION'])
    # max_tries = False
    max_tries = True

    # import unicodecsv
    # from tqdm import tqdm
    #
    # writer = unicodecsv.writer(open("../data/gen.tsv", mode='w+'), encoding="utf-8", dialect='excel-tab')
    #
    # o_r = get_refactor_patterns(u'GENERALIZATION')
    # with tqdm(total=data_utils.get_relation_size(u'GENERALIZATION')) as td:
    #
    #     for data_entry in data_utils.parse_data_per_relation(relation=u'GENERALIZATION'):
    #         question = data_entry['question']
    #         answer = data_entry['answer']
    #
    #         subj = data_entry['c1']  # type:str
    #         obj = data_entry['c2']  # type:str
    #         try:
    #             qa_pattern = pattern_identification(question, answer, subj, obj, max_tries=max_tries)
    #             if qa_pattern is None or qa_pattern not in o_r.keys():
    #                 writer.writerow([question, answer, subj, obj])
    #         except StopIteration:
    #             max_tries = True
    #
    #         td.update(1)
    #
    import pprint

    #
    # patext = pattern_extraction(u'GENERALIZATION', max_tries=max_tries)
    # pprint.pprint(patext)
    # print
    # print "TOTAL: ", sum(patext.values())
    # print
    # print

    # save_patterns_for_relations()
    data_refactor([relation], max_tries=max_tries)

    print
    patterns = {pattern: 0 for pattern in get_refactor_patterns(relation=relation).keys()}
    with tqdm() as td:
        for num, data_entry in enumerate(data_utils.parse_data_per_relation(relation=relation)):

            pattern = pattern_identification(data_entry['question'].lower(), data_entry['answer'].lower(),
                                             data_entry['c1'].lower(), data_entry['c2'].lower(), max_tries=max_tries)
            if pattern in patterns.keys():
                patterns[pattern] = patterns[pattern] + 1
            td.update(1)

    pprint.pprint(patterns)
