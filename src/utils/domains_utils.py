import csv
import os
import codecs
from nltk.tokenize import WordPunctTokenizer

DATA_DIR = "../data/"
DOMAINS_DIR = DATA_DIR + "chatbot_maps/"
D2R_FILE = DOMAINS_DIR + "domains_to_relations.tsv"
PATTERNS_DIR = DATA_DIR + "patterns/"
PATTERNS_FILEPATH = PATTERNS_DIR + "patterns.tsv"

RBN2RKBS = {u'activity': u'ACTIVITY', u'colorPattern': u'COLOR', u'generalization': u'GENERALIZATION',
            u'howToUse': u'HOW_TO_USE', u'material': u'MATERIAL', u'place': u'PLACE', u'purpose': u'PURPOSE',
            u'shape': u'SHAPE', u'similarity': u'SIMILARITY', u'size': u'SIZE', u'smell': u'SMELL',
            u'sound': u'SOUND', u'specialization': u'SPECIALIZATION', u'taste': u'TASTE', u'time': u'TIME',
            u'part': u'PART'}

CHOSEN_DOMAINS = ["Education", "Farming", "Mathematics", "History", "Literature and Theatre", "Health and Medicine"]


def get_all_relations_kbs():
    return RBN2RKBS.values()


def get_all_relations():
    return RBN2RKBS.keys()


def get_choosen_relations():
    relations = set()
    for domain in get_all_domains(chosen=True):
        relations = relations.union(get_relations_for_domain_kbs(domain=domain))
    return list(relations)


def get_domains_to_relations_kbs(d2r_file=D2R_FILE, chosen=False):
    d2r = get_domains_to_relations(d2r_file, chosen=chosen)
    d2r_kbs = {}
    for domain, relations in d2r.items():
        relations_kbs = list(RBN2RKBS[x] for x in relations if len(x) > 0)
        d2r_kbs[domain] = relations_kbs

    return d2r_kbs


def get_domains_to_relations(d2r_file=D2R_FILE, chosen=False):
    res = {}
    if not os.path.isfile(d2r_file):
        return res

    reader = csv.reader(codecs.open(d2r_file, encoding='utf-8', mode='r'), dialect="excel-tab")
    for line in reader:
        if len(line) > 0 and (not chosen or line[0] in CHOSEN_DOMAINS):
            domain = line[0]  # type: str
            relations = line[1:-1]  # type: list

            res[domain] = relations

    return res


def get_all_domains(d2r_file=D2R_FILE, chosen=False):
    res = []
    if not os.path.isfile(d2r_file):
        return res

    reader = csv.reader(codecs.open(d2r_file, encoding='utf-8', mode='r'), dialect="excel-tab")
    for line in reader:
        if len(line) > 0 and (not chosen or line[0] in CHOSEN_DOMAINS):
            res.append(line[0])

    return res


def get_domains_size(d2r_file=D2R_FILE, chosen=False):
    import ioutils
    domains_size = {}
    for dom, rels in get_domains_to_relations_kbs(d2r_file=d2r_file, chosen=chosen).items():
        domains_size[dom] = ioutils.get_data_size(relations=rels)

    return domains_size


def get_relations_for_domain_kbs(domain, d2r_file=D2R_FILE):
    res = []
    if not os.path.isfile(d2r_file):
        return res

    reader = csv.reader(codecs.open(d2r_file, encoding='utf-8', mode='r'), dialect="excel-tab")
    for line in reader:
        if len(line) > 0:
            if line[0] == domain:
                return list(RBN2RKBS[x] for x in line[1:] if len(x) > 0)

    return res


def get_patterns_for_relations(relations=get_all_relations_kbs(), patterns_dir=PATTERNS_DIR):
    relpat = {}
    for relation in relations:
        patterns_file_path = patterns_dir + "/" + relation + ".tsv"
        patterns_file = open(patterns_file_path, mode='r')
        patterns = []
        for line in patterns_file:
            patterns.append(line)

        relpat[relation] = sorted(patterns, key=lambda x: WordPunctTokenizer().tokenize(x))

    return relpat


if __name__ == "__main__":
    pass
