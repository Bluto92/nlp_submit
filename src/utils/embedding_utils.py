from tqdm import tqdm
import ioutils as iot
from nltk import tokenize
import spacy
import numpy
from gensim.models.keyedvectors import KeyedVectors

GLOVE_FILE_PATH = "../glove/glove.840B.300d.txt"
# GLOVE_FILE_PATH_LITTLE = "/run/media/fra/Dati/NLP/glove.6B.300d.txt"
# GLOVE_FILE_PATH_MINE = "/home/fra/Projects/Universita/NLP/FinalProject/glove/model/vectors.txt"

""" GLOVE """


def get_glove_vocabulary_size(glove_file_path=GLOVE_FILE_PATH, additional_words=list()):
    """

    :param additional_words:
    :param glove_file_path:
    :return:
    """
    return len(get_glove_vocabulary(glove_file_path=glove_file_path, additional_words=additional_words))


def get_glove_embedding(glove_file_path=GLOVE_FILE_PATH, bar=False):
    model = {}
    if bar:
        td = tqdm(total=get_glove_vocabulary_size(glove_file_path=glove_file_path))

    glove_file = open(glove_file_path, mode='r')
    for line in glove_file:
        splits = line.split()
        word = splits[0]
        vector = [float(x) for x in splits[1:]]
        model[word] = vector
        if td:
            td.update(1)
    td.close()
    glove_file.close()
    return model


def get_glove_size(glove_file_path=GLOVE_FILE_PATH, additional_words=list()):
    glove_file = open(glove_file_path, mode='r')
    counter = 0
    for _ in glove_file:
        counter += 1
    glove_file.close()
    return counter + len(additional_words)


def get_full_glove_matrix(glove_file_path=GLOVE_FILE_PATH, pad='$pad$', unk='$unk$'):
    index = 2
    total = get_glove_vocabulary_size(glove_file_path=glove_file_path) + index
    coefs_matrix = numpy.zeros((total, 300), dtype=float)
    print "Matrix shape: ", coefs_matrix.shape
    desc = "Extracting Matrix from Glove "
    with tqdm(total=total, desc=desc) as td:
        for _, glove_vector in generator_glove_word_vector(glove_file_path=glove_file_path):
            td.update(1)
            coefs_matrix[index] = glove_vector
            index += 1

    coefs_matrix[0] = numpy.zeros(300, dtype=numpy.float32)
    coefs_matrix[1] = numpy.ones(300, dtype=numpy.float32)
    return coefs_matrix


def get_full_glove_w2i(glove_file_path=GLOVE_FILE_PATH, pad='$pad$', unk='$unk$'):
    w2i = {}

    index = 2
    total = get_glove_vocabulary_size(glove_file_path=glove_file_path) + index
    desc = "Extracting Word2Index from Glove "
    with tqdm(total=total, desc=desc) as td:
        for glove_word in generator_glove_word(glove_file_path=glove_file_path):
            td.update(1)
            w2i[str(glove_word)] = index
            index += 1

        w2i[pad] = 0
        w2i[unk] = 1
        td.update(2)

    return w2i


def get_full_glove_matrix_w2i(glove_file_path=GLOVE_FILE_PATH, pad='$pad$', unk='$unk$'):
    w2i = {}

    index = 2
    total = get_glove_vocabulary_size(glove_file_path=glove_file_path) + index
    coefs_matrix = numpy.zeros((total, 300), dtype=numpy.float)
    desc = "Extracting Word2Index and Matrix from Glove "
    with tqdm(total=total, desc=desc) as td:
        for glove_word, glove_vector in generator_glove_word_vector(glove_file_path=glove_file_path):
            td.update(1)
            coefs_matrix[index] = glove_vector
            w2i[glove_word] = index
            index += 1

    w2i[pad] = 0
    coefs_matrix[0] = numpy.zeros(300, dtype=numpy.float32)
    w2i[unk] = 1
    coefs_matrix[1] = numpy.ones(300, dtype=numpy.float32)

    print "Matrix shape: ", coefs_matrix.shape

    return coefs_matrix, w2i


def get_glove_matrix_w2i(glove_file_path=GLOVE_FILE_PATH, additional_words=list(),
                         restricted=None, pad='$pad$', unk='$unk$'):
    w2i = {}
    vector_list = []
    seen = set()
    if restricted is not None:
        for additional_word in additional_words:
            restricted.add(additional_word)

    index = 2
    total = get_glove_vocabulary_size(glove_file_path=glove_file_path, additional_words=additional_words)
    desc = "Extracting Word2Index and Matrix from Glove "
    with tqdm(total=total, desc=desc) as td:
        for glove_word, glove_vector in generator_glove_word_vector(
                glove_file_path=glove_file_path, additional_words=additional_words):
            td.update(1)
            if glove_word.lower() not in seen:
                seen.add(glove_word.lower())
                if restricted is not None:
                    if glove_word.lower() in restricted:
                        restricted.remove(glove_word.lower())
                        vector_list.append(glove_vector)
                        w2i[glove_word.lower()] = index
                        index += 1
                else:
                    vector_list.append(glove_vector)
                    w2i[glove_word.lower()] = index
                    index += 1

    w2i[pad] = 0
    vector_list.insert(0, numpy.zeros(300, dtype=numpy.float32))
    w2i[unk] = 1
    vector_list.insert(1, numpy.ones(300, dtype=numpy.float32))
    coefs_matrix = numpy.asmatrix(vector_list, dtype=numpy.float)
    print "Matrix shape: ", coefs_matrix.shape

    return coefs_matrix, w2i


def get_glove_matrix(glove_file_path=GLOVE_FILE_PATH, additional_words=list(), restricted=None):
    """

    :param glove_file_path:
    :param additional_words:
    :param List restricted:
    :return:
    """
    vector_list = []
    seen = set()
    if restricted is not None:
        for additional_word in additional_words:
            restricted.add(additional_word)

    total = get_glove_vocabulary_size(glove_file_path=glove_file_path, additional_words=additional_words)
    desc = "Extracting Matrix from Glove: "
    with tqdm(total=total, desc=desc) as td:
        for glove_word, glove_vector in generator_glove_word_vector(
                glove_file_path=glove_file_path, additional_words=additional_words):
            td.update(1)
            if glove_word.lower() not in seen:
                seen.add(glove_word.lower())
                if restricted is not None and glove_word.lower() in restricted:
                    restricted.remove(glove_word.lower())
                    vector_list.append(glove_vector)

    coefs_matrix = numpy.asmatrix(vector_list)
    print "Matrix shape: ", coefs_matrix.shape

    return coefs_matrix


def get_glove_word2index(glove_file_path=GLOVE_FILE_PATH, additional_words=list(), restricted=None):
    w2i = {}
    seen = set()
    index = 0

    if restricted is not None:
        for additional_word in additional_words:
            restricted.add(additional_word)

    total = get_glove_vocabulary_size(glove_file_path=glove_file_path, additional_words=additional_words)
    desc = "Extracting Word2Index from Glove: "
    with tqdm(total=total, desc=desc) as td:
        for glove_word in generator_glove_word(glove_file_path=glove_file_path,
                                               additional_words=additional_words):
            if glove_word.lower() not in seen:
                seen.add(glove_word.lower())
                if restricted is not None:
                    if glove_word.lower() in restricted:
                        restricted.remove(glove_word.lower())
                    else:
                        continue

                w2i[glove_word.lower()] = index
                index += 1
            td.update(1)

    return w2i


def get_glove_index2word(glove_file_path=GLOVE_FILE_PATH, additional_words=list()):
    i2w = {}
    w2i = get_glove_word2index(glove_file_path=glove_file_path, additional_words=additional_words)
    for key, value in w2i.items():
        i2w[value] = key

    return i2w


def get_glove_vocabulary(glove_file_path=GLOVE_FILE_PATH, additional_words=list()):
    vocabulary = list()
    for word in generator_glove_word(glove_file_path=glove_file_path):
        vocabulary.append(word)

    for word in additional_words:
        if word not in vocabulary:
            vocabulary.append(word)

    return vocabulary


def generator_glove_word(glove_file_path=GLOVE_FILE_PATH, additional_words=list()):
    for word in additional_words:
        yield unicode(word)
    # import codecs
    # glove_file = codecs.open(glove_file_path, mode='r', encoding='utf-8')
    glove_file = open(glove_file_path, mode='r')
    for line in glove_file:
        splits = line.split()
        if len(splits) > 0:
            word = splits[0]
            yield word

    glove_file.close()


def generator_glove_word_vector(glove_file_path=GLOVE_FILE_PATH, additional_words=list()):
    for word in additional_words:
        yield unicode(word), numpy.zeros(300, dtype=numpy.float32)

    # import codecs
    # glove_file = codecs.open(glove_file_path, mode='r', encoding='utf-8')
    glove_file = open(glove_file_path, mode='r')
    for line in glove_file:
        splits = line.split()
        if len(splits) > 0:
            word = splits[0]
            vector = splits[1:]
            yield word, vector
    glove_file.close()


def get_training_set_vocabulary(tokenizer=tokenize.WordPunctTokenizer()):
    words = set()

    with tqdm(total=iot.get_data_size()) as td:
        for data_entry in iot.generator_data_from_chunks():
            q_words = tokenizer.tokenize(data_entry['question'])
            a_words = tokenizer.tokenize(data_entry['answer'])

            for q_word, a_word in zip(q_words, a_words):
                if '\\' not in q_word:
                    words.add(q_word.lower())
                if '\\' not in a_word:
                    words.add(a_word.lower())

            td.update(1)

    return words

"""
def compare_models():
    from copy import deepcopy

    Words = get_training_set_vocabulary()
    WCopy = deepcopy(Words)

    print "Words in Training Set", str(len(Words))

    with tqdm(total=get_glove_vocabulary_size(glove_file_path=GLOVE_FILE_PATH_LITTLE)) as td:
        for gword in generator_glove_word(glove_file_path=GLOVE_FILE_PATH_LITTLE):
            if gword.lower() in WCopy:
                WCopy.remove(gword.lower())
            td.update(1)

    print "Dataset Words - Glove: ", str(len(WCopy)), "/", str(len(Words)), " ", str(
        (float(len(Words) - len(WCopy))) / float(len(Words))), "%"

    with tqdm(total=get_glove_vocabulary_size()) as td:
        for gword in generator_glove_word():
            if gword.lower() in WCopy:
                WCopy.remove(gword.lower())
            td.update(1)

    print "Dataset Words - Glove: ", str(len(WCopy)), "/", str(len(Words)), " ", str(
        (float(len(Words) - len(WCopy))) / float(len(Words))), "%"

    exit(1)
    WCopy = deepcopy(Words)
    spacy_vocabulary = spacy.load('en_core_web_md').vocab
    with tqdm(total=len(spacy_vocabulary)) as td:
        for lexeme in spacy_vocabulary:
            if lexeme.has_vector and lexeme.lower_ in WCopy:
                WCopy.remove(lexeme.lower_)
            td.update(1)

    print "Dataset Words - Spacy: ", str(len(WCopy)), "/", str(len(Words)), " ", str(
        (float(len(Words) - len(WCopy))) / float(len(Words))), "%"

    WCopy = deepcopy(Words)
    word_vector = KeyedVectors.load_word2vec_format(
        '/run/media/fra/DATA/NLP/GoogleNews-vectors-negative300.bin/data', binary=True)
    with tqdm(total=len(word_vector.vocab)) as td:
        for word in word_vector.vocab:
            if word in WCopy:
                WCopy.remove(word.lower())
            td.update(1)

    print "Dataset Words - W2V: ", str(len(WCopy)), "/", str(len(Words)), " ", str(
        (float(len(Words) - len(WCopy))) / float(len(Words))), "%"

"""

if __name__ == "__main__":
    # print "Glove Vocabulary Length:", str(len(get_glove_embedding(bar=True)))

    # matrix = get_glove_matrix()
    # compare_models()

    pass
