from nltk.tokenize import WordPunctTokenizer


def strip(sentence, stopwords):
    """

    :param str | unicode sentence:
    :param List[str | unicode] stopwords:
    :return:
    :rtype: unicode
    """
    new_sent = lstrip(sentence, stopwords)
    # print "Left strip:", new_sent
    return rstrip(new_sent, stopwords)


def lstrip(sentence, stopwords):
    """

    :param str | unicode sentence:
    :param List[str | unicode] stopwords:
    :return:
    :rtype: unicode
    """
    if not sentence:
        return u''
    tokenizer = WordPunctTokenizer()
    stripped_sentence = u''
    guard = True
    for word in tokenizer.tokenize(sentence.rstrip().lstrip()):
        if guard:
            if word not in stopwords:
                guard = False
            else:
                continue
        stripped_sentence += word + u' '

    final_sentence = stripped_sentence.rstrip()
    return final_sentence if final_sentence != u'' else None


def rstrip(sentence, stopwords):
    """

    :param str | unicode sentence:
    :param List[str | unicode] stopwords:
    :return:
    :rtype: unicode
    """
    if not sentence:
        return u''
    tokenizer = WordPunctTokenizer()
    stripped_reversed_sentence = u""
    guard = True
    for word in reversed(tokenizer.tokenize(sentence.rstrip().lstrip())):
        if guard:
            if word not in stopwords:
                guard = False
            else:
                continue
        stripped_reversed_sentence += word + u' '

    stripped_sentence = u''
    tokens_reversed = tokenizer.tokenize(stripped_reversed_sentence)
    for word in reversed(tokens_reversed):
        stripped_sentence += word + u' '

    final_sentence = stripped_sentence.rstrip()
    return final_sentence if final_sentence != u'' else None


def find_words(sentence, sub_sentence):
    """
    Tokenizes the sentence using a WordPunctTokenizer and tries to find the sub-sentence.
    It returns the start and and token-position of the sub-sentence in the sentence

    :param unicode sentence:
    :param unicode sub_sentence:
    :return: the start and and token-position of the sub-sentence in the sentence
    :rtype: Tuple[int, int]
    """

    tokenizer = WordPunctTokenizer()
    sentence_tokens = tokenizer.tokenize(sentence)
    sub_sentence_tokens = tokenizer.tokenize(sub_sentence)
    sub_sent_pos = 0
    founds = []
    for sent_pos, sentence_word in enumerate(sentence_tokens):
        if sentence_word == sub_sentence_tokens[sub_sent_pos]:
            sub_sent_pos += 1
            if sub_sent_pos == len(sub_sentence_tokens):
                founds.append((sent_pos - sub_sent_pos + 1, sent_pos + 1))
                sub_sent_pos = 0
        else:
            sub_sent_pos = 0
    return founds

if __name__ == '__main__':
    pass


