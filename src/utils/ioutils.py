import ujson
import glob
import domains_utils

DATA_DIR = "../data/"
CHUNKS_DIR = DATA_DIR + "chunks/"


def get_relation_names(chunks_dir=CHUNKS_DIR):
    relations = []
    for filepath in glob.glob1(chunks_dir, "data_chunk_*.txt"):
        data_file = open(chunks_dir + filepath, 'r')
        data_list = ujson.load(data_file)

        for data_entry in data_list:  # type: dict
            if data_entry['relation'] not in relations:
                relations.append(data_entry['relation'])

    return relations


def extract_data_from_chunks(chunks_dir=CHUNKS_DIR, relations=domains_utils.get_all_relations_kbs()):
    """

    :param list[str] relations:
    :param str chunks_dir:
    :return
    :rtype: dict[str, list[dict]:
    """
    # extract ordered data
    rel_dict_data = {}
    for filepath in glob.glob1(chunks_dir, "data_chunk_*.txt"):
        data_file = open(chunks_dir + filepath, 'r')
        data_list = ujson.load(data_file)

        for data_entry in data_list:  # type: dict
            if data_entry['relation'] in relations:
                for key, value in data_entry.items():
                    data_entry[key] = value.lower()

                if data_entry['relation'] not in rel_dict_data:
                    rel_dict_data[data_entry['relation']] = []
                rel_dict_data[data_entry['relation']].append(data_entry)

    return rel_dict_data


def generator_data_from_chunks(chunks_dir=CHUNKS_DIR, relations=domains_utils.get_all_relations_kbs()):
    """

    :param str chunks_dir:
    :param list[unicode] relations:
    :return:
    :rtype: dict
    """
    for filepath in glob.glob1(chunks_dir, "data_chunk_*.txt"):
        data_file = open(chunks_dir + filepath, 'r')
        data_list = ujson.load(data_file)
        for data_entry in data_list:  # type: dict
            if data_entry['relation'] in relations:
                for key, value in data_entry.items():
                    if type(value) == str or type(value) == unicode:
                        data_entry[key] = value.lower()
                yield data_entry


def generator_datalist_from_chunks(chunks_dir=CHUNKS_DIR):
    for filepath in glob.glob1(chunks_dir, "data_chunk_*.txt"):
        data_file = open(chunks_dir + filepath, 'r')
        data_list = ujson.load(data_file)

        for data_entry in data_list:
            for key, value in data_entry.items():
                if type(value) == str or type(value) == unicode:
                    data_entry[key] = value.lower()

        yield data_list


def get_data_size(chunks_dir=CHUNKS_DIR, relations=domains_utils.get_all_relations_kbs()):
    total_len = 0
    equals = False
    if relations == domains_utils.get_all_relations():
        equals = True
    for data_list in generator_datalist_from_chunks(chunks_dir=chunks_dir):
        if equals:
            total_len += len(data_list)
        else:
            for data_entry in data_list:  # type: dict
                if data_entry['relation'] in relations:
                    total_len += 1

    return total_len


def get_question_answer_lengths(relations=domains_utils.get_all_relations_kbs()):
    from nltk.tokenize import WordPunctTokenizer
    from tqdm import tqdm
    import data_utils

    lengths_questions, lengths_answers = {}, {}
    with tqdm(total=get_data_size(relations=relations)) as td:
        for relation in relations:
            for data_entry in data_utils.parse_data_per_relation(relation=relation,
                                                                 per_rel_dir="/home/fra/Projects/Universita/NLP/"
                                                                             "FinalProject/data/"
                                                                             "per_relation_refactored/"
                                                                             + relation.lower() + "/"):

                question = data_entry['question']
                answer = data_entry['answer']

                tokenizer = WordPunctTokenizer()
                question_tokens = tokenizer.tokenize(question)
                answer_tokens = tokenizer.tokenize(answer)

                question_number_words = len(question_tokens)
                answer_number_words = len(answer_tokens)

                if answer_number_words == 0:
                    print question, ": ", answer

                current_qnw = lengths_questions.get(question_number_words, 0)
                current_anw = lengths_answers.get(answer_number_words, 0)

                lengths_questions[question_number_words] = current_qnw + 1
                lengths_answers[answer_number_words] = current_anw + 1

                td.update(1)

    return lengths_questions, lengths_answers


if __name__ == "__main__":
    get_question_answer_lengths(relations=[u'GENERALIZATION'])
    pass
