import tarfile
import ujson
import codecs
import tqdm
import os
from data_iterators import DataIterator
from ioutils import get_data_size

DATA_DIR = "../data/"
CHUNKS_DIR = DATA_DIR + "chunks/"
IR_DIR = "../IR/"
GZIP_DATA_FILE = IR_DIR + "data.tar"
TMP_DIR = "/opt/"
TMP_DIR_IR = TMP_DIR + "ir_refactor/"


def refactor_data_ir_compress(compressed_file=GZIP_DATA_FILE, compression_format="bz2"):
    compressed_file += "." + compression_format
    gopen = tarfile.open(compressed_file, mode='w:' + compression_format)

    if not os.path.exists(TMP_DIR_IR):
        os.mkdir(TMP_DIR_IR)

    with tqdm.tqdm(total=get_data_size()) as td:
        for relation_element in DataIterator():
            rel_id = str(relation_element[u'HASH'])
            json_representation = ujson.encode(relation_element)
            acf = codecs.open(TMP_DIR_IR + rel_id, mode='w+')
            acf.write(json_representation)
            acf.flush()
            acf.close()
            td.update(1)

        gopen.add(TMP_DIR_IR, '.')
        gopen.close()

        # clean
        for irf in os.listdir(TMP_DIR_IR):
            os.remove(TMP_DIR_IR + irf)
        os.rmdir(TMP_DIR_IR)


def iterator_cir(compressed_file=GZIP_DATA_FILE, compression_format="bz2"):
    compressed_file += "." + compression_format
    gopen = tarfile.open(compressed_file, mode='r:' + compression_format)

    for gfileinfo in gopen:
        gfile = gopen.extractfile(gfileinfo)
        if gfile is not None:
            content = gfile.readline()
            yield ujson.decode(content)


if __name__ == "__main__":
    # refactor_data_ir_compress()
    for json_rep in iterator_cir():
        # print json_rep
        pass
