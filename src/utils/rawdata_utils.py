import ujson
import glob
import domains_utils
from nltk.tokenize import WordPunctTokenizer

DATA_DIR = "../data/"
CHUNKS_DIR = DATA_DIR + "chunks/"

IR_DIR = "../IR/"
IR_DATA_DIR = IR_DIR + "data/"


def get_relation_names(chunks_dir=CHUNKS_DIR):
    relations = []
    for filepath in glob.glob1(chunks_dir, "data_chunk_*.txt"):
        data_file = open(chunks_dir + filepath, 'r')
        data_list = ujson.load(data_file)

        for data_entry in data_list:  # type: dict
            if data_entry['relation'] not in relations:
                relations.append(data_entry['relation'])

    return relations


def extract_data_from_chunks(chunks_dir=CHUNKS_DIR, relations=domains_utils.get_all_relations_kbs()):
    """

    :param list[str] relations:
    :param str chunks_dir:
    :return
    :rtype: dict[str, list[dict]:
    """
    # extract ordered data
    rel_dict_data = {}
    for filepath in glob.glob1(chunks_dir, "data_chunk_*.txt"):
        data_file = open(chunks_dir + filepath, 'r')
        data_list = ujson.load(data_file)

        for data_entry in data_list:  # type: dict
            if data_entry['relation'] in relations:
                if data_entry['relation'] not in rel_dict_data:
                    rel_dict_data[data_entry['relation']] = []
                rel_dict_data[data_entry['relation']].append(data_entry)

    return rel_dict_data


def generator_data_from_chunks(chunks_dir=CHUNKS_DIR, relations=domains_utils.get_all_relations_kbs()):
    """

    :param str chunks_dir:
    :param list[unicode] relations:
    :return:
    :rtype: dict
    """
    for filepath in glob.glob1(chunks_dir, "data_chunk_*.txt"):
        data_file = open(chunks_dir + filepath, 'r')
        data_list = ujson.load(data_file)
        for data_entry in data_list:  # type: dict
            if data_entry['relation'] in relations:
                yield data_entry


def generator_datalist_from_chunks(chunks_dir=CHUNKS_DIR):
    for filepath in glob.glob1(chunks_dir, "data_chunk_*.txt"):
        data_file = open(chunks_dir + filepath, 'r')
        data_list = ujson.load(data_file)
        yield data_list


def get_data_size(chunks_dir=CHUNKS_DIR, relations=domains_utils.get_all_relations_kbs()):
    total_len = 0
    equals = False
    if relations == domains_utils.get_all_relations():
        equals = True
    for data_list in generator_datalist_from_chunks(chunks_dir=chunks_dir):
        if equals:
            total_len += len(data_list)
        else:
            for data_entry in data_list:  # type: dict
                if data_entry['relation'] in relations:
                    total_len += 1

    return total_len


def get_question_answer_lengths(relations=domains_utils.get_all_relations_kbs()):
    from nltk.tokenize import word_tokenize
    from tqdm import tqdm

    lengths_questions, lengths_answers = {}, {}
    with tqdm(total=get_data_size(relations=relations)) as td:
        for data_entry in generator_data_from_chunks(relations=relations):
            question = data_entry['question']
            answer = data_entry['answer']

            question_tokens = word_tokenize(question)
            answer_tokens = word_tokenize(answer)

            question_number_words = len(question_tokens)
            answer_number_words = len(answer_tokens)

            current_qnw = lengths_questions.get(question_number_words, 0)
            current_anw = lengths_answers.get(answer_number_words, 0)

            lengths_questions[question_number_words] = current_qnw + 1
            lengths_answers[answer_number_words] = current_anw + 1

            td.update(1)
    return lengths_questions, lengths_answers


def generate_all_words(fields=None, tokenizer=WordPunctTokenizer(),
                       chunks_dir=CHUNKS_DIR, relations=domains_utils.get_all_relations_kbs()):
    if fields is None:
        fields = ['question', 'answer']
    from tqdm import tqdm
    with tqdm(total=get_data_size(chunks_dir=chunks_dir, relations=relations),
              desc="Scanning all words in dataset") as td:
        for data_entry in generator_data_from_chunks(chunks_dir=chunks_dir, relations=relations):  # type: dict
            for field in fields:
                if field in data_entry.keys():
                    sentence = data_entry[field]
                    if sentence:
                        for token in tokenizer.tokenize(sentence):  # type: unicode
                            if u'\\' not in token:
                                yield token.lower()

            td.update(1)


if __name__ == "__main__":
    counter = 0
    for _ in generator_data_from_chunks():
        counter += 1
    print counter
