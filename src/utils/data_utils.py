import rawdata_utils, domains_utils
import os
import ujson
from tqdm import tqdm
from nltk.tokenize import WordPunctTokenizer

DATA_DIR = "../data/"
CHUNKS_DIR = "../data/chunks/"
DATA_REL_DIR = "../data/per_relation_refactored/"
VOCABULARY_FILEPATH = "../data/vocabulary/vocab"


def refactor_data(relations=domains_utils.get_all_relations_kbs(), chunks_dir=CHUNKS_DIR, output_dir=DATA_REL_DIR):
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    with tqdm(total=len(relations)) as td:
        for relation in relations:
            td.desc = relation
            td.refresh()
            rel_out_filepath = output_dir + relation.lower() + "/" + relation.lower() + ".txt"
            rel_file = open(rel_out_filepath, mode='w+')
            data_entries = []
            for data_entry in rawdata_utils.generator_data_from_chunks(chunks_dir=chunks_dir, relations=[relation]):
                data_entry['question'] = data_entry['question'].strip('\"\'`').rstrip(' ').lstrip()
                answer = data_entry['answer'].strip('\"\'`').rstrip('.,! ').lstrip()
                if answer.lower() == 'y':
                    data_entry['answer'] = 'yes'
                elif answer.lower() == 'n':
                    data_entry['answer'] = 'no'
                else:
                    data_entry['answer'] = answer

                # data_entry['context'] = data_entry['context'].strip('"').strip('`').strip("'").strip("\"")

                data_entries.append(data_entry)
            ujson.dump(data_entries, rel_file, ensure_ascii=False)

            rel_file.close()

            td.update(1)


def parse_data_per_relation(relation, question_min_length=1, answer_min_length=1, per_rel_dir=DATA_REL_DIR):
    tokenizer = WordPunctTokenizer()

    rel_out_filepath = per_rel_dir + relation.lower() + "/" + relation.lower() + ".txt"
    rel_file = open(rel_out_filepath, mode='r+')
    data_entries = ujson.load(rel_file)

    final_data_entries = []
    for data_entry in data_entries:
        question = data_entry['question']
        answer = data_entry['answer']

        if len(tokenizer.tokenize(question)) >= question_min_length and len(
                tokenizer.tokenize(answer)) >= answer_min_length:
            final_data_entries.append(data_entry)

    return final_data_entries


def get_relation_size(relation, question_min_length=1, answer_min_length=1, per_rel_dir=DATA_REL_DIR):
    return len(parse_data_per_relation(relation=relation, per_rel_dir=per_rel_dir,
                                       question_min_length=question_min_length,
                                       answer_min_length=answer_min_length))


if __name__ == "__main__":
    refactor_data()
