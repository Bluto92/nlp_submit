import database
import requests
import ujson
import unicodecsv
import regex
import os
import sys
import logging

logging.basicConfig(filename='../logs/tgrambot.log', level=logging.DEBUG,
                    format='%(asctime)s - %(name)s -  %(message)s')

unicodecsv.field_size_limit(sys.maxsize)
KEY = '8170c577-ad32-4845-99ea-cb0caaa49a30'
DATA_DIR = '../data/'
BABEL_DIR = DATA_DIR + "babelnet/"
BABEL_S2B_FILENAME = "/synset2babel"

BABEL_STOP_MESSAGE = u'Your key is not valid or the daily requests limit has been reached. ' \
                     u'Please visit http://babelnet.org.'


def retrieve_info_for_synset_old(synsetId, max_tries=False, babel_dir=BABEL_DIR):
    if not is_valid_synset(synsetId=synsetId):
        return None

    if not os.path.exists(babel_dir):
        os.mkdir(babel_dir)

    babel_s2b_filepath = babel_dir + BABEL_S2B_FILENAME
    if os.path.exists(babel_s2b_filepath):
        reader = unicodecsv.reader(open(babel_s2b_filepath, mode='r'), encoding='utf8', dialect="excel-tab")
        for info in reader:
            if info[0] == synsetId:
                return ujson.decode(info[1])

    if max_tries:
        return None

    babel_url = "https://babelnet.io/v4/getSynset?id=" + str(synsetId) + "&key=" + str(KEY)
    writer = unicodecsv.writer(open(babel_s2b_filepath, mode='a+'), encoding='utf8', dialect="excel-tab")
    get_res = requests.get(babel_url)
    response_text = get_res.text
    if get_res.status_code == 200:
        decoded = ujson.decode(response_text)
        if u'message' in decoded:
            raise StopIteration(decoded['message'])
        writer.writerow([synsetId, response_text])
    else:
        raise StopIteration(get_res.text)

    return ujson.decode(response_text)


def retrieve_info_for_synset_db(synsetId, max_tries=False):
    if not is_valid_synset(synsetId=synsetId):
        return None

    json_rep = database.get_synset(SynsetId=synsetId)
    if json_rep is not None:
        return ujson.decode(json_rep)

    if max_tries:
        return None

    babel_url = "https://babelnet.io/v4/getSynset?id=" + str(synsetId) + "&key=" + str(KEY)
    get_res = requests.get(babel_url)
    response_text = get_res.text
    if get_res.status_code == 200:
        decoded = ujson.decode(response_text)
        if u'message' in decoded:
            raise StopIteration(decoded['message'])
        database.add_synset(SynsetId=synsetId, json_rep=response_text)
    else:
        raise Exception(get_res.text)

    return ujson.decode(response_text)


def disambiguate_sentence(text):
    babelfy_url = 'https://babelfy.io/v1/disambiguate'
    # params = 'text=' + text + '&lang=EN&key=KEY'  # + KEY
    params = 'text=' + text + '&lang=EN&key=' + KEY
    get_res = requests.get(babelfy_url, params=params)
    response_text = get_res.text
    if get_res.status_code == 200:
        decoded = ujson.decode(response_text)
        if u'message' in decoded:
            raise StopIteration(decoded['message'])
    else:
        raise Exception(get_res.text)

    return ujson.decode(response_text)


def is_valid_synset(synsetId):
    return regex.match("bn:\d{8,8}[a-z]$", synsetId)


def retrieve_lemma_for_synset(synsetId, max_tries=False):
    info = retrieve_info_for_synset_db(synsetId=synsetId, max_tries=max_tries)

    if info is None:
        return None

    senses = info['senses']
    if senses:
        return senses[0]['simpleLemma'].lower()
    return None


def validate_sentence(sentence):
    disambiguated_sentence = disambiguate_sentence(sentence)
    print disambiguated_sentence

    logging.debug('[BABELNET CLIENT] sentence: ' + sentence + ' len: ' + str(len(sentence)))
    for token in disambiguated_sentence:
        token_start_index = token['charFragment']['start']
        token_stop_index = token['charFragment']['end'] + 1
        logging.debug('[BABELNET CLIENT] start: ' + str(token_start_index) + ' end: ' + str(token_stop_index))

        if token_start_index == 0 and token_stop_index == len(disambiguated_sentence):
            return True
    return False


if __name__ == "__main__":
    # synsetId = u"bn:00058715n"
    # print retrieve_lemma_for_synset(synsetId=synsetId)

    # print is_valid_synset(synsetId)
    import pprint

    sentence = u'batman Wayne'
    # disambiguated = disambiguate_sentence(text=sentence)
    # pprint.pprint(disambiguated)

    validate_sentence(sentence)
    pass
