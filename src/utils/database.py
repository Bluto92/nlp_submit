import sys

import psycopg2
import psycopg2.extensions
from nltk.tokenize import WordPunctTokenizer

import babelnet_client
import domains_utils
import logging

logging.basicConfig(filename='../logs/tgrambot.log', level=logging.DEBUG,
                    format='%(asctime)s - %(name)s -  %(message)s')
logger = logging.getLogger(__name__)

QUERING_SCHEMA = 'quering'
ENRICHING_SCHEMA = 'enriching'

UNMATCHED_QUERIES_TABLE = "unmatched_queries"
NO_ANSWER_QUERIES = "no_answer_queries"

filter_objects = ['examples', 'of', 'example', 'or', 'and', 'that', 'this', 'it', 'you', 'is', 'are',
                  'have', 'has', 'a', 'an']
filter_subjects = ['of', 'or', 'and', 'that', 'this', 'it', 'you', 'is', 'are', 'have', 'has', 'a', 'an']


class KBDataWrapper(object):
    def __init__(self, relation, kbdata):
        self.relation = relation
        self.question = kbdata[0]
        self.answer = kbdata[1]
        self.subject = kbdata[2]
        self.object = kbdata[3]
        self.subject_synset = kbdata[4]
        self.object_synset = kbdata[5]
        self.subj_quest = kbdata[6]
        self.obj_quest = kbdata[7]
        self.subj_ans = kbdata[8]
        self.obj_ans = kbdata[9]
        self.yes_no = None


def create_connection(schema=QUERING_SCHEMA):
    connection = psycopg2.connect(
        # "host='localhost' dbname='nlp' user='postgres' password='postgres'")
        "host='localhost' dbname='nlp2' user='postgres' password='postgres'")
    connection.set_client_encoding('utf-8')
    psycopg2.extensions.register_type(psycopg2.extensions.UNICODE, connection)
    psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY, connection)
    cur = connection.cursor()
    cur.execute("SET search_path TO " + schema)
    return connection


def drop_tables(schema):
    connection = None
    try:
        connection = create_connection(schema=schema)
        cursor = connection.cursor()
        for relation in domains_utils.get_all_relations_kbs():
            sql = "DROP TABLE IF EXISTS " + relation + ";"
            cursor.execute(sql)

        connection.commit()
        cursor.close()
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()

        print 'Error %s' % e


def create_tables(schema=QUERING_SCHEMA):
    connection = None
    try:
        connection = create_connection(schema=schema)
        cursor = connection.cursor()

        # babelnet synsets
        cursor.execute("CREATE TABLE IF NOT EXISTS Synsets(SynsetId VARCHAR(12) PRIMARY KEY, json_rep VARCHAR(200000))")

        # QAs per-relation table
        for relation in domains_utils.get_all_relations_kbs():
            sql = "CREATE TABLE IF NOT EXISTS " + relation + "(question VARCHAR(500), " \
                                                             "answer VARCHAR(500) NOT NULL, " \
                                                             "subject VARCHAR(200), " \
                                                             "object VARCHAR(200), " \
                                                             "subject_synset VARCHAR(15), " \
                                                             "object_synset VARCHAR(15), " \
                                                             "subj_quest BOOL, " \
                                                             "obj_quest BOOL, " \
                                                             "subj_ans BOOL, " \
                                                             "obj_ans BOOL, " \
                                                             "yes_no BOOL, " \
                                                             "question_pattern VARCHAR(500), " \
                                                             "answer_pattern VARCHAR(500), " \
                                                             "PRIMARY KEY (question, answer))"
            # print sql
            cursor.execute(sql)

        # unmatched queries table
        if schema == QUERING_SCHEMA:
            # sql = "CREATE TABLE IF NOT EXISTS " + UNMATCHED_QUERIES_TABLE \
            #       + "(id SERIAL PRIMARY KEY, question VARCHAR(500) NOT NULL," \
            #         "yes_no BOOL NOT NULL, predicted_relation VARCHAR(50) NOT NULL)"
            sql = "CREATE TABLE IF NOT EXISTS " + UNMATCHED_QUERIES_TABLE \
                  + "(question VARCHAR(500) NOT NULL, yes_no BOOL NOT NULL, predicted_relation VARCHAR(50) NOT NULL," \
                    " PRIMARY KEY (question, yes_no, predicted_relation))"
            cursor.execute(sql)

        # no answer table
        if schema == ENRICHING_SCHEMA:
            sql = "CREATE TABLE IF NOT EXISTS " + NO_ANSWER_QUERIES + \
                  " (predicted_relation VARCHAR(50)," \
                  " question VARCHAR(500) NOT NULL," \
                  " validated_subject VARCHAR(500)," \
                  " validated_object VARCHAR(500)," \
                  " subject_synset VARCHAR(20)," \
                  " object_synset VARCHAR(20)," \
                  " yes_no BOOL NOT NULL," \
                  " question_pattern VARCHAR(500)," \
                  " PRIMARY KEY (predicted_relation, question)," \
                  "CHECK (validated_subject != '' and validated_object != '')) "
            cursor.execute(sql)

        connection.commit()
        cursor.close()
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()

        print 'Error %s' % e


def get_qas_with_subj_obj(relation, schema=QUERING_SCHEMA, limit=None):
    connection = create_connection(schema=schema)
    try:
        cursor = connection.cursor()
        sql = "SELECT * FROM " + relation + " WHERE (subj_quest = TRUE or subj_ans = TRUE) " \
                                            "and (obj_quest = TRUE or obj_ans = TRUE)"
        if limit:
            sql += " LIMIT " + str(limit)
        cursor.execute(sql)

        connection.commit()
        from copy import deepcopy
        results = []
        if cursor.rowcount > 0:
            results = deepcopy(cursor.fetchall())

        cursor.close()
        return results
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()

            print 'Error %s' % e

    pass


def get_all_qas(relation, schema=QUERING_SCHEMA, limit=None):
    connection = create_connection(schema=schema)
    try:
        cursor = connection.cursor()
        sql = "SELECT * FROM " + relation
        if limit:
            sql += " LIMIT " + str(limit)
        cursor.execute(sql)

        connection.commit()
        from copy import deepcopy
        results = []
        if cursor.rowcount > 0:
            results = deepcopy(cursor.fetchall())

        cursor.close()
        return results
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()

            # print 'Error %s' % e

    pass


def get_obj_subj(relation, subj, obj, schema=QUERING_SCHEMA):
    connection = create_connection(schema=schema)
    try:
        cursor = connection.cursor()
        cursor.execute(
            "SELECT * FROM " + relation + " WHERE object = '" + obj.rstrip(' ') + "' AND subject = '" + subj.rstrip(
                ' ') + "'")

        connection.commit()
        from copy import deepcopy
        results = set()
        if cursor.rowcount > 0:
            for row in cursor.fetchall():
                results.add(row)

        cursor.close()
        return sorted(list(results))
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()


def get_unmatched_questions():
    connection = create_connection()
    try:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM " + UNMATCHED_QUERIES_TABLE)

        connection.commit()
        from copy import deepcopy
        results = {}
        if cursor.rowcount > 0:
            for row in cursor.fetchall():
                rell = results.get(row[2], [])
                rell.append(deepcopy(row))
                results[row[2]] = rell
        tokenizer = WordPunctTokenizer()
        for key, values in results.items():
            results[key] = sorted(values, key=lambda x: tokenizer.tokenize(x[0]))

        cursor.close()
        return results
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()


def delete_unmatched_question(question, relation):
    connection = create_connection()

    try:
        cursor = connection.cursor()
        cursor.execute("DELETE FROM " + UNMATCHED_QUERIES_TABLE
                       + " WHERE lower(question) = '" + question + "' AND predicted_relation = '" + relation + "'")

        connection.commit()
        from copy import deepcopy
        results = {}
        if cursor.rowcount > 0:
            for row in cursor.fetchall():
                rell = results.get(row[2], [])
                rell.append(deepcopy(row))
                results[row[2]] = rell
        tokenizer = WordPunctTokenizer()
        for key, values in results.items():
            results[key] = sorted(values, key=lambda x: tokenizer.tokenize(x[0]))

        cursor.close()
        return results
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()


def get_unanswered_questions(relations=None, grouped=False, yes_no=False):
    connection = create_connection(schema=ENRICHING_SCHEMA)
    try:
        cursor = connection.cursor()
        sql = "SELECT * FROM " + NO_ANSWER_QUERIES + " WHERE validated_subject != '' AND yes_no != '" + str(
            yes_no) + "'"
        if type(relations) == list:
            sql += " AND ("
            for index, relation in enumerate(relations):
                sql += "predicted_relation = '" + relation + "'"
                if index < len(relations) - 1:
                    sql += " OR "

            sql += ")"
        cursor.execute(sql)

        connection.commit()
        from copy import deepcopy
        results = []

        if not grouped:
            if cursor.rowcount > 0:
                results = deepcopy(cursor.fetchall())
        else:
            results = {}
            if cursor.rowcount > 0:
                for row in cursor.fetchall():
                    rell = results.get(row[0], [])
                    rell.append(deepcopy(row))
                    results[row[0]] = rell

        cursor.close()
        return results
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()

        print e


def delete_unanswered_queries(question, relation):
    connection = create_connection(schema=ENRICHING_SCHEMA)
    try:
        cursor = connection.cursor()
        sql = "DELETE FROM " + NO_ANSWER_QUERIES + " WHERE question = '" + question \
              + "' AND predicted_relation = '" + relation + "'"
        cursor.execute(sql)

        connection.commit()
        cursor.close()
        return
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()


def get_all_subj_obj(relation, disambiguated=False):
    """

    :param bool disambiguated: whether to keep only the disambiguated subject/object pairs
    :param str | unicode relation:
    :return: sorted list of subject-objects pairs
    :rtype: List[unicode]
    """
    connection = create_connection()
    try:
        cursor = connection.cursor()
        sql = "SELECT subject, object"
        if disambiguated:
            sql += ", subject_synset, object_synset"
        sql += " FROM " + relation + " WHERE subject != '' AND object != ''"
        if disambiguated:
            sql += " AND subject_synset != '' AND object_synset != ''"
        cursor.execute(sql)

        connection.commit()
        from copy import deepcopy
        results = set()
        if cursor.rowcount > 0:
            for row in cursor.fetchall():
                results.add(row[0])

        cursor.close()
        return sorted(list(results))
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()


def get_all_objects(relation):
    """

    :param relation:
    :return: sorted list of objects
    :rtype: List[unicode]
    """
    connection = create_connection()
    try:
        cursor = connection.cursor()
        cursor.execute("SELECT object FROM " + relation + " WHERE object != ''")

        connection.commit()
        from copy import deepcopy
        results = set()
        if cursor.rowcount > 0:
            for row in cursor.fetchall():
                results.add(row[0])

        cursor.close()
        return sorted(list(results))
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()


def is_known(relation, entity):
    """

        :param str | unicode entity:
        :param str | unicode relation:
        :return: sorted list of subjects
        :rtype: List[unicode]
        """
    connection = create_connection()
    try:
        cursor = connection.cursor()
        cursor.execute(
            "SELECT count(*) FROM " + relation + " WHERE lower(subject) = '" + entity.lower()
            + "' or lower(object) = '" + entity.lower() + "'")

        connection.commit()
        result = cursor.fetchall()[0][0] > 0
        logger.debug("Is Known Entity: " + unicode(entity) + ": " + unicode(result))
        cursor.close()
        return result
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()


def get_all_subjects(relation):
    """

    :param relation:
    :return: sorted list of subjects
    :rtype: List[unicode]
    """
    connection = create_connection()
    try:
        cursor = connection.cursor()
        cursor.execute("SELECT subject FROM " + relation + " WHERE subject != ''")

        connection.commit()
        from copy import deepcopy
        results = set()
        if cursor.rowcount > 0:
            for row in cursor.fetchall():
                results.add(row[0])

        cursor.close()
        return sorted(list(results))
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()


def get_by_question(relation, question, lower=True):
    connection = create_connection()
    try:
        cursor = connection.cursor()
        if lower:
            cursor.execute("SELECT * FROM " + relation + " WHERE lower(question) = '"
                           + str(question).lower() + "'")
        else:
            cursor.execute("SELECT * FROM " + relation + " WHERE question='" + str(question) + "'")

        connection.commit()
        from copy import deepcopy
        results = []
        if cursor.rowcount > 0:
            results = deepcopy(cursor.fetchall())

        cursor.close()
        return results
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()


def get_by_answer(relation, answer, lower=True):
    connection = create_connection()
    try:
        cursor = connection.cursor()
        if lower:
            cursor.execute("SELECT * FROM " + relation + " WHERE lower(answer) = '" + str(answer).lower() + "'")
        else:
            cursor.execute("SELECT * FROM " + relation + " WHERE answer='" + str(answer) + "'")

        connection.commit()
        from copy import deepcopy
        results = []
        if cursor.rowcount > 0:
            results = deepcopy(cursor.fetchall())

        cursor.close()
        return results
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()


def get_by_subj(relation, subj, yes_no=False):
    connection = create_connection()
    try:
        cursor = connection.cursor()
        yn = 'TRUE' if yes_no else 'FALSE'
        cursor.execute(
            "SELECT * FROM " + relation + " WHERE lower(subject) = '" + subj.lower() + "' and yes_no = " + yn)

        connection.commit()
        from copy import deepcopy
        results = []
        if cursor.rowcount > 0:
            results = deepcopy(cursor.fetchall())

        cursor.close()
        return results
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()
        print e


def get_by_obj(relation, obj, yes_no=False):
    connection = create_connection()
    try:
        cursor = connection.cursor()
        yn = 'TRUE' if yes_no else 'FALSE'
        cursor.execute(
            "SELECT * FROM " + relation + " WHERE lower(object) = '" + obj.lower() + "' and yes_no = " + yn)

        connection.commit()
        from copy import deepcopy
        results = []
        if cursor.rowcount > 0:
            results = deepcopy(cursor.fetchall())

        cursor.close()
        return results
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()
        print e


def get_by_subj_obj(relation, subj, obj, yes_no=False):
    connection = create_connection()
    try:
        cursor = connection.cursor()
        yn = 'TRUE' if yes_no else 'FALSE'
        sql = "SELECT * FROM " + relation + " WHERE lower(object) = '" + obj.lower() + \
              "' and lower(subject) = '" + subj.lower() + "'"
        if yes_no is not None:
            sql += " and yes_no = " + yn
        cursor.execute(sql)

        connection.commit()
        from copy import deepcopy
        results = []
        if cursor.rowcount > 0:
            results = deepcopy(cursor.fetchall())

        cursor.close()
        return results
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()
        print e


def get_qas_size(relation, yes_no=None):
    """
    If the parameter 'yes_no' is true returns all the QAs where the answer is "yes" or "no", otherwise the others.
    If it is None then returns the count of all the entries
    :param str | unicode relation:
    :param bool | None yes_no:
    :return:
    """
    connection = create_connection()
    try:
        cursor = connection.cursor()
        sql = "SELECT count(*) FROM " + relation
        if yes_no is not None:
            sql += " WHERE yes_no='" + str(yes_no) + "'"
        cursor.execute(sql)

        connection.commit()
        size = int(cursor.fetchall()[0][0])

        cursor.close()
        return size
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()


def get_containing_words_in_question_qas(relation, words, any=False, yes_no=True):
    """
    If the parameter 'yes_no' is true returns all the QAs where the answer is "yes" or "no", otherwise the others
    :param str | unicode word:
    :param str | unicode relation:
    :param bool yes_no:
    :return:
    """
    connection = create_connection()
    try:
        cursor = connection.cursor()
        sql = "SELECT * FROM " + relation + " WHERE yes_no='" + str(yes_no) + "' and ("
        operation = 'or' if any else 'and'
        for index, word in enumerate(words):
            if 0 < index:
                sql += operation
            sql += " question like '%" + word + "%'"
        sql += ')'

        cursor.execute(sql)

        connection.commit()
        from copy import deepcopy
        results = []
        if cursor.rowcount > 0:
            results = deepcopy(cursor.fetchall())

        cursor.close()
        return results
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()


def get_yes_no_qas(relation, yes_no=True, schema=QUERING_SCHEMA, limit=None):
    """
    If the parameter 'yes_no' is true returns all the QAs where the answer is "yes" or "no", otherwise the others
    :param str | unicode relation:
    :param bool yes_no:
    :return:
    """
    connection = create_connection(schema=schema)
    try:
        cursor = connection.cursor()
        sql = "SELECT * FROM " + relation + " WHERE yes_no='" + str(yes_no) + "'"
        if limit:
            sql += " LIMIT " + str(limit)
        cursor.execute(sql)

        connection.commit()
        from copy import deepcopy
        results = []
        if cursor.rowcount > 0:
            results = deepcopy(cursor.fetchall())

        cursor.close()
        return results
    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()


def add_qa_data(relation, question, answer, schema='quering', subject=None, object=None, subject_synset=None,
                object_synset=None, subj_quest=None, obj_quest=None, subj_ans=None, obj_ans=None, yes_no=None,
                question_pattern=None, answer_pattern=None):
    if object in filter_objects or subject in filter_subjects:
        return

    connection = create_connection(schema=schema)
    try:
        cursor = connection.cursor()
        cursor.execute("INSERT INTO " + relation + " (question, answer, subject, object, subject_synset, object_synset,"
                                                   " subj_quest, obj_quest, subj_ans, obj_ans, yes_no,"
                                                   " question_pattern, answer_pattern) VALUES "
                                                   "(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       (question, answer, subject, object, subject_synset, object_synset,
                        subj_quest, obj_quest, subj_ans, obj_ans, yes_no, question_pattern, answer_pattern))

        connection.commit()
        cursor.close()

    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()

            # print 'Error %s' % e

    pass


def add_unmatched_query(predicted_relation, question, yes_no=True):
    connection = create_connection()
    try:
        cursor = connection.cursor()
        cursor.execute("INSERT INTO " + UNMATCHED_QUERIES_TABLE
                       + " (predicted_relation, question, yes_no) VALUES "
                       + "(%s, %s, %s)", (predicted_relation, question, yes_no))

        connection.commit()
        cursor.close()

    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()

            print 'Error %s' % e

    pass


def add_no_answer_question(predicted_relation, question, validated_subject, validated_object,
                           subject_synset, object_synset, yes_no, question_pattern):
    connection = create_connection(schema=ENRICHING_SCHEMA)
    try:
        cursor = connection.cursor()
        cursor.execute("INSERT INTO " + NO_ANSWER_QUERIES
                       + " (predicted_relation, question, validated_subject, validated_object,"
                       + " subject_synset, object_synset, yes_no, question_pattern) VALUES"
                       + " (%s, %s, %s, %s, %s, %s, %s, %s)",
                       (predicted_relation, question, validated_subject, validated_object,
                        subject_synset, object_synset, yes_no, question_pattern))

        connection.commit()
        cursor.close()

    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()

            print 'Error %s' % e

    pass


def add_synset(SynsetId, json_rep, connection=create_connection()):
    if not babelnet_client.is_valid_synset(synsetId=SynsetId):
        return None

    try:
        cursor = connection.cursor()
        cursor.execute("INSERT INTO Synsets VALUES ('" + SynsetId.strip(" ") + "', '" + json_rep + "');")
        connection.commit()
        cursor.close()

    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()

        print 'Error %s' % e


def get_synset(SynsetId, connection=create_connection()):
    """

    :param str | unicode SynsetId:
    :param connection:
    :return:
    """
    if not babelnet_client.is_valid_synset(synsetId=SynsetId):
        return None

    try:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM Synsets WHERE synsetid='" + SynsetId.strip(" ") + "' LIMIT 1;")
        connection.commit()

        if cursor.rowcount > 0:
            row = cursor.fetchone()[1]
        else:
            row = None
        cursor.close()
        return row

    except psycopg2.DatabaseError, e:
        if connection:
            connection.rollback()

        print 'Error %s' % e


def import_from_old_tsv():
    import unicodecsv
    import os

    unicodecsv.field_size_limit(sys.maxsize)
    DATA_DIR = '../data/'
    BABEL_DIR = DATA_DIR + "babelnet/"
    BABEL_S2B_FILENAME = "/synset2babel"

    babel_s2b_filepath = BABEL_DIR + BABEL_S2B_FILENAME
    if os.path.exists(babel_s2b_filepath):
        reader = unicodecsv.reader(open(babel_s2b_filepath, mode='r'), encoding='utf8', dialect="excel-tab")
        for info in reader:
            if info:
                add_synset(create_connection(), info[0], info[1])


def populate_db(relation):
    from src.utils import data_refactor
    data_refactor.fill_db(relation=relation)


if __name__ == '__main__':
    # import data_utils
    # import babelnet_client
    #
    # to_do = []
    # for data_entry in data_utils.parse_data_per_relation(u'GENERALIZATION'):
    #     if babelnet_client.is_valid_synset(data_entry['c1']):
    #         to_do.append(data_entry['c1'])
    #     if babelnet_client.is_valid_synset(data_entry['c2']):
    #         to_do.append(data_entry['c2'])
    #
    # print "To download: ", str(len(to_do) - 998)
    # pass
    # drop_tables()
    # drop_tables(ENRICHING_SCHEMA)
    create_tables(QUERING_SCHEMA)
    # create_tables(ENRICHING_SCHEMA)

    # yes_no = len(get_yes_no_qas('GENERALIZATION'))
    # complex = len(get_yes_no_qas('GENERALIZATION', yes_no=False))
    # print "YES/NO: ", str(yes_no)
    # print "Complex: ", str(complex)
    # print "Sum: ", str(yes_no + complex)
    # print "All: ", str(len(get_all_qas(u'GENERALIZATION')))
    # create_connection(schema='enriching')
    # print is_known(u'GENERALIZATION', 'batmaan')
    pass
