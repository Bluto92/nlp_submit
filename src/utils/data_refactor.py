from src.utils import data_utils
from nltk.tokenize import WordPunctTokenizer
import regex
from copy import deepcopy
from src.utils import database
from src.utils import babelnet_client
from tqdm import tqdm
import logging

logging.basicConfig(filename='../logs/tgrambot.log', level=logging.DEBUG,
                    format='%(asctime)s - %(name)s -  %(message)s')
logger = logging.getLogger(__name__)

GIVEN_PATTERNS_FILTERED_DIR = "../data/given_patterns/filtered/"
GIVEN_PATTERNS_FILTERING_DIR = "../data/given_patterns/filtering/"
PER_REL_DIR = "../data/per_relation_refactored/"
PRONS = [u'it', u'he', u'she', u'you', u'they', u'them', u'this', u'that', u'those', u'these', u'I']
SUBJ_PLACEHOLDER = u'$X$'
OBJ_PLACEHOLDER = u'$Y$'


def get_qa_patterns(filepath, good=True):
    rel_file = open(filepath, mode='r')
    question_answer_patterns = []

    qa_partial = None
    for line in rel_file:
        line_refactored = u''
        qa_line = line.strip("\n").strip("\r")
        question = qa_line.split("\t")[0]
        answer = qa_line.split("\t")[1].rstrip(' ')
        for word in question.split(" "):
            if word == SUBJ_PLACEHOLDER or word == OBJ_PLACEHOLDER:
                line_refactored += word + u' '
            else:
                line_refactored += word.lower() + u' '

        if good:
            if qa_partial is None:
                qa_partial = (line_refactored.rstrip(u' '), answer)
            else:
                question_answer_patterns.append((deepcopy(qa_partial), (line_refactored.rstrip(u' '), answer)))
                qa_partial = None
        else:
            question_answer_patterns.append(
                (deepcopy((line_refactored.rstrip(u' '), answer)), deepcopy((line_refactored.rstrip(u' '), answer))))

    def comparator(x, y):
        if len(x[0][0].split()) != len(y[0][0].split()):
            return len(x[0][0].split()) - len(y[0][0].split())
        else:
            return len(x[0][1].split()) - len(y[0][1].split())

    # question_answer_patterns.sort(key=lambda (x, y): len(x[0].split()), reverse=True)
    question_answer_patterns.sort(cmp=comparator, reverse=True)

    qapatterns2regexp = {}
    for original, refactor in question_answer_patterns:
        qpattern, apattern = original[0], original[1]

        regexp_question = u'^'
        for word in qpattern.split(u' '):
            if word == SUBJ_PLACEHOLDER or word == OBJ_PLACEHOLDER:
                regexp_question += u'(.*?) '
            elif word == u'?':
                regexp_question += u'\?'
            else:
                regexp_question += word.lower() + u' '
        regexp_answer = u'^'
        for word in apattern.split(u' '):
            if word == SUBJ_PLACEHOLDER or word == OBJ_PLACEHOLDER:
                regexp_answer += u'(.*?) '
            elif word == u'?':
                regexp_answer = regexp_answer.rstrip() + u'[ ]\?'
            else:
                regexp_answer += word.lower() + u' '

        qapatterns2regexp[(qpattern, apattern)] = (
            regexp_question.rstrip(u' ') + u'$', regexp_answer.rstrip(u' ') + u'$')

    return question_answer_patterns, qapatterns2regexp


def get_qa_patterns_wrong(relation):
    filtering_rel_dir = GIVEN_PATTERNS_FILTERING_DIR + relation.lower() + "/"
    question_answer_patterns, qapatterns2regexp = get_qa_patterns(
        filtering_rel_dir + relation.lower() + "_qa_patterns_wrong.tsv", good=False)
    return question_answer_patterns, qapatterns2regexp


def get_qa_patterns_good(relation, mode=database.QUERING_SCHEMA):
    filtering_rel_dir = GIVEN_PATTERNS_FILTERING_DIR + relation.lower() + "/"
    question_answer_patterns, qapatterns2regexp = get_qa_patterns(
        filtering_rel_dir + relation.lower() + "_qa_patterns_good_" + mode + ".tsv")

    return question_answer_patterns, qapatterns2regexp


def get_patterns(relation):
    rel_file = open(GIVEN_PATTERNS_FILTERED_DIR + relation.lower() + "_patterns.txt", mode='r')
    question_patterns = []

    for line in rel_file:
        question_patterns.append(line.rstrip(u' \n'))

    question_patterns = sorted(question_patterns, key=lambda x: len(x.split(u' ')), reverse=True)
    qpatterns2regexp = {}
    for qpattern in question_patterns:
        qpattern_proc = qpattern.replace(SUBJ_PLACEHOLDER, u'(.*?)')
        qpattern_proc = qpattern_proc.replace(OBJ_PLACEHOLDER, u'(.*?)')
        regexp = u'^' + qpattern_proc.rstrip(' ?') + u'[ ]\?$'
        qpatterns2regexp[qpattern] = regexp.rstrip(u' ') + u'$'

    return question_patterns, qpatterns2regexp


def get_patterns_lowered(relation):
    rel_file = open(GIVEN_PATTERNS_FILTERED_DIR + relation.lower() + "_patterns.txt", mode='r')
    question_patterns = []

    for line in rel_file:
        line_refactored = u''
        for word in line.strip("\n").split(" "):
            if word == SUBJ_PLACEHOLDER or word == OBJ_PLACEHOLDER:
                line_refactored += word + u' '
            else:
                line_refactored += word.lower() + u' '

        question_patterns.append(line_refactored.rstrip(u' '))

    question_patterns = sorted(question_patterns, key=lambda x: len(x.split(u' ')), reverse=True)
    qpatterns2regexp = {}
    for qpattern in question_patterns:
        regexp = u'^'
        for word in qpattern.split(u' '):
            if word == SUBJ_PLACEHOLDER or word == OBJ_PLACEHOLDER:
                regexp += u'(.*?) '
            elif word == u'?':
                regexp = regexp.rstrip() + u'[ ]\?'
            else:
                regexp += word.lower() + u' '

        qpatterns2regexp[qpattern] = regexp.rstrip(u' ') + u'$'

    return question_patterns, qpatterns2regexp


def match_pattern_question_answer(question_answer_patterns, qapatterns2regexp, question_answer):
    """

    :param question_answer_patterns:
    :param qapatterns2regexp:
    :param question_answer:
    :return:
    :rtype: Tuple[str, str]
    """
    found_patterns = None  # type: tuple
    for qpattern, refactor in question_answer_patterns:
        current_pattern = qapatterns2regexp[qpattern]
        if regex.match(current_pattern[0], question_answer[0]) and regex.match(current_pattern[1], question_answer[1]):
            found_patterns = qpattern, refactor
            break

    return found_patterns


def match_pattern_relation_question(relation, question):
    question_patterns, qpatterns2regexp = get_qa_patterns_good(relation=relation)
    found_pattern = None  # type: str
    for qpattern in question_patterns:
        current_pattern = qpatterns2regexp[qpattern]
        if regex.match(current_pattern, question):
            found_pattern = qpattern
            break
    return found_pattern


def match_pattern_question(question_patterns, qpatterns2regexp, question):
    found_pattern = None  # type: str
    for qpattern in question_patterns:
        current_pattern = qpatterns2regexp[qpattern]
        if regex.match(current_pattern, question):
            found_pattern = qpattern
            break
    return found_pattern


def extract_subj_obj_from_sentence(sentence, sentence_pattern, subj_placeholder=SUBJ_PLACEHOLDER,
                                   obj_placeholder=OBJ_PLACEHOLDER, lower=False, cased=False):
    # analyze pattern
    logger.debug("Sentence: " + sentence)
    sentence = sentence.rstrip('? ')
    if cased:
        new_sentence = u''
        for sent_word in sentence.split():
            if sent_word.lower() in sentence_pattern.strip():
                new_sentence += sent_word.lower() + u' '
            else:
                new_sentence += sent_word + u' '
        sentence = new_sentence.rstrip()
        logger.debug("Uncased Sentence: " + sentence)

    logger.debug("Sentence Pattern: " + sentence_pattern)
    obj_index = sentence_pattern.find(obj_placeholder.lower()) if lower else sentence_pattern.find(obj_placeholder)
    # obj_index = sys.maxint if obj_index == -1 else obj_index

    subj_index = sentence_pattern.find(subj_placeholder.lower()) if lower else sentence_pattern.find(subj_placeholder)
    # subj_index = sys.maxint if subj_index == -1 else subj_index

    subj, obj = None, None  # type: str
    reg_pattern = '^' + sentence_pattern.replace(obj_placeholder, '(.*?)').replace(subj_placeholder, '(.*?)') + '$'
    finds = regex.findall(reg_pattern, sentence.rstrip().lstrip())
    logger.debug("Found: " + str(finds))

    if 0 < len(finds):
        if subj_index == -1:
            obj = finds[0]
            logger.debug("Found Obj: " + obj)
        elif obj_index == -1:
            subj = finds[0]
            logger.debug("Found Subj: " + subj)
        elif subj_index < obj_index:
            obj = finds[0][1]
            subj = finds[0][0]
            logger.debug("Found Subj-Obj: " + subj + " " + obj)
        else:
            obj = finds[0][0]
            subj = finds[0][1]
            logger.debug("Found Obj-Subj: " + obj + " " + subj)

    return subj, obj


def get_question_subj_obj(relation, question):
    """

    :param relation:
    :param str | unicode question:
    :return:
    """
    question_patterns, qpatterns2regexp = get_patterns_lowered(relation)

    # refactor question
    question = question.lower().rstrip(' ?')
    question += u' ?'

    # guess pattern
    found_pattern = match_pattern_question(question_patterns, qpatterns2regexp, question=question)  # type: str

    if found_pattern is None:
        return None, None

    found_pattern = found_pattern.replace('?', '\?')
    return extract_subj_obj_from_sentence(question, found_pattern)


def there_is(sent, checkwords):
    for word in sent.split():
        if word in checkwords:
            return True
    return False


def extract_question_answer_patterns(relation):
    question_patterns, qpatterns2regexp = get_patterns_lowered(relation)
    tokenizer = WordPunctTokenizer()

    questions_matching = 0
    per_rel_dir = "/home/fra/Projects/Universita/NLP/FinalProject/data/per_relation_refactored/"
    # per_rel_dir = "/home/fra/Projects/Universita/NLP/FinalProject/data/per_relation/"

    short_answers, yes_answers, no_answers, others_patterns, others_answers = 0, 0, 0, 0, 0
    question_answer_patterns = set()
    not_refactorable, refactorable = [], []

    with tqdm(total=data_utils.get_relation_size(relation, per_rel_dir=per_rel_dir), desc=relation.lower()) as td:
        for data_entry in data_utils.parse_data_per_relation(relation, per_rel_dir=per_rel_dir):
            td.update(1)
            data_entry_so = deepcopy(data_entry)
            # normalize questions
            question = data_entry_so['question'].lower()  # type: str
            if (question.endswith(u'?') and not question.endswith(u' ?')) or not question.endswith('?'):
                question = question[:-1] + u' ?'
            answer = data_entry_so['answer'].lower()  # type: str

            # categorize answer
            tokens = tokenizer.tokenize(answer.rstrip('.!?'))
            yes_no = tokens[0].lower() == u'yes' or tokens[0].lower() == u'no'
            data_entry_so['yes_no'] = yes_no

            # guess pattern
            found_pattern = match_pattern_question(question_patterns, qpatterns2regexp, question)  # type: str
            if found_pattern is None:
                not_refactorable.append(data_entry_so)
                continue

            data_entry_so['question_pattern'] = found_pattern
            found_pattern = found_pattern.replace('?', '\?')

            subj, obj = extract_subj_obj_from_sentence(question, found_pattern)
            subj_quest, obj_quest = subj is not None, obj is not None

            questions_matching += 1
            subj_answer, obj_answer = False, False

            if len(tokens) == 1:
                # apattern = None
                short_answers += 1
                if tokens[0].lower() == u'yes':
                    yes_answers += 1
                    question_answer_patterns.add((found_pattern.replace('\?', '?'), answer.lower()))
                    apattern = u'yes'
                elif tokens[0].lower() == u'no':
                    no_answers += 1
                    question_answer_patterns.add((found_pattern.replace('\?', '?'), answer.lower()))
                    apattern = u'no'
                else:
                    _, value = extract_subj_obj_from_sentence(answer, '$Y$')
                    if not value:
                        continue

                    if subj and not obj:
                        obj = value
                        apattern = OBJ_PLACEHOLDER
                    elif not subj and obj:
                        subj = value
                        apattern = SUBJ_PLACEHOLDER
                    elif subj and obj:
                        if value == subj:
                            apattern = SUBJ_PLACEHOLDER
                        elif value == obj:
                            apattern = OBJ_PLACEHOLDER
                        else:
                            continue
                    else:
                        continue
                    others_answers += 1
                    others_patterns += 1

                data_entry_so['answer_pattern'] = apattern

            elif 2 <= len(tokens):
                others_patterns += 1
                other_answer_patterns = ['a ' + OBJ_PLACEHOLDER, 'an ' + OBJ_PLACEHOLDER, OBJ_PLACEHOLDER]
                for apattern in other_answer_patterns:
                    if regex.match(apattern.replace(OBJ_PLACEHOLDER, '.*?'), answer):
                        question_answer_patterns.add((found_pattern.replace('\?', '?'), apattern))
                        subj_a, obj_a = extract_subj_obj_from_sentence(answer, apattern)
                        data_entry_so['answer_pattern'] = apattern
                        break
                others_answers += 1

                subj_answer, obj_answer = subj_a is not None, obj_a is not None
                subj = subj if subj is not None else subj_a
                obj = obj if obj is not None else obj_a

            if subj:
                data_entry_so['subj'] = subj.rstrip('?')
                data_entry_so['subj_quest'] = subj_quest
                data_entry_so['subj_ans'] = subj_answer
            if obj:
                # check if it is a complex sentence: it is a ... / subj is a ...
                if not there_is(obj, PRONS + [subj]):
                    data_entry_so['obj'] = obj.rstrip('?')
                    data_entry_so['obj_quest'] = obj_quest
                    data_entry_so['obj_ans'] = obj_answer
                else:
                    obj = None

            if subj is None or obj is None:
                not_refactorable.append(data_entry_so)
            else:
                refactorable.append(data_entry_so)

    print "Number of not Refactorable: ", len(not_refactorable)
    print "Number of Refactorable: ", len(refactorable)
    print "Number of questions matching ", questions_matching
    print "Number of short answers catched ", short_answers
    print "Number of YES answers ", yes_answers
    print "Number of NO answers ", no_answers
    print "Number of Others shorts patterns: ", others_patterns
    print "Number of Others shorts answers: ", others_answers

    return refactorable, not_refactorable


def get_refactored_data_from_patterns(relation, mode):
    question_answer_good_patterns, qa_g_patterns2regexp = get_qa_patterns_good(relation, mode)
    question_answer_wrongs_patterns, qa_w_patterns2regexp = get_qa_patterns_wrong(relation=relation)

    pattern_found, found, total, removed = 0, 0, 0, 0
    refactorable, not_refactorable = extract_question_answer_patterns(relation)
    refactored_entries = []

    with tqdm(total=len(refactorable)) as td:
        for data_entry in refactorable:
            td.update(1)

            question = data_entry['question'].lower()  # type: str
            answer = data_entry['answer'].lower().rstrip('.,! ')

            if not question or question == '' or not answer or answer == '':
                removed += 1
                continue

            wrong_pattern = match_pattern_question_answer(question_answer_wrongs_patterns, qa_w_patterns2regexp,
                                                          (question, answer))  # type: str
            if wrong_pattern is not None:
                removed += 1
                continue

            found_pattern = match_pattern_question_answer(question_answer_good_patterns, qa_g_patterns2regexp,
                                                          (question, answer))  # type: str

            if found_pattern is not None:
                old_pattern_question = found_pattern[0][0]
                old_pattern_answer = found_pattern[0][1]
                data_entry['question_pattern'] = old_pattern_question
                data_entry['answer_pattern'] = old_pattern_answer

                new_answer = u''
                new_answer_pattern = found_pattern[1][1]
                subj = data_entry.get('subj', None)
                obj = data_entry.get('obj', None)
                pattern_found += 1

                if subj is not None and obj is not None:
                    found += 1
                    for word in new_answer_pattern.split(' '):
                        if word == SUBJ_PLACEHOLDER:
                            new_answer += subj + u' '
                        elif word == OBJ_PLACEHOLDER:
                            new_answer += obj + u' '
                        else:
                            new_answer += word + u' '

                    new_answer = new_answer.rstrip(' ').strip('?')
                    data_entry['answer'] = new_answer
                    data_entry['answer_pattern'] = new_answer_pattern
                    data_entry['obj_ans'] = OBJ_PLACEHOLDER in new_answer_pattern
                    data_entry['subj_ans'] = SUBJ_PLACEHOLDER in new_answer_pattern

            total += 1
            refactored_entries.append(data_entry)

    print "All data entries pre-refactor: ", data_utils.get_relation_size(relation=relation)
    print "Removed: ", removed
    print "Pattern Found: ", pattern_found, "/", total
    print "Found: ", found, "/", total
    print "Number of all entries after-refactor: ", str(len(refactored_entries) + len(not_refactorable))

    return refactored_entries, not_refactorable


def fill_db(relation, schema=database.QUERING_SCHEMA):
    refactored, not_refactored = get_refactored_data_from_patterns(relation=relation, mode=schema)
    all_entries = refactored + not_refactored
    with tqdm(total=len(all_entries), desc="Filling DB with Entries of " + relation.lower()) as td:
        for refactorable_entry in all_entries:
            # print refactorable_entry
            subj = refactorable_entry.get('subj', None)
            obj = refactorable_entry.get('obj', None)
            subj_quest = refactorable_entry.get('subj_quest', None)
            subj_answer = refactorable_entry.get('subj_ans', None)
            obj_quest = refactorable_entry.get('obj_quest', None)
            obj_answer = refactorable_entry.get('obj_ans', None)

            question_pattern = refactorable_entry.get('question_pattern', None)
            answer_pattern = refactorable_entry.get('answer_pattern', None)
            yes_no = refactorable_entry['yes_no']

            question = refactorable_entry['question'].lower()
            answer = refactorable_entry['answer'].lower()
            c1 = refactorable_entry['c1']
            if not babelnet_client.is_valid_synset(c1):
                if '::' in c1:
                    c1 = c1.split('::')[1]
                    if not babelnet_client.is_valid_synset(c1):
                        c1 = None
                else:
                    c1 = None
            else:
                c1 = None

            c2 = refactorable_entry['c2']
            if not babelnet_client.is_valid_synset(c2):
                if '::' in c2:
                    c2 = c2.split('::')[1]
                    if not babelnet_client.is_valid_synset(c2):
                        c2 = None
                else:
                    c2 = None
            else:
                c2 = None

            database.add_qa_data(relation, schema=schema, question=question, answer=answer, subject=subj, object=obj,
                                 subj_quest=subj_quest, obj_quest=obj_quest, subj_ans=subj_answer, obj_ans=obj_answer,
                                 yes_no=yes_no, subject_synset=c1, object_synset=c2,
                                 question_pattern=question_pattern, answer_pattern=answer_pattern)
            td.update(1)


if __name__ == '__main__':
    from src.utils import domains_utils, database

    Relations = domains_utils.get_all_relations_kbs()
    # database.drop_tables(database.ENRICHING_SCHEMA)
    # database.create_tables(database.ENRICHING_SCHEMA)
    # for relation in Relations:
    #     print relation
    #     fill_db(relation, schema=database.ENRICHING_SCHEMA)
    #
    # print
    # print
    # print

    database.drop_tables(database.QUERING_SCHEMA)
    database.create_tables(database.QUERING_SCHEMA)
    for relation in Relations:
        print relation
        fill_db(relation, schema=database.QUERING_SCHEMA)
