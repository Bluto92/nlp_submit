from nltk.tokenize import WordPunctTokenizer
from nltk.corpus import stopwords
import data_refactor
import sentence_manipulation
import codecs
import logging

EnglishStopwords = stopwords.words('english')
Tokenizer = WordPunctTokenizer()

PATTERNS_DIR = '../data/given_patterns/filtered/'
SUBJ_PLACEHOLDER = u'$X$'
OBJ_PLACEHOLDER = u'$Y$'

logging.basicConfig(filename='../logs/tgrambot.log', level=logging.DEBUG,
                    format='%(asctime)s - %(name)s -  %(message)s')
logger = logging.getLogger(__name__)


def check_pattern(pattern):
    return SUBJ_PLACEHOLDER in pattern or OBJ_PLACEHOLDER in pattern


def is_pattern_know(pattern, relation):
    pattern_file_name = PATTERNS_DIR + relation.lower() + '_patterns.txt'
    with open(pattern_file_name, mode='r') as pattern_file:
        for line in pattern_file:
            if line.rstrip('? \n').lstrip().lower() == pattern.rstrip('? ').lstrip().lower():
                return True
    return False


def build_pattern(sentence, subj=None, obj=None):
    if not (subj or obj):
        raise StandardError(
            "Build Pattern (Patterns Manager) can not be invoked without at least one between subj and obj")

    tokenizer = WordPunctTokenizer()
    pattern_words = tokenizer.tokenize(sentence)
    if obj:
        found_object = sentence_manipulation.find_words(sentence=sentence, sub_sentence=obj)
        if len(found_object) != 1:
            return None
        start, end = found_object[0]
        pattern_words = pattern_words[0:start] + [OBJ_PLACEHOLDER] + pattern_words[end:]

    if subj:
        found_subject = sentence_manipulation.find_words(sentence=sentence, sub_sentence=subj)
        if len(found_subject) != 1:
            return None
        start, end = found_subject[0]
        pattern_words = pattern_words[0:start] + [SUBJ_PLACEHOLDER] + pattern_words[end:]

    # check if pattern is different from sentence, i.e. OBJ/SUBJ PLACEHOLDER were inserted
    if pattern_words == tokenizer.tokenize(sentence):
        return None

    # rebuild text representation
    pattern = u''
    for word in pattern_words:
        pattern += word + u' '

    return pattern


def add_pattern(pattern, relation, clean=True):
    """

    :param bool clean: whether to clean or not the given pattern
    :param str | unicode pattern:
    :param str | unicode relation:
    :return:
    """

    if not check_pattern(pattern):
        return None

    clean_pattern = pattern.rstrip('? ').lstrip() + u' ?' if clean else pattern
    if not is_pattern_know(clean_pattern, relation):
        pattern_file_name = PATTERNS_DIR + relation.lower() + '_patterns.txt'
        pattern_file = codecs.open(pattern_file_name, mode='a', encoding='utf-8')
        pattern_file.write('\n' + clean_pattern)
        pattern_file.close()
        logger.debug("Added new pattern for the relation " + relation + ":" + pattern)


def get_patterns(relation, obj=True, obj_ph=OBJ_PLACEHOLDER, subj=True, subj_ph=SUBJ_PLACEHOLDER):
    patterns = data_refactor.get_patterns(relation=relation)[0]
    result_patterns = set(patterns)

    if not obj:
        for pattern in patterns:
            if obj_ph in pattern and pattern in result_patterns:
                result_patterns.remove(pattern)

    if not subj:
        for pattern in patterns:
            if subj_ph in pattern and pattern in result_patterns:
                result_patterns.remove(pattern)

    return result_patterns


if __name__ == '__main__':
    print get_patterns('GENERALIZATION', subj=True, obj=False)
