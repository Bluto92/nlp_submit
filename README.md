# README #

### What is this repository for? ###

This repository contains the result for the final project of the course Natural Language Project 2017.

### How do I get set up? ###
* Dependencies
	* Postgres: [https://www.postgresql.org/download/](https://www.postgresql.org/download/)
	* Keras: use pip or conda
	* Theano: use pip or conda
	* spaCy: use pip or conda

* Theano configuration
	Use the old cuda backend, not the new GpuArray. The latter creates some problems with the multi-module settings of the project.

* spaCy configuration
	please download and link the "en_core_web_md" spacy model. You can do this with the command:
	
		python -m spacy download 'en_core_web_md'
	


* Database configuration
	Register the user **postgres** with password **postgres** (or edit the function create_connection in utils.database).
	Then create a database named **nlp2** with owner **postgres** and use the provided backup (**nlp2.backup**) to restore the data.

* Deployment instructions
	To run the program, please set up the PYTHONPATH variable to the root of the project, then change directory to the source one and run:
	
		FinalProject$ export PYTHONPATH=`(realpath .)`
		FinalProject$ cd src
		src$ python2.7 -m src.telegram\_manager
	