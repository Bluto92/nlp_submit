# -*- coding: utf-8 -*-
import sys

### Author: Jose Camacho-Collados

#This script evaluates the quality of domain annotations of a lexical resource with respect to a gold dataset. See the README for more details


def f1(precision,recall):
    return 2*(precision*recall)/(precision+recall)

def get_gold_annotations(path_goldstandard):
    gold_file=open(path_goldstandard).readlines()
    set_testdomains=set()
    dict_gold={}
    for line in gold_file:
        linesplit=line.replace("\n",'').split("\t")
        synset=linesplit[0]
        domain=linesplit[1].strip()
        dict_gold[synset]=domain
        set_testdomains.add(domain)
    return dict_gold,set_testdomains

def main(path_goldstandard, path_babeldomains, evaluation_mode="onlytest"):


    print ("\nType of evaluation: "+str(evaluation_mode))

    dict_gold,set_testdomains=get_gold_annotations(path_goldstandard)
    print ("Number of domains in the evaluation file: "+str(len(set_testdomains)))

    final_domains_file=open(path_babeldomains).readlines()
    
    prec=0
    total=0
    total_precision=0
    
    for line in final_domains_file:
        linesplit=line.replace("\n",'').split("\t")
        if len(linesplit)>=2:
            synset=linesplit[0]
            first_domain=linesplit[1].strip()
            if synset in dict_gold:
                if evaluation_mode!="all" and first_domain not in set_testdomains:
                    continue
                total+=1
                if dict_gold[synset]==first_domain:
                    prec+=1
    
    total_recall=len(dict_gold)
    precision=(prec*100.0)/total
    recall=(prec*100.0)/total_recall
    f_measure=f1(precision,recall)

    print ("\nPrecision: "+str(round(precision,3))+"%"+ " ("+str(prec)+" out of "+str(total)+ ") ")
    print ("Recall: "+str(round(recall,3))+"%"+ " ("+str(prec)+" out of "+str(total_recall)+ ") ")
    print ("F-Measure: "+str(round(f_measure,3))+"%\n")


if __name__ == '__main__':

    args = sys.argv[1:]

    if len(args) >= 2:

        path_goldstandard = args[0]
        path_babeldomains = args[1]
        
        if len(args) >= 3:
            evaluation_mode=args[2]
            main(path_goldstandard, path_babeldomains, evaluation_mode)
        else:
            main(path_goldstandard, path_babeldomains)
        
        
    else:
        sys.exit('''
            Requires:
            path_goldstandard -> Path of the outlier detection directory
            path_babeldomains -> Path of the input word vectors
            evaluation_mode (optional) -> "all" or "onlytest"
            ''')

