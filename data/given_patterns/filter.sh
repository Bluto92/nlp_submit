#
# author: Francesco Ilario
#

RELATIONS=(activity colorPattern generalization howToUse material place purpose shape similarity size smell sound specialization taste time part)

DIR_FILTER='./filtered/'
DIR_DATA='./patterns/'

if [ ! -d "$DIR_DATA" ]
then
	echo "Can not find the patterns directory: $DIR_DATA"
	exit
fi

if [ ! -d "$DIR_FILTER" ]
then
	mkdir $DIR_FILTER
fi

if [ $# -gt 0 ]
	then
		RELATIONS=("$@")
fi

rm -r -f $DIR_DATA"patterns1.txt"
rm -r -f $DIR_DATA"patterns29.txt"

for RELATION in "${RELATIONS[@]}"
do
	REL_PAT_FILE=$RELATION"_patterns.txt"

	cat $DIR_DATA* | grep "	"$RELATION | grep "?" | sort  | uniq -i > $REL_PAT_FILE".tmp"
	sed {'s/ ? /?/g;s/? /?/g;s/ ?/?/g;s/"	"$RELATION//g;s/?.*/?/g;s/?/ ?/g;s/#X#/X/g;s/#Y#/Y/g;s/X/$X$/g;s/Yes/yes/g;s/Y/$Y$/g'} $REL_PAT_FILE".tmp" | uniq
	sed {'s/ ? /?/g;s/? /?/g;s/ ?/?/g;s/"	"$RELATION//g;s/?.*/?/g;s/?/ ?/g;s/#X#/X/g;s/#Y#/Y/g;s/X/$X$/g;s/Yes/yes/g;s/Y/$Y$/g'} $REL_PAT_FILE".tmp" | uniq > $DIR_FILTER$REL_PAT_FILE
	rm $REL_PAT_FILE".tmp"
done
