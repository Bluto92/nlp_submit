\documentclass[a4paper,9pt]{extarticle}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{lmodern}
\usepackage{graphicx}
\usepackage[font=scriptsize, labelfont=bf]{caption}
\usepackage[margin=1.2in]{geometry}
\usepackage{subfigure}
\usepackage{hyperref}
\usepackage{titling} % Customizing the title section
\usepackage{titlesec} % Allows section customization
\usepackage{abstract} % Allows abstract customization
\usepackage{multicol}
\usepackage[table]{xcolor}% http://ctan.org/pkg/xcolor
\usepackage{cleveref}
\usepackage{enumitem}
\usepackage{caption}
\usepackage{titlesec}
\usepackage{textcomp}
\usepackage{comment}
% \usepackage{indentfirst}
\usepackage{tabularx}

\titlespacing*{\section}{0pt}{0.5\baselineskip}{0.5\baselineskip}
\titlespacing*{\subsection}{0pt}{0.4\baselineskip}{0.4\baselineskip}
\titlespacing*{\subsubsection}{0pt}{0.3\baselineskip}{0.3\baselineskip}

\graphicspath{ {imgs/} }
\renewcommand{\abstractnamefont}{\normalfont\bfseries}
\renewcommand{\abstracttextfont}{\normalfont\small\itshape}
\titleformat{\section}{\normalfont\fontsize{10}{12}\bfseries}{\thesection}{1em}{}
\titleformat{\subsection}{\normalfont\fontsize{10}{12}\bfseries}{\thesubsection}{1em}{}
\titlespacing*{\section}{0pt}{1.5ex plus 1ex minus .2ex}{1ex plus .2ex}
\titlespacing*{\subsection}{0pt}{1.5ex plus 1ex minus .2ex}{1ex plus .2ex}

\setlength{\droptitle}{-4\baselineskip} % Move the title up
\title{Natural Language Processing 2017\\%
        Final Project}
\author{Francesco Ilario\\%
        1469228}

\date{}

\begin{document}
\maketitle

\begin{abstract}
\noindent
The result of my work is a bot that tries to understand questions and to give the best answer.
He is also able to ask questions and analyze the answers.
He heavily relies on patterns, Babelfy\cite{sw:babelfy} and spaCy\cite{sw:spacy}, and is able to enrich his patterns' pool analyzing the users' questions.
In the project are also used various Machine Learning tools \cite{sw:scilearn}, Recurrent Neural Networks \cite{sw:keras} and Glove\cite{sw:glove}.
\end{abstract}

\tableofcontents

\newpage
\begin{figure*}[!b]
  \begin{center}
    \includegraphics[width=\linewidth]{NLP-Querying.png}
    \vspace{-8ex}
    \caption{Querying Process Workflow - created with Bizagi Modeler\cite{sw:bizagi-modeler}}
    \label{fig:querying}
  \end{center}
\end{figure*}

\begin{multicols}{2}
\section{Data Pre-Processing}
The given Knowledge Base was very noisy, so I spent a lot of time developing an automated procedure to clean it.
Indeed, in the given Knowledge Base there were a lot of completely wrong Question-Answer pairs like ``What is \$X\$? yes''.
I was able to remove all the syntactically wrong Question-Answer pairs, but could not be able to filter out the semantically wrong ones.
An example of a correct and a wrong pairs that are still present in the relation \textit{Generalization} is the following:
\begin{itemize}[noitemsep]
  \item What is Google Earth? Virtual Globe
  \item What is Earth? Virtual Globe
\end{itemize}

\section{Used Features}
\subsection{Machine Learning tools}
\label{k-means}
\begin{itemize}[noitemsep, nolistsep]
  \item \textbf{TfIdfVectorizer}: used to preprocess the input of the Support Vector Machines (next) [scikit-learn\cite{sw:scilearn}].
  \item \textbf{Support Vector Machine}: used to predict the Relation, the Question type and Answer type [scikit-learn\cite{sw:scilearn}].
  \item \textbf{K-Means Algorithm}: used to identify the \textit{Per Relation Stopwords} [scikit-learn\cite{sw:scilearn}]. 
  \item \textbf{Recurrent Neural Networks}: used to predict Subject/Object in sentences and to guess a Yes/No Answer [keras\cite{sw:keras}].
\end{itemize}

\subsection{Per Relation Stopwords}
\label{subsec:prstopwords}
As anticipated in \cref{k-means}, I used a Machine Learning Clustering Algorithm to extract a set of ad-hoc stopwords for each relation.
For each relation, I calculated the \textit{Term Frequency} of each term -considering only the questions- and I used the K-Means algorithm to identify the words that appear with a very high frequency.
This action is performed one time for each relation and the result is a list of lists of words that behave, in terms of frequency, as stopwords for their relation.
These words and the English set of nltk's\cite{sw:nltk} stopwords are used to build the set of \textit{per-relation stopwords}.
As an example, this procedure correctly identifies the word ``color'' as a stopword for the relation Color, or ``example'' for Generalization, or ``shape'' for Shape, and so on.

\subsection{Postgres Database}
\label{subsec:postgres}
I stored the data in a Postgres database. 
This allowed me to handle the data quickly and to have a better data visualization.
I created two schemas, one for the Querying and one for the Enriching phase.
In each of them there is one table for relation and an additional table needed in specific parts of each phase workflow (discussed in \cref{ssec:pattern-enriching} and \cref{subsec:choose-answer}).

\section{Querying}
The main components of the Querying phase are: 
\begin{enumerate}[noitemsep]
  \item Relation Prediction 
  \item Question Type Prediction
  \item Full Question Match
  \item Pattern Analysis 
  \item Subject-Object Guess
  \item Main Sense Extraction
  \item Yes/No Answer Guess
  \item Answer Building
\end{enumerate}
From here to the end of the Querying section, we will call the Selected Domain $\mathcal{D}$, the predicted Relation $\mathcal{R}$ and the User Question $\mathcal{Q}$.

\subsection{Relation Prediction}
\label{subsec:relation-prediction}
As first thing the program uses the Scikit-Learn tools \cite{sw:scilearn} to predict the (correct) relation $\mathcal{R}$ from the given $\langle \mathcal{Q}, \mathcal{D} \rangle$ pair.
In order to find the best classifier to use for this problem, I have extracted a train and a test set from the questions in the Knowledge Base and used the \textit{Pipeline} tool to combine the \textit{TfidfVectorizer} with the MultinomialNB, LinearSVC, SGDClassifier Classifiers and performed a \textit{GridSearchCV} on each Pipeline.
The classifier that performed best was the SGDClassifier -a Linear Support Machine with stochastic gradient descent (SGD) learning, obtaining for each domain an F1 Score in the interval $0.96$-$0.99$.

\subsection{Question Type Prediction}
\label{subsec:question-type-prediction}
Another important prediction that the program does is whether the user question wants a Yes/No answer or an Open-Ended one.
In order to create this classifier I extracted the following two sets: one composed by all the questions that contain only the Subject (Object) and have in the answer the Object (Subject), and one composed by all the questions that contain both Subject and Object and the answer is ``yes'' or ``no''.
To decide the best suited classifier for this task, I used the same approach described in \cref{subsec:relation-prediction}, and obtained almost the same results.

\subsection{Full Question Match}
In this phase the algorithm looks in the database for the question $\mathcal{Q}$, and collects all the Question-Answer pairs that matches the user question $\mathcal{Q}$.
If the result is not an empty list, it chooses an answer (\cref{subsec:choose-answer}).

\subsection{Pattern Analysis}
If $\mathcal{Q}$ respects a known pattern for the predicted relation $\mathcal{R}$, the algorithm uses this pattern to extract the Subject and/or Object.
The main problem in this case is if the used pattern does not properly match the user question.
As an example the pattern ``What is \$X\$?'' matches the question ``What is an example of car?''.
Indeed the extracted subject will be ``an example of car''. 
In order to perform a correct extraction the algorithm behaves in the following way.
First of all he sorts all the patterns from the longest to the shortest and checks the longest first. 
So if it is also known the pattern ``What is an example of \$X\$?'', it checks this one first.
Otherwise he validates the extracted concepts against the known objects and subjects -stored in the Knowledge Base, so he can say ``yes it is a correct extraction because I know these concepts'', or Babelfy\cite{sw:babelfy} or the spaCy\cite{sw:spacy} entities, as described in \cref{sssec:concept-validation-procedure}.
If he is able to validate the extracted concepts, he looks in the database for matches with the extracted information and goes to the choose answer phase (\cref{subsec:choose-answer}).

\subsubsection{Concept Validation Procedure}
\label{sssec:concept-validation-procedure}
The Concept Validation Procedure keeps as input a sentence and tries to state if this represent a concept or not using its own Knowledge Base, Babelfy\cite{sw:babelfy} and spaCy\cite{sw:spacy}.
Hence the first things that this procedure does are to look in its Knowledge Base for a match with the known Subjects and Objects, to ask spaCy if the sentence represents an entity and to ask Babelfy to disambiguate the sentence in order to extract Concepts and Named Entities.
If this checks fail all, then the program keeps the \textit{per-relation stopwords} and uses them to right and left strip the terms in the sentence.
So, assuming that the set of stopwords contains the words ``is'', ``an'', ``example'' and ``of'', then stripping the sentence ``an example of car of Toyota is'' would give as result ``car of Toyota''.
After this stripping, it tries to validate the newly extracted sentences.
If this second validation is successful, the program can use this data to extract a new pattern.

\subsubsection{Pattern Enriching}
\label{ssec:pattern-enriching}
If the used pattern gives the opportunity to extract a new pattern (like in the previous example), the program will store this new pattern in its patterns pool.
As an example, through the pattern ``What is \$X\$?'', the program is able to identify the subsequence ``an example of car'' from the question ``What is an example of car?''.
After the first validation fails, the program strip the subsequence obtaining the concept ``car'' and so he is able to build the new pattern ``What is an example of \$X\$?''.
Otherwise, if he could not match any pattern he stores the $\langle \mathcal{Q} - \mathcal{R} - $QuestionType$\rangle$ triple in a database table.
These data can be used lately in order to extract new patterns.

\subsection{Subject-Object Guess}
\label{ssec:so-guessing}
If until now, it was not possible to extract a valid Object/Subject (or both), the program uses a Recursive Neural Network that I trained on tagged questions.
Giving a question as input, the RNN will give as output a tag for each word in the sentence that expresses if the current word is a subject or an object or none of them.
If the result of this guessing is a consistent one, i.e. there aren't more than one sequence of words tagged as Object or Subject and there is at least one for one of them, then the algorithm tries to validate them (or it).
If the validation is successful he asks to the database for the triples with that Object/Subject (or both).

\subsection{YesNo Answer Guess}
If the question type is a Yes-No one (vid \cref{subsec:question-type-prediction}) we can finally rely on a set of RNNs that I trained, one for each relation, on this type of question.
Each one of these RNNs is able to predict a 'Yes' or 'No' answer to the given question.
The results of the training and testing was quite astonishing, indeed the F1 score is quite always 1.0.
However, the RNN will always give an answer, but it is not certain to guess right.
So I decided to store the Questions that are guessed and that was previously correctly validated in the Enriching database and reuse them in the Enriching phase (\cref{sss:unanswered-uq}).

\subsection{Main Sense Extraction}
\label{subsec:mse}
This phase is only reached if the question is not a Yes/No one and it was not possible with the previously described checks to individuate the Subject/Object.
Indeed the Main Sense Analysis may be very imprecise with Yes/No questions, where it is very important to correctly recognize the roles of the concepts.

The Main Sense Extraction algorithm tries to extract the main sense from the user questions.
It uses Babelfy\cite{sw:babelfy} and spaCy\cite{sw:spacy} to disambiguate the question and identify the concepts.
After that it builds the list of words of the question, merges the words of the found concepts and removes the words that are in the \textit{per-relation stopwords}.
An example is the following:
\begin{itemize}[noitemsep]
  \item \textbf{User Question}: What is google earth?
  \item \textbf{Concepts}: what, is, google earth
  \item \textbf{Concepts w/o Stopwords}: google earth
\end{itemize}
At this point the algorithm asks to the database for all the Question-Answer pairs where \textbf{all} the extracted concepts appear.
If the result is an empty list, it asks for all the Question-Answer pairs where \textbf{at least one} extracted concept appear.
Then for each Question-Answer pair it extracts the Main Sense, and compute the \textbf{Jaccard Distance} between the current Main Sense and the User Question's one.
After that it considers only the list of Question-Answer pairs that maximize the Jaccard Distance.
Anyway, if the maximum Jaccard Distance score is equal or lower than $0.5$ then the extracted answers are not considered valid.

\subsection{Answer Building}
\label{subsec:choose-answer}
This is the last task in the Querying Workflow.
The scenario here is that the program processed the user question, tried to understand it and extracted a list of Question-Answer pairs from its Knowledge Base to use to build a valid answer.
If this list is not empty, the program has to choose one of them.
The simplest case is when the list is composed by only one possible answer, and in this case he will use that one.
If more than one Question-Answer pair are extracted, he, as first thing, looks for the Babelnet \cite{sw:babelnet} Synset Id associated with the Subject/Object of the User Question and tries to find a match with the one of the candidates Question-Answer pairs.
In the case that this match is not obtained, he will reduce the candidates list using the Jaccard Distance approach described in \cref{subsec:mse}.
However, the final result may always be a list of possible answers and he chooses one at random among them. 
I also tried to collapse them in a unique answer, but the result was most of the time very imprecise.
Another case is if the Question is of the Yes/No type. 
In this case the list of answers will be composed by a certain number of \textit{yes} and/or \textit{no}.
If this list does not contain at the same time one \textit{yes} and one \textit{no} it is considered a valid extraction.

It is possible that this phase does not have valid Question-Answer pairs to deal with.
In this case the algorithm produces a \textit{Sorry} message.
If in the computation he were able to identify the Subject/Object he will express that.

Furthermore, if he was able to understand the pattern and validate the information, but he does not know anything about, he stores these data in a table so he will be able to reuse them in the Enriching phase (\cref{sss:unanswered-uq}).

\begin{comment}
\subsection{Conclusions}
Finally, we can resume the Querying Workflow, in its main parts.
Initially the program tries a raw question matching with the Question-Answer pairs that he knows.
If it fails, he tries to analyze the question in order to understand the Subject/Object and/or to extract the Main Sense.
He uses the data produced in these latter phases to find a good answer.
If he is not able to answer but is able to understand the concepts and their role in the question, he stores that data in an ad-hoc database's table for further use during the Enriching phase.
Otherwise, if he could not understand nothing, he stores the data in another an ad-hoc database's table.
This latter table's data can be used to mining new patterns.
% The big issue in this case, is to assign the role to the newly extracted patterns.
\end{comment}

\section{Enriching}
\begin{figure*}[!t]
  \begin{center}
    \includegraphics[width=\linewidth]{NLP-Enriching.png}
    \vspace{-8ex}
    \caption{Enriching Process Workflow - created with Bizagi Modeler\cite{sw:bizagi-modeler}}
    \label{fig:enriching}
  \end{center}
  \vspace{-2ex}
\end{figure*}
\subsection{Ask Question}
\subsubsection{Unanswered User Questions}
\label{sss:unanswered-uq}
When there is the necessity of building a question for enriching, the first action that the program does is to look in the database for the questions that he was able to understand during the querying phase but at which he did not have an answer.
These questions may be of both the types, Yes/No and Open-Ended.
If there are not unanswered questions for the given domain $\mathcal{D}$, then he needs to build a new one.
\subsubsection{Relation Selection}
In this phase the program decides which relation $\mathcal{R}$ he wants to talk about, given the user chosen domain $\mathcal{D}$.
The main ideas here are to enrich the poorest relations and avoid the scenario in which the bot asks always about the same things. % like a granny
Given the domain $\mathcal{D}$, the program retrieves the set of relations $\mathcal{R_\mathcal{D}}$ related to $\mathcal{D}$, keeps only half of them choosing among the poorest and decides one at random.

\subsubsection{Subject choice and Question building}
In order to chose the Subject of the question to be asked, the program looks in the database for all the disambiguated subjects and randomically chooses one that is successfully validated using Babelfy\cite{sw:babelfy} (this validation is needed because the Knowledge Base is very noisy).
Then, he looks for all the patterns that contain only the subject and builds the question substituting the Subject Tag with the chosen Subject.

\subsection{User Answer Analysis}
\subsubsection{Open-Ended Answer}
If the question is a Open-Ended One, the answer analysis is divided in the following phases:
\begin{enumerate}[noitemsep, nolistsep]
  \item Answer Validation
  \item Semantic Tree Analysis
  \item Subject/Object Guessing
\end{enumerate}

\paragraph{Answer Validation}
This is the first check and is done in order to detect ``factoid'' answers.
As an example, doing this we are able to correctly process Question-Answer pairs like ``What is a car'', ``a vehicle''.
Here, it is used the validation procedure described in the \cref{sssec:concept-validation-procedure}.

\paragraph{Semantic Tree Analysis}
If the answer is not a Factoid, the previous validation will probably fail.
In this case, the algorithm tries to analyze the answer structure and to tag the triple's object.
This is done considering all the verbs in the semantic tree build on the answer.
So, first of all it builds the semantic tree using spaCy\cite{sw:spacy} and collapse the \textit{compound terms}.
Then, for each verb in the tree, it analyzes the left and right subtrees and validates the extracted concepts with Babelfy\cite{sw:babelfy}.

This part aim is to extract information from \textit{CONCEPT-VERB-CONCEPT} phrases.
It will also check the left and right subtrees in order to find a reference to the subject.

\paragraph{Subject/Object Guessing}
If the semantic tree analysis was unsuccessful, the algorithm tries to tag the answer words with an RNN that I trained on the answers of the Knowledge Base, similar to the one described in \cref{ssec:so-guessing}.
The original Knowledge Base's answers were a mix of factoid answers and open-ended ones.
Hence, I tried to extract answer patterns and to refactor them with an automatic procedure in order to have more open-ended answers.
The result of this refactor is stored in the ``enriching'' schema in the database (vid \cref{subsec:postgres}).
As usual the result of the guessing are validated with spaCy\cite{sw:spacy} and Babelfy\cite{sw:babelfy}.

\subsubsection{YesNo Answer}
If the question is a Yes/No one, then the program processes the user answer using a Linear Support Vector Machine with Stochastic Gradient Descendant (SGD) learning to classify the answer type.
The answer may be classified as a ``Yes'', a ``No'' or a ``Maybe''.
This third class is used to isolate answers like ``I don't know'' or ``I could not understand the question'', and so on.
Only the answers classified as ``Yes'' and ``No'' are considered as valid.
To train this classifier I hand-wrote a list of possible answers and tagged them as ``Yes'', ``No'' or ``Maybe''.

\subsubsection{Update Results to the Shared KB}
I thought that not all the user answers will be serious answers.
So I decided to do not update to the Shared Knowledge Base all the user answers.
Indeed, only the ones that the program was able to validate are uploaded.
In this way, answers like ``I don't know'' or ``I do not understand you'' should not be uploaded.
Furthermore, when the bot starts a new thread is launched to maintain the local database up to date.
Every hour this thread wakes up and checks for new updates.

\section{Pattern Mining}
\label{ssec:pattern-mining}
For each relation $\mathcal{R}$, the system uses the questions $\mathcal{Q_R}$ that he was not able to analyze using patterns to mine new patterns.
The system starts extracting an ad-hoc set of stopwords $\mathcal{S_R}$, composed by the \textit{per-relation stopwords} and the list of more frequent words in the questions $\mathcal{Q_R}$, obtained applying the techniques described in \cref{subsec:prstopwords}.
Then it uses $\mathcal{S_R}$ to extract common patterns and group the data.
More precisely, in each question it substitutes all the words that are not in its ad-hoc set of stopwords with a special tag and collapse all the special tags that are subsequent.
As an example from the sentence ``is United Kingdom in Italy?'', it will probably return ``is SENSE in SENSE?''.
The algorithm keeps track of the questions related to each pattern, uses these patterns to extract the concepts and Babelfy\cite{sw:babelfy} and spaCy\cite{sw:spacy} to validate them.
The main problem here is to decide which is the right tag to assign to each concept.
I used the RNN I trained to tag questions' concepts with their triple's role, described in \cref{ssec:so-guessing}.
% This RNN classifies the words in each question as Triple's Subject or Triple's Object or none of them.
The predicted concepts are compared with the ones extracted using patterns.
If, in this way, it is possible to successfully extract a good amount of consistent data they are used to extract one new pattern.
For example, if the RNN correctly tags ``United Kingdom'' as Subject and ``Italy'' as Object, and tags at least 20 extracted questions for this pattern in this way, it can correctly conclude that the pattern ``is \$X\$ in \$Y\$?'' is a new valid pattern.
As last thing, it adds this new pattern to the patterns pool and remove all the entries that matches this pattern from the not understood questions' table.
When the program is executed it launches a new thread that every hour tries to mine new patterns.
\end{multicols}

\newpage
\section{Notes for Bot Execution}
\noindent
Be sure to have at least 16 GB of RAM before running the program.
The only things that are required are to link or copy the Glove model \href{http://nlp.stanford.edu/data/glove.840B.300d.zip}{glove.840B.300d.txt} in the directory \textit{glove} and to setup the postgres database.
To have a ``lightweight'' execution I disabled the Pattern Miner feature, to enable it please uncomment the corresponding lines in the ``telegram\_manager.py'' main but the program will require more than 16 GB of RAM.

\subsection{Download the Complete Solution}
\noindent
You can find the complete solution at the following link:
\begin{center}
  \text{\href{https://Bluto92@bitbucket.org/Bluto92/nlp\_submit}{https://Bluto92@bitbucket.org/Bluto92/nlp\_submit}}
\end{center}

\subsection{Postgres}
\noindent
Please install Postgres and psycopg2 (from pip).
Then create the database ``nlp2'' with owner \textbf{postgres} and password \textbf{postgres}, then restore the data with the provided backup.

\subsection{Theano and Keras}
\noindent
The developed Recurrent Neural Networks use Keras 2.0.8 and Theano 0.9.
I've noticed some problems with the intra-module settings of the program and the GpuArray backend for Theano, please use the old cuda backend.
To do this add the line (or edit if yet present) ``device = gpu'' to the file ``.theanorc'' (this configuration file should be in your home directory).
However, I suggest to use the cpu (``device = cpu''), because I found this configuration faster when no training is needed.

\subsection{spaCy}
\noindent
Please install also spaCy and download the model ``en\_core\_web\_md''.
To download the model use the following command:
\begin{center}
\text{python -m spacy download `en\_core\_web\_md'}
\end{center}

\subsection{Launch the program}
\noindent
Please launch the program with the bash script ``\textbf{start\_bot.sh}'', or follow the following simple steps.
Before launching the program set the PYTHONPATH variable to the root directory of the project, change directory to the ``src'' dir and launch as module using python2.7.
\begin{center}
  \begin{tabular} {l}
    \text{\texttt{FinalProject\$} export PYTHONPATH=\textasciigrave(realpath .)\textasciigrave } \\
    \text{\texttt{FinalProject\$} cd src}  \\
    \text{\texttt{src\$} python2.7 -m src.telegram\_manager}
  \end{tabular}
\end{center}

\subsection{Bot Commands}
\noindent
To start the dialogue with the bot it is not needed any particular command, while you can stop it through the command \texttt{/stop}.
Once you choose a domain, you can ask which  are the relations in the domain using the command \texttt{/relations}.
To print the help use the \texttt{/help} command.
An important command to use during Querying is \texttt{/verbose}.
It set the verbosity of the output. You must pass an integer value in the interval $[0, 5]$.
With a verbosity of $0$ it will display only the answer, while with a verbosity of $5$ it will display a lot of information such the identified subject/object and their synsetId (if recognized) or the Knowledge Base's Question-Answer pairs that are used to build the answer.

\begin{thebibliography}{9}
\bibitem{sw:babelfy}
  \emph{Babelfy}: \href{http://babelfy.org}{http://babelfy.org}
  
\bibitem{sw:spacy}
  \emph{spaCy}: \href{https://spacy.io/}{https://spacy.io/}

\bibitem{sw:babelnet}
  \emph{BabelNet}: \href{http://babelnet.org/}{http://babelnet.org/}

\bibitem{sw:nltk}
  \emph{nltk}: \href{http://www.nltk.org/}{http://www.nltk.org/}
    
\bibitem{sw:scilearn}
  \emph{Scikit-Learn}: \href{http://scikit-learn.org/}{http://scikit-learn.org/}

\bibitem{sw:keras}
  \emph{keras}: \href{http://keras.io/}{http://keras.io/}

\bibitem{sw:glove}
  \emph{Glove}: \href{https://nlp.stanford.edu/projects/glove/}{https://nlp.stanford.edu/projects/glove/}

\bibitem{sw:postgres}
  \emph{Postgres}: \href{https://www.postgresql.org}{https://www.postgresql.org}
  
\bibitem{sw:bizagi-modeler}
  \emph{Bizagi Modeler}: \href{https://www.bizagi.com/en/products/bpm-suite/modeler}{https://www.bizagi.com/en/products/bpm-suite/modeler}  

\end{thebibliography}


\end{document}
